package com.mtjsoft.www.myapplication.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.model.ZhiNengXunJianModel;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mtj 智能巡检机器人台账及告警值设定、修改记录
 *         Created by Administrator on 2018/3/9.
 */

public class ZhiNengXunJianActivity extends BaseActivity {

    private static final int CHOOSEBIANDIAN = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private int row = 20;
    private List<ZhiNengXunJianModel> zhiNengXunJianModelList;
    private List<List<ZhiNengXunJianModel>> listRowDatas = new ArrayList<>();
    private String zhan = "___";

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setOnClickListener(this);
        setTopName(String.format(getString(R.string.zhineng_xunjian_title), zhan));
        addTableTitle();
        readData();
        addCenterView(row);
        setData();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        removeTopLayoutViewAll();
        String[] titles = getResources().getStringArray(R.array.zhinengxunjian);
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < 8; i++) {
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            if (i == 3) {
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
            }
            drawTextView.setLayoutParams(layoutParams);
            drawTextView.setText(titles[i]);
            view.addView(drawTextView);
        }
        LinearLayout shuLayout = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_vertical_linearlayout_f_f, null);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 3);
        shuLayout.setLayoutParams(layoutParams);
        //添加告警值设定修改情况
        DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText(titles[8]);
        shuLayout.addView(drawTextView);
        //
        LinearLayout hengLayout = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_f, null);
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText(titles[9]);
        hengLayout.addView(drawTextView);
        //
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText(titles[10]);
        hengLayout.addView(drawTextView);
        //
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText(titles[11]);
        hengLayout.addView(drawTextView);
        shuLayout.addView(hengLayout);
        view.addView(shuLayout);
        addTopLayoutView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(int row) {
        for (int j = 0; j < row; j++) {
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            for (int i = 0; i < 11; i++) {
                if (i == 6) {
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                    drawTextView.setTag(i);
                    drawTextView.setOnClickListener(this);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                } else if (i == 3) {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                    drawEditText.setLayoutParams(layoutParams);
                    view.addView(drawEditText);
                } else {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                    drawEditText.setTag(i);
                    if (i == 0) {
                        drawEditText.setEnabled(false);
                        drawEditText.setText((pLayout.getChildCount() + 1) + "");
                    }
                    drawEditText.setLayoutParams(layoutParams);
                    view.addView(drawEditText);
                }
            }
            pLayout.addView(view);
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        if (v.getId() == R.id.iv_add_row) {
            addCenterView(5);
            return;
        }
        if (v.getId() == R.id.tv_base_top_name) {
            Intent intent = new Intent(this, ChooseActivity.class);
            intent.putExtra("type", 1);
            startActivityForResult(intent, CHOOSEBIANDIAN);
            return;
        }
        int tag = (int) v.getTag();
        switch (tag) {
            case 6:
                chooseTime();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            zhan = name;
                            setTopName(String.format(getString(R.string.zhineng_xunjian_title), zhan));
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 选择日期
     */
    private void chooseTime() {
        //时间选择器
        TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                clickText.setText(TurnsUtils.convertToString(date,"yyyy-MM-dd"));
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(new boolean[]{true, true, true, false, false, false})
                .build();
        pvTime.show();
    }

    /**
     * 读取数据
     */
    private void readData() {
        String type = "0";
        zhiNengXunJianModelList = DataSupport.findAll(ZhiNengXunJianModel.class);
        if (zhiNengXunJianModelList != null && zhiNengXunJianModelList.size() > 0) {
            for (int i = 0; i < zhiNengXunJianModelList.size(); i++) {
                if (!type.equals(zhiNengXunJianModelList.get(i).getSbbh())) {
                    type = zhiNengXunJianModelList.get(i).getSbbh();
                    List<ZhiNengXunJianModel> rowList = DataSupport.where("sbbh = ?", type).find(ZhiNengXunJianModel.class);
                    listRowDatas.add(rowList);
                }
            }
        }
        if (listRowDatas != null && listRowDatas.size() > row) {
            row = listRowDatas.size();
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        if (listRowDatas != null && listRowDatas.size() > 0) {
            for (int i = 0, plCh = listRowDatas.size(); i < plCh; i++) {
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                ZhiNengXunJianModel model = listRowDatas.get(i).get(0);
                if (!TextUtils.isEmpty(model.getBzmc())) {
                    zhan = model.getBzmc();
                }
                ((DrawEditText) linearLayout.getChildAt(1)).setText(model.getSbbh());
                ((DrawEditText) linearLayout.getChildAt(2)).setText(model.getSydd());
                ((DrawEditText) linearLayout.getChildAt(3)).setText(model.getCj());
                ((DrawEditText) linearLayout.getChildAt(4)).setText(model.getXh());
                ((DrawEditText) linearLayout.getChildAt(5)).setText(model.getBh());
                ((DrawTextView) linearLayout.getChildAt(6)).setText(model.getTyrq());
                ((DrawEditText) linearLayout.getChildAt(7)).setText(model.getCszsd());
                for (int j = 8; j < listRowDatas.get(i).size() + 8; j++) {
                    if (j < 11) {
                        ((DrawEditText) linearLayout.getChildAt(j)).setText(listRowDatas.get(i).get(j - 8).getXgdz());
                    }
                }
            }
            setTopName(String.format(getString(R.string.zhineng_xunjian_title), zhan));
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        DataSupport.deleteAll(ZhiNengXunJianModel.class);
        List<ZhiNengXunJianModel> zhiNengXunJianModelList = new ArrayList<>();
        for (int i = 0, plCh = pLayout.getChildCount(); i < plCh; i++) {
            if (pLayout.getChildAt(i) instanceof LinearLayout) {
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                DrawEditText drawTextView = (DrawEditText) linearLayout.getChildAt(1);
                if (!TextUtils.isEmpty(drawTextView.getText().toString())) {
                    //三次加入三个
                    for (int j = 8; j < linearLayout.getChildCount(); j++) {
                        ZhiNengXunJianModel model = new ZhiNengXunJianModel();
                        model.setBzmc(zhan);
                        model.setSbbh(drawTextView.getText().toString());
                        model.setSydd(((DrawEditText) linearLayout.getChildAt(2)).getText().toString());
                        model.setCj(((DrawEditText) linearLayout.getChildAt(3)).getText().toString());
                        model.setXh(((DrawEditText) linearLayout.getChildAt(4)).getText().toString());
                        model.setBh(((DrawEditText) linearLayout.getChildAt(5)).getText().toString());
                        model.setTyrq(((DrawTextView) linearLayout.getChildAt(6)).getText().toString());
                        model.setCszsd(((DrawEditText) linearLayout.getChildAt(7)).getText().toString());
                        model.setCs("" + (j - 7));
                        model.setXgdz(((DrawEditText) linearLayout.getChildAt(j)).getText().toString());
                        zhiNengXunJianModelList.add(model);
                    }
                }
            }
        }
        DataSupport.saveAll(zhiNengXunJianModelList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
