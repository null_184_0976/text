package com.mtjsoft.www.myapplication.ui;

import android.content.Intent;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.YunWeiQueXianModel;
import com.mtjsoft.www.myapplication.utils.HHDensityUtils;
import com.mtjsoft.www.myapplication.utils.HHInputUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mtj 设备缺陷记录
 *         Created by Administrator on 2018/3/11.
 */

public class YunWeiQueXianActivity extends BaseActivity {

    private static final int CHOOSEBIANDIAN = 1001;
    private static final int CHOOSEGJ = 1002;
    private static final int CHOOSENAME = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private List<DrawTextView> drawTextViewList = new ArrayList<>();
    private List<DrawEditText> drawEditTextList = new ArrayList<>();

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setVisibility(View.GONE);
        setTopName("设备缺陷记录");
        addTitleLayout("缺陷填报",false);
        addCenterView("变电站：", true, "缺陷设备：", false);
        addCenterView("电压等级：（设备电压等级）", false, "设备双重名称：", false);
        addCenterView("缺陷性质：（危急、严重、一般）", false, "发现方式：（巡视、带电检测等）", false);
        addCenterView("发现人：", true, "发现时间：", true);
        addCenterView("填报人：", true, "填报时间：", true);
        addDrawEditTextLayout("缺陷描述：","典型特征：（没有则空白）");
        addDrawEditTextLayout("汇报调度情况：（*时*分汇报值班调度员**，值班调度员命令**，无需汇报则空白）","汇报监控情况：（*时*分汇报当值监控员**，当值监控员命令**，无需汇报则空白）");
        //
        addTitleLayout("缺陷审核",false);
        addCenterView("审核意见：", false, "缺陷性质：", false);
        addCenterView("审核时间：", true, "审核人：", true);
        //
        addTitleLayout("缺陷审定",false);
        addCenterView("审定意见：", false, "缺陷性质：", false);
        addCenterView("审定时间：", true, "审定人：", true);
        //
        addTitleLayout("缺陷安排",false);
        addCenterView("安排意见：", false, "安排人：", true);
        addCenterView("安排时间：", true);
        //
        addTitleLayout("消缺汇报",false);
        addDrawEditTextLayout("缺陷部位（部件）：","处理方案：");
        addDrawEditTextLayout("缺陷原因:","处理结果：");
        addDrawEditTextLayout("责任原因:","技术原因：");
        addCenterView("工作负责人：", true, "延期原因：", false);
        addCenterView("消缺人员：", true, "消缺时间：", true);
        //
        addTitleLayout("缺陷验收",false);
        addDrawEditTextLayout("处理结果：","验收意见：");
        addDrawEditTextLayout("汇报调度情况：（*时*分汇报值班调度员**，无需汇报则空白）","汇报监控情况：（*时*分汇报当值监控员**，无需汇报则空白）");
        addCenterView("验收人：", true, "验收时间：", true);
        addTitleLayout("备注：过热上传红外图谱、带电检测上传相关报告。",true);
        setData();
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(String leftName, boolean leftIsText, String rightName, boolean rightIsText) {
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < 4; i++) {
            DrawTextView drawTextView = null;
            DrawEditText drawEditText = null;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,1);
            switch (i) {
                case 0:
                    drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(leftName);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                    break;
                case 1:
                    if (leftIsText) {
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setLayoutParams(layoutParams);
                        drawTextView.setOnClickListener(this);
                        drawTextView.setId(drawTextViewList.size());
                        drawTextViewList.add(drawTextView);
                        view.addView(drawTextView);
                    } else {
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        drawEditTextList.add(drawEditText);
                        view.addView(drawEditText);
                    }
                    break;
                case 2:
                    drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(rightName);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                    break;
                case 3:
                    if (rightIsText) {
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setLayoutParams(layoutParams);
                        drawTextView.setOnClickListener(this);
                        drawTextView.setId(drawTextViewList.size());
                        drawTextViewList.add(drawTextView);
                        view.addView(drawTextView);
                    } else {
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        drawEditTextList.add(drawEditText);
                        view.addView(drawEditText);
                    }
                    break;
                default:
                    break;
            }
        }
        pLayout.addView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(String leftName, boolean leftIsText) {
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < 2; i++) {
            DrawTextView drawTextView = null;
            DrawEditText drawEditText = null;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,1);
            switch (i) {
                case 0:
                    drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(leftName);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                    break;
                case 1:
                    if (leftIsText) {
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setLayoutParams(layoutParams);
                        drawTextView.setOnClickListener(this);
                        drawTextView.setId(drawTextViewList.size());
                        drawTextViewList.add(drawTextView);
                        view.addView(drawTextView);
                    } else {
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        drawEditTextList.add(drawEditText);
                        view.addView(drawEditText);
                    }
                    break;
                default:
                    break;
            }
        }
        pLayout.addView(view);
    }

    /**
     *
     * @param leftname
     * @param rightname
     */
    private void addDrawEditTextLayout(String leftname,String rightname) {
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,1);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText(leftname);
        drawTextView.setPadding(16, 0, 0, 0);
        drawTextView.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
        drawTextView.setLineShow(true,true,true,false);
        view.addView(drawTextView);
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText(rightname);
        drawTextView.setPadding(16, 0, 0, 0);
        drawTextView.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
        drawTextView.setLineShow(true,true,true,false);
        view.addView(drawTextView);
        pLayout.addView(view);
        //
        view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
        drawEditText.setMinHeight(HHDensityUtils.dip2px(getBaseContext(), 100));
        drawEditText.setLayoutParams(layoutParams);
        drawEditText.setLineShow(true,false,true,true);
        drawEditTextList.add(drawEditText);
        view.addView(drawEditText);
        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
        drawEditText.setMinHeight(HHDensityUtils.dip2px(getBaseContext(), 100));
        drawEditText.setLayoutParams(layoutParams);
        drawEditText.setLineShow(true,false,true,true);
        drawEditTextList.add(drawEditText);
        view.addView(drawEditText);
        pLayout.addView(view);
    }

    /**
     * 添加标题
     * @param name
     * @param isLeft 是否字体居左显示
     */
    private void addTitleLayout(String name,boolean isLeft) {
        DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        drawTextView.setLayoutParams(layoutParams);
        if (isLeft) {
            drawTextView.setPadding(16, 0, 0, 0);
            drawTextView.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
        }
        drawTextView.setText(name);
        pLayout.addView(drawTextView);
    }


    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        Intent intent = null;
        switch (v.getId()) {
            case 0:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 1);
                startActivityForResult(intent, CHOOSEBIANDIAN);
                break;
            case 2:
            case 4:
            case 5:
            case 7:
            case 10:
            case 13:
            case 15:
                HHInputUtils.chooseTime(true, clickText);
                break;
            case 1:
            case 3:
            case 6:
            case 8:
            case 9:
            case 11:
            case 12:
            case 14:
                intent = new Intent(this, LineLockActivity.class);
                intent.putExtra("isChoose", true);
                startActivityForResult(intent, CHOOSENAME);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSENAME:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        YunWeiQueXianModel model = DataSupport.findLast(YunWeiQueXianModel.class);
        if (model != null) {
            drawEditTextList.get(0).setText(model.getQxsb());
            drawEditTextList.get(1).setText(model.getDydj());
            drawEditTextList.get(2).setText(model.getSbscmc());
            drawEditTextList.get(3).setText(model.getQxxz());
            drawEditTextList.get(4).setText(model.getFxfs());
            drawEditTextList.get(5).setText(model.getQxms());
            drawEditTextList.get(6).setText(model.getDxtz());
            drawEditTextList.get(7).setText(model.getHbddqk());
            drawEditTextList.get(8).setText(model.getHbjkqk());
            drawEditTextList.get(9).setText(model.getShyj());
            drawEditTextList.get(10).setText(model.getShqxxz());
            drawEditTextList.get(11).setText(model.getSdyj());
            drawEditTextList.get(12).setText(model.getSdqxxz());
            drawEditTextList.get(13).setText(model.getApyj());
            drawEditTextList.get(14).setText(model.getQxbw());
            drawEditTextList.get(15).setText(model.getClfa());
            drawEditTextList.get(16).setText(model.getQxyy());
            drawEditTextList.get(17).setText(model.getCljg());
            drawEditTextList.get(18).setText(model.getZryy());
            drawEditTextList.get(19).setText(model.getJsyy());
            drawEditTextList.get(20).setText(model.getYqyy());
            drawEditTextList.get(21).setText(model.getQxyscljg());
            drawEditTextList.get(22).setText(model.getYsyj());
            drawEditTextList.get(23).setText(model.getQxyshbddqk());
            drawEditTextList.get(24).setText(model.getQxyshbjkqk());
            //
            drawTextViewList.get(0).setText(model.getBdz());
            drawTextViewList.get(1).setText(model.getFxr());
            drawTextViewList.get(2).setText(model.getFxsj());
            drawTextViewList.get(3).setText(model.getTbr());
            drawTextViewList.get(4).setText(model.getTbsj());
            drawTextViewList.get(5).setText(model.getShsj());
            drawTextViewList.get(6).setText(model.getShr());
            drawTextViewList.get(7).setText(model.getSdsj());
            drawTextViewList.get(8).setText(model.getSdr());
            drawTextViewList.get(9).setText(model.getApr());
            drawTextViewList.get(10).setText(model.getApsj());
            drawTextViewList.get(11).setText(model.getGzfzr());
            drawTextViewList.get(12).setText(model.getXqry());
            drawTextViewList.get(13).setText(model.getXqsj());
            drawTextViewList.get(14).setText(model.getYsr());
            drawTextViewList.get(15).setText(model.getYssj());
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        YunWeiQueXianModel model = new YunWeiQueXianModel();
        model.setQxsb(drawEditTextList.get(0).getText().toString());
        model.setDydj(drawEditTextList.get(1).getText().toString());
        model.setSbscmc(drawEditTextList.get(2).getText().toString());
        model.setQxxz(drawEditTextList.get(3).getText().toString());
        model.setFxfs(drawEditTextList.get(4).getText().toString());
        model.setQxms(drawEditTextList.get(5).getText().toString());
        model.setDxtz(drawEditTextList.get(6).getText().toString());
        model.setHbddqk(drawEditTextList.get(7).getText().toString());
        model.setHbjkqk(drawEditTextList.get(8).getText().toString());
        model.setShyj(drawEditTextList.get(9).getText().toString());
        model.setShqxxz(drawEditTextList.get(10).getText().toString());
        model.setSdyj(drawEditTextList.get(11).getText().toString());
        model.setSdqxxz(drawEditTextList.get(12).getText().toString());
        model.setApyj(drawEditTextList.get(13).getText().toString());
        model.setQxbw(drawEditTextList.get(14).getText().toString());
        model.setClfa(drawEditTextList.get(15).getText().toString());
        model.setQxyy(drawEditTextList.get(16).getText().toString());
        model.setCljg(drawEditTextList.get(17).getText().toString());
        model.setZryy(drawEditTextList.get(18).getText().toString());
        model.setJsyy(drawEditTextList.get(19).getText().toString());
        model.setYqyy(drawEditTextList.get(20).getText().toString());
        model.setQxyscljg(drawEditTextList.get(21).getText().toString());
        model.setYsyj(drawEditTextList.get(22).getText().toString());
        model.setQxyshbddqk(drawEditTextList.get(23).getText().toString());
        model.setQxyshbjkqk(drawEditTextList.get(24).getText().toString());
        //
        model.setBdz(drawTextViewList.get(0).getText().toString());
        model.setFxr(drawTextViewList.get(1).getText().toString());
        model.setFxsj(drawTextViewList.get(2).getText().toString());
        model.setTbr(drawTextViewList.get(3).getText().toString());
        model.setTbsj(drawTextViewList.get(4).getText().toString());
        model.setShsj(drawTextViewList.get(5).getText().toString());
        model.setShr(drawTextViewList.get(6).getText().toString());
        model.setSdsj(drawTextViewList.get(7).getText().toString());
        model.setSdr(drawTextViewList.get(8).getText().toString());
        model.setApr(drawTextViewList.get(9).getText().toString());
        model.setApsj(drawTextViewList.get(10).getText().toString());
        model.setGzfzr(drawTextViewList.get(11).getText().toString());
        model.setXqry(drawTextViewList.get(12).getText().toString());
        model.setXqsj(drawTextViewList.get(13).getText().toString());
        model.setYsr(drawTextViewList.get(14).getText().toString());
        model.setYssj(drawTextViewList.get(15).getText().toString());
        model.save();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
