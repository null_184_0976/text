package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 运维月计划表
 * Created by Administrator on 2018/3/9.
 */

public class YunWeiYModel extends DataSupport {
    private int id;//主键id
    private String bzmc;//		班组名称
    private String yf;//月份
    private String rq;//日期
    private String bdz;//	变电站
    private String gznr;//		工作内容
    private String zrr;//	责任人
    private String gzry;//		工作人员
    private String bz;//备注

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBzmc() {
        return bzmc;
    }

    public void setBzmc(String bzmc) {
        this.bzmc = bzmc;
    }

    public String getYf() {
        return yf;
    }

    public void setYf(String yf) {
        this.yf = yf;
    }

    public String getRq() {
        return rq;
    }

    public void setRq(String rq) {
        this.rq = rq;
    }

    public String getBdz() {
        return bdz;
    }

    public void setBdz(String bdz) {
        this.bdz = bdz;
    }

    public String getGznr() {
        return gznr;
    }

    public void setGznr(String gznr) {
        this.gznr = gznr;
    }

    public String getZrr() {
        return zrr;
    }

    public void setZrr(String zrr) {
        this.zrr = zrr;
    }

    public String getGzry() {
        return gzry;
    }

    public void setGzry(String gzry) {
        this.gzry = gzry;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }
}
