package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 避雷器泄流记录
 *         Created by Administrator on 2018/3/15.
 */

public class BiLeiJiLuModel extends DataSupport {
    private int id;//主键id
    private String bdz;//变电站
    private String jcsj;//检查时间
    private String jcr;//检查人
    private String jcqk;//检查情况
    private String bdzdy;//	变电站单元
    private String sbmc;//设备名称
    private String dydj;//电压等级
    private String xldl;//泄漏电流(mA)
    private String xldlzdz;//	泄漏电流最大值(mA)
    private String scxldlz;//	上次泄漏电流值(mA)
    private String sczsz;//	上次指示值
    private String zsz;//指示值
    private String dzcs;//动作次数
    private String scdzcs;//	上次动作次数
    private String ljdzcs;//	累计动作次数
    private String dzqk;//动作情况
    private String sjlx;//事件类型
    private String bpzdz;//	表盘最大值

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBdz() {
        return bdz;
    }

    public void setBdz(String bdz) {
        this.bdz = bdz;
    }

    public String getJcsj() {
        return jcsj;
    }

    public void setJcsj(String jcsj) {
        this.jcsj = jcsj;
    }

    public String getJcr() {
        return jcr;
    }

    public void setJcr(String jcr) {
        this.jcr = jcr;
    }

    public String getJcqk() {
        return jcqk;
    }

    public void setJcqk(String jcqk) {
        this.jcqk = jcqk;
    }

    public String getBdzdy() {
        return bdzdy;
    }

    public void setBdzdy(String bdzdy) {
        this.bdzdy = bdzdy;
    }

    public String getSbmc() {
        return sbmc;
    }

    public void setSbmc(String sbmc) {
        this.sbmc = sbmc;
    }

    public String getDydj() {
        return dydj;
    }

    public void setDydj(String dydj) {
        this.dydj = dydj;
    }

    public String getXldl() {
        return xldl;
    }

    public void setXldl(String xldl) {
        this.xldl = xldl;
    }

    public String getXldlzdz() {
        return xldlzdz;
    }

    public void setXldlzdz(String xldlzdz) {
        this.xldlzdz = xldlzdz;
    }

    public String getScxldlz() {
        return scxldlz;
    }

    public void setScxldlz(String scxldlz) {
        this.scxldlz = scxldlz;
    }

    public String getSczsz() {
        return sczsz;
    }

    public void setSczsz(String sczsz) {
        this.sczsz = sczsz;
    }

    public String getZsz() {
        return zsz;
    }

    public void setZsz(String zsz) {
        this.zsz = zsz;
    }

    public String getDzcs() {
        return dzcs;
    }

    public void setDzcs(String dzcs) {
        this.dzcs = dzcs;
    }

    public String getScdzcs() {
        return scdzcs;
    }

    public void setScdzcs(String scdzcs) {
        this.scdzcs = scdzcs;
    }

    public String getLjdzcs() {
        return ljdzcs;
    }

    public void setLjdzcs(String ljdzcs) {
        this.ljdzcs = ljdzcs;
    }

    public String getDzqk() {
        return dzqk;
    }

    public void setDzqk(String dzqk) {
        this.dzqk = dzqk;
    }

    public String getSjlx() {
        return sjlx;
    }

    public void setSjlx(String sjlx) {
        this.sjlx = sjlx;
    }

    public String getBpzdz() {
        return bpzdz;
    }

    public void setBpzdz(String bpzdz) {
        this.bpzdz = bpzdz;
    }
}
