package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 仪器借用记录
 *         Created by Administrator on 2018/3/11.
 */

public class YiQiJieYongModel extends DataSupport {
    private int id;//主键id
    private String bzmc;//		班站名称
    private String mc;//名称
    private String ggxh;//		规格型号
    private String yt;//用途
    private String sl;//数量
    private String dw;//单位
    private String lyr;//	领用人
    private String lyrq;//		领用日期
    private String ghrq;//		归还日期

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBzmc() {
        return bzmc;
    }

    public void setBzmc(String bzmc) {
        this.bzmc = bzmc;
    }

    public String getMc() {
        return mc;
    }

    public void setMc(String mc) {
        this.mc = mc;
    }

    public String getGgxh() {
        return ggxh;
    }

    public void setGgxh(String ggxh) {
        this.ggxh = ggxh;
    }

    public String getYt() {
        return yt;
    }

    public void setYt(String yt) {
        this.yt = yt;
    }

    public String getSl() {
        return sl;
    }

    public void setSl(String sl) {
        this.sl = sl;
    }

    public String getDw() {
        return dw;
    }

    public void setDw(String dw) {
        this.dw = dw;
    }

    public String getLyr() {
        return lyr;
    }

    public void setLyr(String lyr) {
        this.lyr = lyr;
    }

    public String getLyrq() {
        return lyrq;
    }

    public void setLyrq(String lyrq) {
        this.lyrq = lyrq;
    }

    public String getGhrq() {
        return ghrq;
    }

    public void setGhrq(String ghrq) {
        this.ghrq = ghrq;
    }
}
