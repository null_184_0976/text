package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 巡视危险点分析与预防控制措施
 *         Created by Administrator on 2018/3/13.
 */

public class WeiXianDianModel extends DataSupport {
    private int id;//主键id
    private String fflx;//防范类型
    private String wxd;//危险点
    private String yfkzcs;//	预防控制措施

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFflx() {
        return fflx;
    }

    public void setFflx(String fflx) {
        this.fflx = fflx;
    }

    public String getWxd() {
        return wxd;
    }

    public void setWxd(String wxd) {
        this.wxd = wxd;
    }

    public String getYfkzcs() {
        return yfkzcs;
    }

    public void setYfkzcs(String yfkzcs) {
        this.yfkzcs = yfkzcs;
    }
}
