package com.mtjsoft.www.myapplication.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.model.YiQiTaiZhangModel;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mtj 仪器仪表及工器具台账记录
 *         Created by Administrator on 2018/3/9.
 */

public class YiQiTaiZhangActivity extends BaseActivity {

    private static final int CHOOSEBIANDIAN = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private int col = 0;
    private int row = 20;
    private List<YiQiTaiZhangModel> yiQiTaiZhangModelList;
    private String zhan = "___";

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setOnClickListener(this);
        setTopName(String.format(getString(R.string.yiqitaizhang_title), zhan));
        addTableTitle();
        readData();
        addCenterView(row);
        setData();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        removeTopLayoutViewAll();
        String[] titles = getResources().getStringArray(R.array.yiqi_taizhang);
        col = titles.length;
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < col; i++) {
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
            drawTextView.setLayoutParams(layoutParams);
            drawTextView.setText(titles[i]);
            view.addView(drawTextView);
        }
        addTopLayoutView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(int row) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
        for (int j = 0; j < row; j++) {
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            for (int i = 0; i < col; i++) {
                if (i == 5) {
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setTag(i);
                    drawTextView.setOnClickListener(this);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                } else {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    drawEditText.setLayoutParams(layoutParams);
                    if (i == 0) {
                        drawEditText.setText((pLayout.getChildCount() + 1) + "");
                        drawEditText.setEnabled(false);
                    }
                    view.addView(drawEditText);
                }
            }
            pLayout.addView(view);
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        if (v.getId() == R.id.iv_add_row) {
            addCenterView(5);
            return;
        }
        if (v.getId() == R.id.tv_base_top_name) {
            Intent intent = new Intent(this, ChooseActivity.class);
            intent.putExtra("type", 1);
            startActivityForResult(intent, CHOOSEBIANDIAN);
            return;
        }
        int tag = (int) v.getTag();
        switch (tag) {
            case 5:
                chooseTime();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            zhan = name;
                            clickText.setText(String.format(getString(R.string.yiqitaizhang_title), zhan));
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 选择日期
     */
    private void chooseTime() {
        //时间选择器
        TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                clickText.setText(TurnsUtils.convertToString(date,"yyyy-MM-dd"));
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(new boolean[]{true, true, true, false, false, false})
                .build();
        pvTime.show();
    }

    /**
     * 读取数据
     */
    private void readData() {
        yiQiTaiZhangModelList = DataSupport.findAll(YiQiTaiZhangModel.class);
        if (yiQiTaiZhangModelList != null && yiQiTaiZhangModelList.size() > row) {
            row = yiQiTaiZhangModelList.size();
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        if (yiQiTaiZhangModelList != null && yiQiTaiZhangModelList.size() > 0) {
            for (int i = 0, plCh = yiQiTaiZhangModelList.size(); i < plCh; i++) {
                YiQiTaiZhangModel model = yiQiTaiZhangModelList.get(i);
                zhan = model.getBzmc();
                setTopName(String.format(getString(R.string.yiqitaizhang_title), zhan));
                if (pLayout.getChildAt(i) instanceof LinearLayout) {
                    LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                    ((DrawEditText) linearLayout.getChildAt(1)).setText(model.getMc());
                    ((DrawEditText) linearLayout.getChildAt(2)).setText(model.getGgxh());
                    ((DrawEditText) linearLayout.getChildAt(3)).setText(model.getBh());
                    ((DrawEditText) linearLayout.getChildAt(4)).setText(model.getSccj());
                    ((DrawTextView) linearLayout.getChildAt(5)).setText(model.getCcrq());
                }
            }
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        DataSupport.deleteAll(YiQiTaiZhangModel.class);
        List<YiQiTaiZhangModel> yiQiTaiZhangModelList = new ArrayList<>();
        for (int i = 0, plCh = pLayout.getChildCount(); i < plCh; i++) {
            YiQiTaiZhangModel model = new YiQiTaiZhangModel();
            model.setBzmc(zhan);
            if (pLayout.getChildAt(i) instanceof LinearLayout) {
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                DrawEditText drawTextView = (DrawEditText) linearLayout.getChildAt(1);
                if (!TextUtils.isEmpty(drawTextView.getText().toString())) {
                    model.setMc(drawTextView.getText().toString());
                    model.setGgxh(((DrawEditText) linearLayout.getChildAt(2)).getText().toString());
                    model.setBh(((DrawEditText) linearLayout.getChildAt(3)).getText().toString());
                    model.setSccj(((DrawEditText) linearLayout.getChildAt(4)).getText().toString());
                    model.setCcrq(((DrawTextView) linearLayout.getChildAt(5)).getText().toString());
                    yiQiTaiZhangModelList.add(model);
                }
            }
        }
        DataSupport.saveAll(yiQiTaiZhangModelList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
