package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 事故预想记录
 *         Created by Administrator on 2018/3/16.
 */

public class YunWeiShiGuModel extends DataSupport {
    private int id;//主键id
    private String bzmc;//班站名称
    private String zcr;//主持人
    private String hdsj;//活动时间
    private String jlsj;//记录时间
    private String cjry;//参加人员
    private String yxsgqk;//	预想事故情况
    private String yxfs;//运行方式
    private String sgclgc;//	事故处理过程
    private String dtsj;//答题时间
    private String dtr;//答题人
    private String pj;//评价
    private String bzpyr;//	班组评议人
    private String pysj;//评议时间
    private String pxzz;//培训专责
    private String pjsj;//评价时间
    private String bz;//备注

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBzmc() {
        return bzmc;
    }

    public void setBzmc(String bzmc) {
        this.bzmc = bzmc;
    }

    public String getZcr() {
        return zcr;
    }

    public void setZcr(String zcr) {
        this.zcr = zcr;
    }

    public String getHdsj() {
        return hdsj;
    }

    public void setHdsj(String hdsj) {
        this.hdsj = hdsj;
    }

    public String getJlsj() {
        return jlsj;
    }

    public void setJlsj(String jlsj) {
        this.jlsj = jlsj;
    }

    public String getCjry() {
        return cjry;
    }

    public void setCjry(String cjry) {
        this.cjry = cjry;
    }

    public String getYxsgqk() {
        return yxsgqk;
    }

    public void setYxsgqk(String yxsgqk) {
        this.yxsgqk = yxsgqk;
    }

    public String getYxfs() {
        return yxfs;
    }

    public void setYxfs(String yxfs) {
        this.yxfs = yxfs;
    }

    public String getSgclgc() {
        return sgclgc;
    }

    public void setSgclgc(String sgclgc) {
        this.sgclgc = sgclgc;
    }

    public String getDtsj() {
        return dtsj;
    }

    public void setDtsj(String dtsj) {
        this.dtsj = dtsj;
    }

    public String getDtr() {
        return dtr;
    }

    public void setDtr(String dtr) {
        this.dtr = dtr;
    }

    public String getPj() {
        return pj;
    }

    public void setPj(String pj) {
        this.pj = pj;
    }

    public String getBzpyr() {
        return bzpyr;
    }

    public void setBzpyr(String bzpyr) {
        this.bzpyr = bzpyr;
    }

    public String getPysj() {
        return pysj;
    }

    public void setPysj(String pysj) {
        this.pysj = pysj;
    }

    public String getPxzz() {
        return pxzz;
    }

    public void setPxzz(String pxzz) {
        this.pxzz = pxzz;
    }

    public String getPjsj() {
        return pjsj;
    }

    public void setPjsj(String pjsj) {
        this.pjsj = pjsj;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }
}
