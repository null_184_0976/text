package com.mtjsoft.www.myapplication.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.GuiChengShenPiModel;
import com.mtjsoft.www.myapplication.utils.HHDensityUtils;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mtj 变电站现场运行规程编制（修订）审批表
 *         Created by Administrator on 2018/3/11.
 */

public class GuiChengShenPiActivity extends BaseActivity {

    private static final int CHOOSEName = 1001;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private DrawTextView clickText;
    private List<DrawEditText> drawEditTextList = new ArrayList<>();
    private List<DrawTextView> drawTextViewList = new ArrayList<>();

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setVisibility(View.GONE);
        setTopName("变电站现场运行规程编制（修订）审批表");
        getTopNameTextView().setOnClickListener(null);
        addTableTitle();
        addCenterView();
        readData();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        removeTopLayoutViewAll();
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        LinearLayout.LayoutParams layoutParams = null;
        DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText("编号：");
        view.addView(drawTextView);
        //
        DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 3);
        drawEditText.setLayoutParams(layoutParams);
        view.addView(drawEditText);
        drawEditTextList.add(drawEditText);
        addTopLayoutView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView() {
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        LinearLayout.LayoutParams layoutParams = null;
        DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText("规程名称：");
        view.addView(drawTextView);
        DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 3);
        drawEditText.setLayoutParams(layoutParams);
        view.addView(drawEditText);
        drawEditTextList.add(drawEditText);
        pLayout.addView(view);
        //
        view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText("修编原因:");
        view.addView(drawTextView);
        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 3);
        drawEditText.setLayoutParams(layoutParams);
        view.addView(drawEditText);
        drawEditTextList.add(drawEditText);
        pLayout.addView(view);
        //
        view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText("修编内容（修编范围及主要条款）：");
        view.addView(drawTextView);
        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
        drawEditText.setMinHeight(HHDensityUtils.dip2px(getBaseContext(), 100));
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 3);
        drawEditText.setLayoutParams(layoutParams);
        view.addView(drawEditText);
        drawEditTextList.add(drawEditText);
        pLayout.addView(view);
        //
        addQianZiYiJian("编写人员（签字）：", "编写负责人：");
        //
        addQianZiYiJian("会审人员（签字）：", "");
        //
        addQianZiYiJian("安质部意见：", "审核人：");
        //
        addQianZiYiJian("调控中心意见：", "审核人：");
        //
        addQianZiYiJian("运检部意见：", "审核人：");
        //
        addQianZiYiJian("分管领导意见：", "签发人：");
    }

    private void addQianZiYiJian(String title, String fuzeren) {
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setLineShow(true, true, false, false);
        drawTextView.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText(title);
        view.addView(drawTextView);
        DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
        drawEditText.setGravity(Gravity.TOP);
        drawEditText.setLineShow(false, true, true, false);
        drawEditText.setMinHeight(HHDensityUtils.dip2px(getBaseContext(), 100));
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 3);
        drawEditText.setLayoutParams(layoutParams);
        view.addView(drawEditText);
        drawEditTextList.add(drawEditText);
        pLayout.addView(view);
        //
        view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setLineShow(true, false, false, true);
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText(fuzeren);
        view.addView(drawTextView);
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setLineShow(false, false, false, true);
        drawTextView.setLayoutParams(layoutParams);
        view.addView(drawTextView);
        if (!TextUtils.isEmpty(fuzeren)) {
            drawTextView.setTag("chooseName");
            drawTextView.setOnClickListener(this);
            drawTextView.setId(drawTextViewList.size());
            drawTextViewList.add(drawTextView);
        }
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setLineShow(false, false, false, true);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText("日期：");
        view.addView(drawTextView);
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setLineShow(false, false, true, true);
        drawTextView.setLayoutParams(layoutParams);
        view.addView(drawTextView);
        drawTextView.setTag("chooseTime");
        drawTextView.setId(drawTextViewList.size());
        drawTextView.setOnClickListener(this);
        drawTextViewList.add(drawTextView);
        pLayout.addView(view);
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = drawTextViewList.get(v.getId());
        }
        if ("chooseName".equals(v.getTag())) {
            Intent intent = new Intent(this, LineLockActivity.class);
            intent.putExtra("isChoose", true);
            startActivityForResult(intent, CHOOSEName);
            return;
        }
        if ("chooseTime".equals(v.getTag())) {
            chooseTime();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSEName:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 选择日期
     */
    private void chooseTime() {
        //时间选择器
        TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                clickText.setText(TurnsUtils.convertToString(date,"yyyy年MM月dd日"));
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(new boolean[]{true, true, true, false, false, false})
                .build();
        pvTime.show();
    }

    /**
     * 读取数据
     */
    private void readData() {
        GuiChengShenPiModel model = DataSupport.findLast(GuiChengShenPiModel.class);
        if (model != null) {
            drawEditTextList.get(0).setText(model.getBh());
            drawEditTextList.get(1).setText(model.getGcmc());
            drawEditTextList.get(2).setText(model.getXbyy());
            drawEditTextList.get(3).setText(model.getXbnr());
            drawEditTextList.get(4).setText(model.getBxry());
            drawEditTextList.get(5).setText(model.getHsry());
            drawEditTextList.get(6).setText(model.getAzbyj());
            drawEditTextList.get(7).setText(model.getTkzxyj());
            drawEditTextList.get(8).setText(model.getYjbyj());
            drawEditTextList.get(9).setText(model.getFgldyj());
            //
            drawTextViewList.get(0).setText(model.getBxfzr());
            drawTextViewList.get(1).setText(model.getBxqzrq());
            drawTextViewList.get(2).setText(model.getHsqzrq());
            drawTextViewList.get(3).setText(model.getAzshr());
            drawTextViewList.get(4).setText(model.getAzqzrq());
            drawTextViewList.get(5).setText(model.getTkshr());
            drawTextViewList.get(6).setText(model.getTkqzrq());
            drawTextViewList.get(7).setText(model.getYjbshr());
            drawTextViewList.get(8).setText(model.getYjqzrq());
            drawTextViewList.get(9).setText(model.getFgqfr());
            drawTextViewList.get(10).setText(model.getFgqzrq());
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        GuiChengShenPiModel model = new GuiChengShenPiModel();
        model.setBh(drawEditTextList.get(0).getText().toString());
        model.setGcmc(drawEditTextList.get(1).getText().toString());
        model.setXbyy(drawEditTextList.get(2).getText().toString());
        model.setXbnr(drawEditTextList.get(3).getText().toString());
        model.setBxry(drawEditTextList.get(4).getText().toString());
        model.setHsry(drawEditTextList.get(5).getText().toString());
        model.setAzbyj(drawEditTextList.get(6).getText().toString());
        model.setTkzxyj(drawEditTextList.get(7).getText().toString());
        model.setYjbyj(drawEditTextList.get(8).getText().toString());
        model.setFgldyj(drawEditTextList.get(9).getText().toString());
        //
        model.setBxfzr(drawTextViewList.get(0).getText().toString());
        model.setBxqzrq(drawTextViewList.get(1).getText().toString());
        model.setHsqzrq(drawTextViewList.get(2).getText().toString());
        model.setAzshr(drawTextViewList.get(3).getText().toString());
        model.setAzqzrq(drawTextViewList.get(4).getText().toString());
        model.setTkshr(drawTextViewList.get(5).getText().toString());
        model.setTkqzrq(drawTextViewList.get(6).getText().toString());
        model.setYjbshr(drawTextViewList.get(7).getText().toString());
        model.setYjqzrq(drawTextViewList.get(8).getText().toString());
        model.setFgqfr(drawTextViewList.get(9).getText().toString());
        model.setFgqzrq(drawTextViewList.get(10).getText().toString());
        model.saveOrUpdate("bh = ?", model.getBh());
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
