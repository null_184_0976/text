package com.mtjsoft.www.myapplication.ui;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bigkoo.pickerview.OptionsPickerView;
import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.XdcDModel;
import com.mtjsoft.www.myapplication.model.XdcZModel;
import com.mtjsoft.www.myapplication.utils.HHDensityUtils;
import com.mtjsoft.www.myapplication.utils.HHScreenUtils;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;
import com.mtjsoft.www.myapplication.utils.UserInfoUtils;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mtj  012蓄电池检测记录
 * @date 2018-3-6
 * <p>
 * zm 站名
 */
public class XuDianChiActivity extends BaseActivity {

    private static final int CHOOSE_USER = 1000;
    private static final int CHOOSE_USER_FUZE = 1001;
    private LinearLayout linearLayoutTop;
    private LinearLayout linearLayoutCenter;
    private LinearLayout linearLayoutBottom;
    private List<DrawTextView> listTextView = new ArrayList<>();
    private DrawTextView clickText;
    private int width;
    /**
     * 蓄电池总数
     */
    private int countDC = 104;
    /**
     * 最后一个电压的id
     */
    private int firstDyId = 0;
    private int lostDyId = 0;
    /**
     * 行列数
     */
    int col = 0;
    int row = 0;
    /**
     * 电压最小值和最大
     */
    private double minDy = 05.00;

    private double maxDy = 24.00;
    /**
     * 站名
     */
    private String zm = "嵩山站蓄电池检测记录表";

    private XdcZModel xdcZModel;

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_xudianchi, null);
        linearLayoutTop = view.findViewById(R.id.ll_base_top);
        linearLayoutCenter = view.findViewById(R.id.ll_base_center);
        linearLayoutBottom = view.findViewById(R.id.ll_base_bottom);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void initView() {
        width = HHScreenUtils.getScreenWidth(getBaseContext()) - HHDensityUtils.dip2px(getBaseContext(), 20);
        setTopName(zm);
        String count = UserInfoUtils.getUserInfo(getBaseContext(), UserInfoUtils.LOST_DIAN_CHI_COUNT);
        if (!TextUtils.isEmpty(count)) {
            countDC = TurnsUtils.getInt(count, 104);
        }
        addTopView();
        addCtenterView();
        addBottomView();
        readData();
    }

    /**
     * 添加头部表
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void addTopView() {
        String[] nameTitles = getResources().getStringArray(R.array.xu_dian_chi);
        for (int i = 0; i < 5; i++) {
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            if (i == 4) {
                DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                drawTextView.setLayoutParams(layoutParams);
                drawTextView.setText(getString(R.string.xdc_dy));
                view.addView(drawTextView);
            } else {
                for (int j = 0; j < 6; j++) {
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                    drawTextView.setLayoutParams(layoutParams);
                    if ((j + 1) % 2 == 0) {
                        drawTextView.setId(listTextView.size());
                        drawTextView.setOnClickListener(this);
                        drawTextView.setTag("top");
                        listTextView.add(drawTextView);
                    } else {
                        drawTextView.setText(nameTitles[(i * 3 + j / 2)]);
                    }
                    if ((i * 6 + j) == 3) {
                        drawTextView.setText(countDC + "");
                    } else if ((i * 6 + j) == 7) {
                        drawTextView.setText(TurnsUtils.convertToString(new Date(),"yyyy-MM-dd"));
                    }
                    view.addView(drawTextView);
                }
            }
            linearLayoutTop.addView(view);
        }
        firstDyId = listTextView.size() - 1;
    }

    /**
     * 添加中间电压部分
     */
    private void addCtenterView() {
        if (lostDyId > 0 && firstDyId > 0 && lostDyId > firstDyId) {
            for (int i = 0; i < lostDyId - firstDyId; i++) {
                listTextView.remove(firstDyId + 1);
            }
        }
        linearLayoutCenter.removeAllViews();
        row = (countDC / 6) + (countDC % 6 == 0 ? 0 : 1);
        col = 12;
        for (int j = 0; j < col; j++) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_vertical_linearlayout_w_w, null);
            view.setLayoutParams(layoutParams);
            for (int i = 0; i <= row; i++) {
                if (i == 0) {
                    String title = (j + 1) % 2 == 0 ? getString(R.string.dianya) : getString(R.string.xuhao);
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    drawTextView.setLayoutParams(layoutParams);
                    drawTextView.setText(title);
                    view.addView(drawTextView);
                } else {
                    String title = (j + 1) % 2 == 0 ? "" : "" + (i + (j / 2) * row);
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    drawTextView.setLayoutParams(layoutParams);
                    if ((i + (j / 2) * row) > countDC) {
                        drawTextView.setText("");
                    } else {
                        drawTextView.setText(title);
                        if ((j + 1) % 2 == 0) {
                            drawTextView.setOnClickListener(this);
                            drawTextView.setTag("DYZ");
                            drawTextView.setId((firstDyId + (i + (j / 2) * row)));
                            listTextView.add((firstDyId + (i + (j / 2) * row)), drawTextView);
                            lostDyId = firstDyId + (i + (j / 2) * row);
                            drawTextView.setTextSize(12);
                        }
                    }
                    view.addView(drawTextView);
                }
            }
            linearLayoutCenter.addView(view);
        }
    }

    /**
     * 添加底部布局
     */
    private void addBottomView() {
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 1; i <= 4; i++) {
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            drawTextView.setLayoutParams(layoutParams);
            switch (i) {
                case 1:
                    drawTextView.setText(getString(R.string.ceshiren));
                    break;
                case 2:
                    drawTextView.setTag("userName");
                    break;
                case 3:
                    drawTextView.setText(getString(R.string.ceshiyuan));
                    break;
                case 4:
                    drawTextView.setTag("userChoose");
                    break;
                default:
                    break;
            }
            if (i % 2 == 0) {
                drawTextView.setOnClickListener(this);
                drawTextView.setId(listTextView.size());
                listTextView.add(drawTextView);
            }
            view.addView(drawTextView);
        }
        linearLayoutBottom.addView(view);
        //分析结论
        view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText(getString(R.string.fenxijielun));
        view.addView(drawTextView);
        //分析结论内容
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 3);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setId(listTextView.size());
        view.addView(drawTextView);
        listTextView.add(drawTextView);
        linearLayoutBottom.addView(view);
    }

    /**
     * 点击监听
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        clickText = (DrawTextView) v;
        dianYaChooseColor(v.getId());
        //选择电压值
        if ("DYZ".equals(clickText.getTag()) || "DYY".equals(clickText.getTag())) {
            String s = clickText.getText().toString().trim();
            if (!TextUtils.isEmpty(s)) {
                String[] nums = s.split("\\.");
                chooseOptions("", TurnsUtils.getInt(nums[0], 0), TurnsUtils.getInt(nums[1], 0));
            } else {
                chooseOptions("", 0, 0);
            }
            return;
        }
        //选择测试负责人
        if ("userName".equals(clickText.getTag())) {
            Intent intent = new Intent(this, LineLockActivity.class);
            intent.putExtra("isChoose", true);
            startActivityForResult(intent, CHOOSE_USER_FUZE);
            return;
        }
        //选择测试成员
        if ("userChoose".equals(clickText.getTag())) {
            Intent intent = new Intent(this, LineLockActivity.class);
            intent.putExtra("isChoose", true);
            intent.putExtra("chooseNames", clickText.getText().toString());
            startActivityForResult(intent, CHOOSE_USER);
            return;
        }
        switch (v.getId()) {
            case 0:
                //运行编号
                chooseBianhao(clickText.getText().toString());
                break;
            case 1:
                //选择全电池数
                chooseDianChiShu(TurnsUtils.getInt(clickText.getText().toString(), 0));
                break;
            case 2:
                //型号
                chooseBianhao(clickText.getText().toString());
                break;
            case 3:
                //日期选择
                chooseTime();
                break;
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                //选择顶部表的电压电流和温度
                String s = clickText.getText().toString().trim();
                if (TextUtils.isEmpty(s)) {
                    chooseOptions("", 0, 0);
                } else {
                    String[] nums = s.split("\\.");
                    chooseOptions("", TurnsUtils.getInt(nums[0], 0), TurnsUtils.getInt(nums[1], 0));
                }
                break;
            case 11:
                //测试分类
                chooseBianhao(clickText.getText().toString());
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            switch (requestCode) {
                case CHOOSE_USER:
                    //测试成员
                    if (clickText != null) {
                        String s = listTextView.get((int) clickText.getId() - 1).getText().toString();
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(s) && (!TextUtils.isEmpty(name) && name.contains(s))) {
                            Toast.makeText(getBaseContext(), getString(R.string.ceshiyuan_error), Toast.LENGTH_LONG).show();
                        } else {
                            clickText.setText(name.substring(0, name.length() - 1));
                        }
                    }
                    break;
                case CHOOSE_USER_FUZE:
                    //测试负责人
                    if (clickText != null) {
                        String s = listTextView.get((int) clickText.getId() + 1).getText().toString();
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(s) && (!TextUtils.isEmpty(name) && s.contains(name))) {
                            Toast.makeText(getBaseContext(), getString(R.string.ceshiren_error), Toast.LENGTH_LONG).show();
                        } else {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                default:
                    break;
            }
            saveData();
        }
    }

    /**
     * 选择日期
     */
    private void chooseTime() {
        //时间选择器
        TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                clickText.setText(TurnsUtils.convertToString(date,"yyyy-MM-dd"));
                saveData();
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(new boolean[]{true, true, true, false, false, false})
                .build();
        pvTime.show();
    }

    /**
     * 电压条件选择
     *
     * @param dw
     */
    private void chooseOptions(final String dw, final int option1, int option2) {
        //添加数据源
        final List<String> options1Items = new ArrayList<>();
        final List<String> options2Items = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            if (i < 10) {
                options1Items.add("0" + i);
                options2Items.add("0" + i);
            } else {
                options1Items.add("" + i);
                options2Items.add("" + i);
            }
        }
        OptionsPickerView pvOptions = new OptionsPickerView.Builder(this, new OptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int options2, int options3, View v) {
                //返回的分别是三个级别的选中位置
                clickText.setText(options1Items.get(options1) + "." + options2Items.get(options2) + dw);
                dianYaChooseColor(clickText.getId());
                saveData();
            }
        })
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(false)
                //设置选择的三级单位
                .setLabels(".", dw, "")
                //循环与否
                .setCyclic(false, false, false)
                //设置默认选中项
                .setSelectOptions(option1, option2, 1)
                //点击外部dismiss default true
                .setOutSideCancelable(true)
                //是否显示为对话框样式
                .isDialog(true)
                .build();
        pvOptions.setNPicker(options1Items, options2Items, null);
        pvOptions.show();
    }

    /**
     * 全电池数条件选择
     */
    private void chooseDianChiShu(final int option1) {
        //添加数据源
        final List<String> options1Items = new ArrayList<>();
        for (int i = 0; i < 151; i++) {
            options1Items.add("" + i);
        }
        OptionsPickerView pvOptions = new OptionsPickerView.Builder(this, new OptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int options2, int options3, View v) {
                //返回的分别是三个级别的选中位置
                //清空提示信息和结论
                setTopMsg("");
                listTextView.get(listTextView.size() - 1).setText("");
                clickText.setText(options1Items.get(options1));
                UserInfoUtils.saveUserInfo(getBaseContext(), UserInfoUtils.LOST_DIAN_CHI_COUNT, options1Items.get(options1));
                countDC = TurnsUtils.getInt(options1Items.get(options1), 120);
                addCtenterView();
                saveData();
            }
        })
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(false)
                //设置选择的三级单位
                .setLabels("", "", "")
                //循环与否
                .setCyclic(false, false, false)
                //设置默认选中项
                .setSelectOptions(option1, 1, 1)
                //点击外部dismiss default true
                .setOutSideCancelable(true)
                //是否显示为对话框样式
                .isDialog(true)
                .build();
        pvOptions.setNPicker(options1Items, null, null);
        pvOptions.show();
    }

    /**
     * 编号条件选择
     */
    private void chooseBianhao(String text) {
        int option1 = 0;
        //添加数据源
        String[] ss = getResources().getStringArray(R.array.yunxingNums);
        final List<String> options1Items = new ArrayList<>();
        for (int i = 0; i < ss.length; i++) {
            options1Items.add(ss[i]);
            if (ss[i].equals(text)) {
                option1 = i;
            }
        }
        OptionsPickerView pvOptions = new OptionsPickerView.Builder(this, new OptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int options2, int options3, View v) {
                //返回的分别是三个级别的选中位置
                clickText.setText(options1Items.get(options1));
                saveData();
            }
        })
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(false)
                //设置选择的三级单位
                .setLabels("", "", "")
                //循环与否
                .setCyclic(false, false, false)
                //设置默认选中项
                .setSelectOptions(option1, 1, 1)
                //点击外部dismiss default true
                .setOutSideCancelable(true)
                //是否显示为对话框样式
                .isDialog(true)
                .build();
        pvOptions.setNPicker(options1Items, null, null);
        pvOptions.show();
    }

    /**
     * 设置电压值显示的颜色。正常主题色；异常红色；
     *
     * @param id
     */
    private void dianYaChooseColor(int id) {
        StringBuffer resultDy = new StringBuffer();
        for (int i = 0; i < listTextView.size(); i++) {
            //选择电压值
            DrawTextView textView = listTextView.get(i);
            textView.setBackgroundColor(getResources().getColor(R.color.white));
            if ("DYZ".equals(textView.getTag())) {
                String s = textView.getText().toString();
                if (!TextUtils.isEmpty(s)) {
                    double dy = TurnsUtils.getDouble(s.substring(0, s.length() - 1), 0);
                    if (dy >= minDy && dy <= maxDy) {
                        textView.setTag("DYZ");
                        if (i <= id) {
                            textView.setTextColor(getResources().getColor(R.color.main_base_color));
                        }
                    } else {
                        textView.setTag("DYY");
                        textView.setTextColor(getResources().getColor(R.color.startColor));
                        String tomMsg = String.format(getString(R.string.dyy), (i - firstDyId) + "", minDy + "", maxDy + "");
                        resultDy.append(tomMsg);
                    }
                }
            } else if ("DYY".equals(textView.getTag())) {
                String s = textView.getText().toString();
                if (!TextUtils.isEmpty(s)) {
                    double dy = TurnsUtils.getDouble(s.substring(0, s.length() - 1), 0);
                    if (dy >= minDy && dy <= maxDy) {
                        textView.setTag("DYZ");
                        if (i <= id) {
                            textView.setTextColor(getResources().getColor(R.color.main_base_color));
                        }
                    } else {
                        textView.setTextColor(getResources().getColor(R.color.startColor));
                        String tomMsg = String.format(getString(R.string.dyy), (i - firstDyId) + "", minDy + "", maxDy + "");
                        resultDy.append(tomMsg);
                    }
                }
            }
            if (clickText != null && clickText.getId() == i) {
                textView.setBackgroundColor(getResources().getColor(R.color.endColor));
            }
        }
        listTextView.get(listTextView.size() - 1).setText(TextUtils.isEmpty(resultDy.toString()) ? getString(R.string.dyz) : resultDy.toString());
        if (clickText != null && !TextUtils.isEmpty(resultDy.toString()) && ("DYZ".equals(clickText.getTag()) || "DYY".equals(clickText.getTag()))) {
            Toast.makeText(getBaseContext(), resultDy.toString(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * 读取数据
     */
    private void readData() {
        xdcZModel = DataSupport.findLast(XdcZModel.class);
        if (xdcZModel != null) {
            //电压表数据
            List<XdcDModel> xdcDModelList = DataSupport.where("xdcZId = ?", xdcZModel.getId() + "").order("xh asc").find(XdcDModel.class);
            for (int i = 0; i < listTextView.size(); i++) {
                DrawTextView drawTextView = listTextView.get(i);
                switch (i) {
                    case 0:
                        drawTextView.setText(xdcZModel.getYxbh());
                        break;
                    case 1:
                        drawTextView.setText(xdcZModel.getQdcs());
                        break;
                    case 2:
                        drawTextView.setText(xdcZModel.getXh());
                        break;
                    case 3:
                        drawTextView.setText(xdcZModel.getJcrq());
                        break;
                    case 4:
                        drawTextView.setText(xdcZModel.getHjwd());
                        break;
                    case 5:
                        drawTextView.setText(xdcZModel.getScqdcfcdy());
                        break;
                    case 6:
                        drawTextView.setText(xdcZModel.getZlxtjcqdcfcdy());
                        break;
                    case 7:
                        drawTextView.setText(xdcZModel.getFhdl());
                        break;
                    case 8:
                        drawTextView.setText(xdcZModel.getFcdl());
                        break;
                    case 9:
                        drawTextView.setText(xdcZModel.getZmxdddyz());
                        break;
                    case 10:
                        drawTextView.setText(xdcZModel.getFmxdddyz());
                        break;
                    case 11:
                        drawTextView.setText(xdcZModel.getCsfl());
                        break;
                    default:
                        if ("DYZ".equals(drawTextView.getTag()) || "DYY".equals(drawTextView.getTag())) {
                            drawTextView.setText(xdcDModelList.get(i - 12).getDy());
                        } else if ("userName".equals(drawTextView.getTag())) {
                            drawTextView.setText(xdcZModel.getCsfzr());
                        } else if ("userChoose".equals(drawTextView.getTag())) {
                            drawTextView.setText(xdcZModel.getCscy());
                        } else if (i == listTextView.size() - 1) {
                            drawTextView.setText(xdcZModel.getFxjl());
                        }
                        break;
                }
            }
            int choose = TurnsUtils.getInt(xdcZModel.getClickTextId(), -1);
            if (choose > 0 && choose < listTextView.size()) {
                clickText = listTextView.get(choose);
            }
            dianYaChooseColor(choose == 0 ? -1 : choose);
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        List<XdcDModel> listD = new ArrayList<>();
        int xh = 1;
        XdcZModel xdcZModel = new XdcZModel();
        xdcZModel.setZm(zm);
        if (clickText != null) {
            xdcZModel.setClickTextId(clickText.getId() + "");
        }
        for (int i = 0; i < listTextView.size(); i++) {
            DrawTextView drawTextView = listTextView.get(i);
            String text = drawTextView.getText().toString();
            switch (i) {
                case 0:
                    xdcZModel.setYxbh(text);
                    break;
                case 1:
                    xdcZModel.setQdcs(text);
                    break;
                case 2:
                    xdcZModel.setXh(text);
                    break;
                case 3:
                    xdcZModel.setJcrq(text);
                    break;
                case 4:
                    xdcZModel.setHjwd(text);
                    break;
                case 5:
                    xdcZModel.setScqdcfcdy(text);
                    break;
                case 6:
                    xdcZModel.setZlxtjcqdcfcdy(text);
                    break;
                case 7:
                    xdcZModel.setFhdl(text);
                    break;
                case 8:
                    xdcZModel.setFcdl(text);
                    break;
                case 9:
                    xdcZModel.setZmxdddyz(text);
                    break;
                case 10:
                    xdcZModel.setFmxdddyz(text);
                    break;
                case 11:
                    xdcZModel.setCsfl(text);
                    break;
                default:
                    if ("DYZ".equals(drawTextView.getTag()) || "DYY".equals(drawTextView.getTag())) {
                        XdcDModel model = new XdcDModel();
                        model.setXh(xh);
                        model.setDy(text);
                        listD.add(model);
                        xh++;
                    } else if ("userName".equals(drawTextView.getTag())) {
                        xdcZModel.setCsfzr(text);
                    } else if ("userChoose".equals(drawTextView.getTag())) {
                        xdcZModel.setCscy(text);
                    } else if (i == listTextView.size() - 1) {
                        xdcZModel.setFxjl(text);
                    }
                    break;
            }
        }
        List<XdcZModel> chaResults = DataSupport.where("jcrq = ?", xdcZModel.getJcrq()).find(XdcZModel.class);
        if (chaResults == null || chaResults.size() == 0) {
            //不存在今日数据就保存
            boolean isSave = xdcZModel.save();
            if (isSave) {
                for (int i = 0; i < listD.size(); i++) {
                    listD.get(i).setXdcZId(xdcZModel.getId());
                }
                DataSupport.saveAll(listD);
            }
        } else {
            //修改今日主表
            xdcZModel.updateAll("jcrq = ?", xdcZModel.getJcrq());
            //修改今日点击的电压值
            if (clickText != null && ("DYZ".equals(clickText.getTag()) || "DYY".equals(clickText.getTag()))) {
                //点击了电压值
                int clickId = clickText.getId();
                ContentValues values = new ContentValues();
                values.put("dy", clickText.getText().toString());
                DataSupport.updateAll(XdcDModel.class, values, "xh = ? and xdcZId = ?", (clickId - 11) + "", chaResults.get(0).getId() + "");
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
