package com.mtjsoft.www.myapplication.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.model.ChooseModel;

import java.util.List;

/**
 * @author mtj
 * Created by Administrator on 2018/3/8.
 */

public class ChooseBaseAdapter extends BaseAdapter{

    private  Context context;
    private List<ChooseModel> list;
    private boolean isHaveTwo;

    public ChooseBaseAdapter(Context context, List<ChooseModel> list, boolean isHaveTwo){
        this.isHaveTwo = isHaveTwo;
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHodel hodel = null;
        if (convertView == null) {
            hodel = new ViewHodel();
            convertView = View.inflate(context, R.layout.item_textview_array, null);
            hodel.textView = convertView.findViewById(R.id.tv_item);
            convertView.setTag(hodel);
        } else {
            hodel = (ViewHodel) convertView.getTag();
        }
        hodel.textView.setTag(list.get(position).getTiaojian());
        hodel.textView.setText(list.get(position).getTiaojian());
        if (isHaveTwo) {
            hodel.textView.setCompoundDrawablesWithIntrinsicBounds(0,0,R.mipmap.right_arrow,0);
        }
        return convertView;
    }

    private class ViewHodel{
        TextView textView;
    }
}
