package com.mtjsoft.www.myapplication.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.XiaoFangJianChaModel;
import com.mtjsoft.www.myapplication.model.XiaoFangTaiZhangModel;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mtj 消防设施检查表
 *         Created by Administrator on 2018/3/9.
 */

public class XiaoFangJianChaActivity extends BaseActivity {

    private static final int CHOOSENAME = 1002;
    private static final int CHOOSEBIANDIAN = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private int row = 0;
    private List<XiaoFangTaiZhangModel> xiaoFangTaiZhangModelList;
    private List<List<XiaoFangJianChaModel>> listRowDatas = new ArrayList<>();
    private String zhan = "___";
    private LinearLayout hengTopLayout;
    private LinearLayout hengBottomLayout;

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setVisibility(View.GONE);
        setTopName(String.format(getString(R.string.xiangfang_jiancha_title), zhan));
        addTableTitle();
        readData();
        addCenterView(row);
        setData();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        removeTopLayoutViewAll();
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText("序号");
        view.addView(drawTextView);
        //
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText("设施名称/位置");
        view.addView(drawTextView);
        //
        LinearLayout shuLayout = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_vertical_linearlayout_f_f, null);
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10);
        shuLayout.setLayoutParams(layoutParams);
        //检查日期
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText("检查日期");
        shuLayout.addView(drawTextView);
        //
        hengTopLayout = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_f, null);
        for (int i = 0; i < 10; i++) {
            drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            drawTextView.setLayoutParams(layoutParams);
            drawTextView.setText("x月x日");
            drawTextView.setOnClickListener(this);
            drawTextView.setTag("chooseTime");
            hengTopLayout.addView(drawTextView);
        }
        shuLayout.addView(hengTopLayout);
        view.addView(shuLayout);
        addTopLayoutView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(int row) {
        if (xiaoFangTaiZhangModelList != null && xiaoFangTaiZhangModelList.size() > 0) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            for (int j = 0; j < row; j++) {
                LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
                for (int i = 0; i < 12; i++) {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    if (i == 0) {
                        drawEditText.setEnabled(false);
                        drawEditText.setText((pLayout.getChildCount() + 1) + "");
                    } else if (i == 1) {
                        drawEditText.setEnabled(false);
                    }
                    drawEditText.setLayoutParams(layoutParams);
                    view.addView(drawEditText);
                }
                pLayout.addView(view);
            }
            hengBottomLayout = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            for (int i = 0; i < 12; i++) {
                DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                if (i == 0) {
                    drawTextView.setText((pLayout.getChildCount() + 1) + "");
                } else if (i == 1) {
                    drawTextView.setText("检查人");
                } else {
                    drawTextView.setText("签名");
                    drawTextView.setTag("chooseName");
                    drawTextView.setOnClickListener(this);
                }
                drawTextView.setLayoutParams(layoutParams);
                hengBottomLayout.addView(drawTextView);
            }
            pLayout.addView(hengBottomLayout);
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        if (v.getId() == R.id.tv_base_top_name) {
            Intent intent = new Intent(this, ChooseActivity.class);
            intent.putExtra("type", 1);
            startActivityForResult(intent, CHOOSEBIANDIAN);
            return;
        }
        String tag = (String) v.getTag();
        switch (tag) {
            case "chooseTime":
                chooseTime();
                break;
            case "chooseName":
                Intent intent = new Intent(this, LineLockActivity.class);
                intent.putExtra("isChoose", true);
                startActivityForResult(intent, CHOOSENAME);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            zhan = name;
                            setTopName(String.format(getString(R.string.xiangfang_jiancha_title), zhan));
                        }
                    }
                    break;
                case CHOOSENAME:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 选择日期
     */
    private void chooseTime() {
        //时间选择器
        TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                clickText.setText(TurnsUtils.convertToString(date,"yyyy-MM-dd"));
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(new boolean[]{false, true, true, false, false, false})
                .build();
        pvTime.show();
    }

    /**
     * 读取数据
     */
    private void readData() {
        xiaoFangTaiZhangModelList = DataSupport.findAll(XiaoFangTaiZhangModel.class);
        if (xiaoFangTaiZhangModelList != null && xiaoFangTaiZhangModelList.size() > 0) {
            for (int i = 0; i < xiaoFangTaiZhangModelList.size(); i++) {
                List<XiaoFangJianChaModel> rowList = DataSupport.where("ssmcwz = ?", xiaoFangTaiZhangModelList.get(i).getMc()).find(XiaoFangJianChaModel.class);
                if (rowList != null && rowList.size() > 0) {
                    listRowDatas.add(rowList);
                }
            }
        }
        if (xiaoFangTaiZhangModelList != null && xiaoFangTaiZhangModelList.size() > row) {
            row = xiaoFangTaiZhangModelList.size();
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        if (xiaoFangTaiZhangModelList != null && xiaoFangTaiZhangModelList.size() > 0) {
            for (int i = 0, plCh = xiaoFangTaiZhangModelList.size(); i < plCh; i++) {
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                ((DrawEditText) linearLayout.getChildAt(1)).setText(xiaoFangTaiZhangModelList.get(i).getMc());
                if (listRowDatas.size() > i) {
                    for (int j = 2; j < listRowDatas.get(i).size() + 2; j++) {
                        XiaoFangJianChaModel model = listRowDatas.get(i).get(j - 2);
                        if (model != null) {
                            if (!TextUtils.isEmpty(model.getBdz())) {
                                zhan = model.getBdz();
                            }
                            if (j < 12) {
                                ((DrawEditText) linearLayout.getChildAt(j)).setText(model.getZxjg());
                                ((DrawTextView) hengTopLayout.getChildAt(j - 2)).setText(model.getRq());
                                ((DrawTextView) hengBottomLayout.getChildAt(j)).setText(model.getJcr());
                            }
                        }
                    }
                }
            }
            setTopName(String.format(getString(R.string.xiangfang_jiancha_title), zhan));
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        DataSupport.deleteAll(XiaoFangJianChaModel.class);
        List<XiaoFangJianChaModel> xiaoFangJianChaModelList = new ArrayList<>();
        for (int i = 0, plCh = pLayout.getChildCount() - 1; i < plCh; i++) {
            LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
            DrawEditText drawTextView = (DrawEditText) linearLayout.getChildAt(1);
            if (!TextUtils.isEmpty(drawTextView.getText().toString())) {
                for (int j = 2; j < linearLayout.getChildCount(); j++) {
                    XiaoFangJianChaModel model = new XiaoFangJianChaModel();
                    model.setBdz(zhan);
                    model.setSsmcwz(drawTextView.getText().toString());
                    model.setRq(((DrawTextView) hengTopLayout.getChildAt(j - 2)).getText().toString());
                    model.setJcr(((DrawTextView) hengBottomLayout.getChildAt(j)).getText().toString());
                    model.setZxjg(((DrawEditText) linearLayout.getChildAt(j)).getText().toString());
                    xiaoFangJianChaModelList.add(model);
                }
            }
        }
        DataSupport.saveAll(xiaoFangJianChaModelList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
