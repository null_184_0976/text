package com.mtjsoft.www.myapplication.ui;

import android.content.Intent;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.YunWeiFenXiModel;
import com.mtjsoft.www.myapplication.utils.HHDensityUtils;
import com.mtjsoft.www.myapplication.utils.HHInputUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mtj 运维分析记录
 *         Created by Administrator on 2018/3/11.
 */

public class YunWeiFenXiActivity extends BaseActivity {

    private static final int CHOOSEBIANDIAN = 1001;
    private static final int CHOOSEGJ = 1002;
    private static final int CHOOSENAME = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private List<DrawTextView> drawTextViewList = new ArrayList<>();
    private List<DrawEditText> drawEditTextList = new ArrayList<>();

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setVisibility(View.GONE);
        setTopName("运维分析记录");
        addCenterView("运维班（站）", true, "班次", false);
        addCenterView("记录日期", true, "分析类别", false);
        addBottomLayout("分析题目");
        addCenterView("提出人", true);
        addCenterView("参加人", false);
        addCenterView("缺席人员", false,"缺席原因", false);
        addBottomLayout("分析内容");
        addBottomLayout("措施及执行情况");
        addCenterView("记录人", true, "检查人", true);
        addBottomLayout("备注");
        setData();
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(String leftName, boolean leftIsText, String rightName, boolean rightIsText) {
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < 4; i++) {
            DrawTextView drawTextView = null;
            DrawEditText drawEditText = null;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            switch (i) {
                case 0:
                    drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(leftName);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                    break;
                case 1:
                    if (leftIsText) {
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setLayoutParams(layoutParams);
                        drawTextView.setOnClickListener(this);
                        drawTextView.setId(drawTextViewList.size());
                        drawTextViewList.add(drawTextView);
                        view.addView(drawTextView);
                    } else {
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        drawEditTextList.add(drawEditText);
                        view.addView(drawEditText);
                    }
                    break;
                case 2:
                    drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(rightName);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                    break;
                case 3:
                    if (rightIsText) {
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setLayoutParams(layoutParams);
                        drawTextView.setOnClickListener(this);
                        drawTextView.setId(drawTextViewList.size());
                        drawTextViewList.add(drawTextView);
                        view.addView(drawTextView);
                    } else {
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        drawEditTextList.add(drawEditText);
                        view.addView(drawEditText);
                    }
                    break;
                default:
                    break;
            }
        }
        pLayout.addView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(String leftName, boolean leftIsText) {
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < 2; i++) {
            DrawTextView drawTextView = null;
            DrawEditText drawEditText = null;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            switch (i) {
                case 0:
                    drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(leftName);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                    break;
                case 1:
                    layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 3);
                    if (leftIsText) {
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setLayoutParams(layoutParams);
                        drawTextView.setOnClickListener(this);
                        drawTextView.setId(drawTextViewList.size());
                        drawTextViewList.add(drawTextView);
                        view.addView(drawTextView);
                    } else {
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        drawEditTextList.add(drawEditText);
                        view.addView(drawEditText);
                    }
                    break;
                default:
                    break;
            }
        }
        pLayout.addView(view);
    }

    private void addBottomLayout(String bottomName) {
        DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText(bottomName);
        drawTextView.setPadding(16, 0, 0, 0);
        drawTextView.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
        pLayout.addView(drawTextView);
        //
        DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
        drawEditText.setMinHeight(HHDensityUtils.dip2px(getBaseContext(), 200));
        drawEditText.setLayoutParams(layoutParams);
        drawEditTextList.add(drawEditText);
        pLayout.addView(drawEditText);
    }


    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        Intent intent = null;
        switch (v.getId()) {
            case 0:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 1);
                startActivityForResult(intent, CHOOSEBIANDIAN);
                break;
            case 1:
                HHInputUtils.chooseTime(false, clickText);
                break;
            case 2:
            case 3:
            case 4:
                intent = new Intent(this, LineLockActivity.class);
                intent.putExtra("isChoose", true);
                startActivityForResult(intent, CHOOSENAME);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSENAME:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        YunWeiFenXiModel model = DataSupport.findLast(YunWeiFenXiModel.class);
        if (model != null) {
            drawEditTextList.get(0).setText(model.getBc());
            drawEditTextList.get(1).setText(model.getFxlb());
            drawEditTextList.get(2).setText(model.getFxtm());
            drawEditTextList.get(3).setText(model.getCjr());
            drawEditTextList.get(4).setText(model.getQxry());
            drawEditTextList.get(5).setText(model.getQxyy());
            drawEditTextList.get(6).setText(model.getFxnr());
            drawEditTextList.get(7).setText(model.getCsjzxqk());
            drawEditTextList.get(8).setText(model.getBz());
            //
            drawTextViewList.get(0).setText(model.getYwbz());
            drawTextViewList.get(1).setText(model.getJlrq());
            drawTextViewList.get(2).setText(model.getTcr());
            drawTextViewList.get(3).setText(model.getJlr());
            drawTextViewList.get(4).setText(model.getJcr());
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        YunWeiFenXiModel model = new YunWeiFenXiModel();
        model.setBc(drawEditTextList.get(0).getText().toString());
        model.setFxlb(drawEditTextList.get(1).getText().toString());
        model.setFxtm(drawEditTextList.get(2).getText().toString());
        model.setCjr(drawEditTextList.get(3).getText().toString());
        model.setQxry(drawEditTextList.get(4).getText().toString());
        model.setQxyy(drawEditTextList.get(5).getText().toString());
        model.setFxnr(drawEditTextList.get(6).getText().toString());
        model.setCsjzxqk(drawEditTextList.get(7).getText().toString());
        model.setBz(drawEditTextList.get(8).getText().toString());
        //
        model.setYwbz(drawTextViewList.get(0).getText().toString());
        model.setJlrq(drawTextViewList.get(1).getText().toString());
        model.setTcr(drawTextViewList.get(2).getText().toString());
        model.setJlr(drawTextViewList.get(3).getText().toString());
        model.setJcr(drawTextViewList.get(4).getText().toString());
        model.save();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
