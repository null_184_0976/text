package com.mtjsoft.www.myapplication.adapter;


import android.content.Context;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.utils.Picture;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by jihe on 16/8/23.
 */
public class MyAdapter extends BaseAdapter {
    private Context context;
    private GridView mGv;
    private List<Picture> pictures;
    private ArrayList<HashMap<String, Object>> lstImageItem;
    private static int ROW_NUMBER = 5;

    /**
     * 数据资源：标题 ＋ 图片
     */
    //定义图标数组
    private int[] imageRes = {
            R.mipmap.ywjh,
            R.mipmap.sczb,
            R.mipmap.yxgz,
            R.mipmap.sbxs,
            R.mipmap.wxd,
            R.mipmap.fwbs,
            R.mipmap.zxjc,
            R.mipmap.jqr,
            R.mipmap.whgz,
            R.mipmap.xfss,
            R.mipmap.fxss,
            R.mipmap.zbyh,
            R.mipmap.ywjl,
            R.mipmap.aqgqj,
            R.mipmap.ybgj,
            R.mipmap.scyc,
            R.mipmap.wlry,
            R.mipmap.bpbj,
            R.mipmap.bdzsbzlcsb,
            R.mipmap.tzzl,
    };

    //定义图标下方的名称数组
    private String[] arrText = {
            "运维计划",
            "生产准备",
            "运行规程",
            "设备巡视",
            "危险点",
            "防误闭锁",
            "在线监测",
            "机器人",
            "维护工作",
            "消防设施",
            "防汛设施",
            "周边隐患",
            "运维记录",
            "安全工器具",
            "仪表工具",
            "生产用车",
            "外来人员",
            "备品备件",
            "变电站设备载流参数表",
            "图纸资料"
    };


    public MyAdapter(Context context, GridView gv) {
       /* this.context = context;
        this.mGv = gv;
        int length = imageRes.length;

        //生成动态数组，并且转入数据
        ArrayList<HashMap<String, Object>> lstImageItem = new ArrayList<HashMap<String, Object>>();
        for (int i = 0; i < length; i++) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put( arrText[i], imageRes[i]);//添加图像资源的ID
//            map.put("ItemText", arrText[i]);//按序号做ItemText
            lstImageItem.add(map);
        }*/
        this.context = context;
        this.mGv = gv;
                 pictures = new ArrayList<>();
                for (int i = 0; i < 20; i++) {
                        Picture pt = new Picture(arrText[i], imageRes[i]);
                        pictures.add(pt);
                    }
    }

    @Override
    public int getCount() {
        if (null != pictures) {
            return pictures.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return pictures.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = LayoutInflater.from(context).inflate(R.layout.layout2, null);
            holder.iv = (ImageView) convertView.findViewById(R.id.iv_item);
            //设置显示图片
            holder.iv.setBackgroundResource(imageRes[position]);
            holder.tv = (TextView) convertView.findViewById(R.id.tv_item);
            //设置标题
            holder.tv.setText(arrText[position]);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

      /*  //高度计算
        AbsListView.LayoutParams param = new AbsListView.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                mGv.getHeight() / ROW_NUMBER);

        convertView.setLayoutParams(param);*/
        return convertView;
    }

    class Holder {
        ImageView iv;
        TextView tv;
    }
}