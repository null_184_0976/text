package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 消防设施检查表
 *         Created by Administrator on 2018/3/16.
 */

public class XiaoFangJianChaModel extends DataSupport {
    private int id;//主键id
    private String bdz;//变电站
    private String ssmcwz;//	设施名称/位置
    private String rq;//日期
    private String zxjg;//执行结果（（√或描述异常）
    private String jcr;//检查人

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBdz() {
        return bdz;
    }

    public void setBdz(String bdz) {
        this.bdz = bdz;
    }

    public String getSsmcwz() {
        return ssmcwz;
    }

    public void setSsmcwz(String ssmcwz) {
        this.ssmcwz = ssmcwz;
    }

    public String getRq() {
        return rq;
    }

    public void setRq(String rq) {
        this.rq = rq;
    }

    public String getZxjg() {
        return zxjg;
    }

    public void setZxjg(String zxjg) {
        this.zxjg = zxjg;
    }

    public String getJcr() {
        return jcr;
    }

    public void setJcr(String jcr) {
        this.jcr = jcr;
    }
}
