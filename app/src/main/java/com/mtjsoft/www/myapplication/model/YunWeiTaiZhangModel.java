package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 运维记录及台账检查记录
 *         Created by Administrator on 2018/3/11.
 */

public class YunWeiTaiZhangModel extends DataSupport {
    private int id;//主键id
    private String rq;//日期
    private String bdzmc;//	变电站名称
    private String jcnr;//检查内容
    private String wtsm;//问题说明
    private String zgqk;//整改情况
    private String bzjcr;//	班组检查人
    private String ywdwjcr;//	运维单位检查人

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRq() {
        return rq;
    }

    public void setRq(String rq) {
        this.rq = rq;
    }

    public String getBdzmc() {
        return bdzmc;
    }

    public void setBdzmc(String bdzmc) {
        this.bdzmc = bdzmc;
    }

    public String getJcnr() {
        return jcnr;
    }

    public void setJcnr(String jcnr) {
        this.jcnr = jcnr;
    }

    public String getWtsm() {
        return wtsm;
    }

    public void setWtsm(String wtsm) {
        this.wtsm = wtsm;
    }

    public String getZgqk() {
        return zgqk;
    }

    public void setZgqk(String zgqk) {
        this.zgqk = zgqk;
    }

    public String getBzjcr() {
        return bzjcr;
    }

    public void setBzjcr(String bzjcr) {
        this.bzjcr = bzjcr;
    }

    public String getYwdwjcr() {
        return ywdwjcr;
    }

    public void setYwdwjcr(String ywdwjcr) {
        this.ywdwjcr = ywdwjcr;
    }
}
