package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 电气设备检修试验记录
 *         Created by Administrator on 2018/3/12.
 */

public class YunWeiShiYanModel extends DataSupport {
    private int id;//主键id
    private String bdzmc;//	变电站名称
    private String yc;//页次
    private String rq;//日期
    private String sbjgmc;//设备（间隔）名称
    private String gzpbh;//	工作票编号
    private String gznr;//工作内容
    private String jxxz;//检修性质
    private String jxxmjczwt;//检修项目及存在问题
    private String jl;//结论
    private String gzfzr;//	工作负责人
    private String ysryqz;//	验收人员签字

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBdzmc() {
        return bdzmc;
    }

    public void setBdzmc(String bdzmc) {
        this.bdzmc = bdzmc;
    }

    public String getYc() {
        return yc;
    }

    public void setYc(String yc) {
        this.yc = yc;
    }

    public String getRq() {
        return rq;
    }

    public void setRq(String rq) {
        this.rq = rq;
    }

    public String getSbjgmc() {
        return sbjgmc;
    }

    public void setSbjgmc(String sbjgmc) {
        this.sbjgmc = sbjgmc;
    }

    public String getGzpbh() {
        return gzpbh;
    }

    public void setGzpbh(String gzpbh) {
        this.gzpbh = gzpbh;
    }

    public String getGznr() {
        return gznr;
    }

    public void setGznr(String gznr) {
        this.gznr = gznr;
    }

    public String getJxxz() {
        return jxxz;
    }

    public void setJxxz(String jxxz) {
        this.jxxz = jxxz;
    }

    public String getJxxmjczwt() {
        return jxxmjczwt;
    }

    public void setJxxmjczwt(String jxxmjczwt) {
        this.jxxmjczwt = jxxmjczwt;
    }

    public String getJl() {
        return jl;
    }

    public void setJl(String jl) {
        this.jl = jl;
    }

    public String getGzfzr() {
        return gzfzr;
    }

    public void setGzfzr(String gzfzr) {
        this.gzfzr = gzfzr;
    }

    public String getYsryqz() {
        return ysryqz;
    }

    public void setYsryqz(String ysryqz) {
        this.ysryqz = ysryqz;
    }
}
