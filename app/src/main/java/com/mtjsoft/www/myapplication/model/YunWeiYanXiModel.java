package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 反事故演习记录
 *         Created by Administrator on 2018/3/16.
 */

public class YunWeiYanXiModel extends DataSupport {
    private int id;//主键id
    private String yxbz;//演习班组
    private String yxbdz;//	演习变电站
    private String yxzcr;//	演习主持人
    private String yxry;//演习人员
    private String yxkssj;//	演习开始时间
    private String yxjssj;//	演习结束时间
    private String tq;//天气
    private String jlrq;//记录日期
    private String yxfs;//运行方式
    private String yxtm;//演习题目
    private String sgxx;//事故现象
    private String cljg;//处理经过
    private String gjcsjpj;//	改进措施及评价
    private String yxryqz;//	演习人员（签字）
    private String zcrqz;//	主持人（签字）
    private String jcrqz;//	检查人（签字）
    private String bz;//备注

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getYxbz() {
        return yxbz;
    }

    public void setYxbz(String yxbz) {
        this.yxbz = yxbz;
    }

    public String getYxbdz() {
        return yxbdz;
    }

    public void setYxbdz(String yxbdz) {
        this.yxbdz = yxbdz;
    }

    public String getYxzcr() {
        return yxzcr;
    }

    public void setYxzcr(String yxzcr) {
        this.yxzcr = yxzcr;
    }

    public String getYxry() {
        return yxry;
    }

    public void setYxry(String yxry) {
        this.yxry = yxry;
    }

    public String getYxkssj() {
        return yxkssj;
    }

    public void setYxkssj(String yxkssj) {
        this.yxkssj = yxkssj;
    }

    public String getYxjssj() {
        return yxjssj;
    }

    public void setYxjssj(String yxjssj) {
        this.yxjssj = yxjssj;
    }

    public String getTq() {
        return tq;
    }

    public void setTq(String tq) {
        this.tq = tq;
    }

    public String getJlrq() {
        return jlrq;
    }

    public void setJlrq(String jlrq) {
        this.jlrq = jlrq;
    }

    public String getYxfs() {
        return yxfs;
    }

    public void setYxfs(String yxfs) {
        this.yxfs = yxfs;
    }

    public String getYxtm() {
        return yxtm;
    }

    public void setYxtm(String yxtm) {
        this.yxtm = yxtm;
    }

    public String getSgxx() {
        return sgxx;
    }

    public void setSgxx(String sgxx) {
        this.sgxx = sgxx;
    }

    public String getCljg() {
        return cljg;
    }

    public void setCljg(String cljg) {
        this.cljg = cljg;
    }

    public String getGjcsjpj() {
        return gjcsjpj;
    }

    public void setGjcsjpj(String gjcsjpj) {
        this.gjcsjpj = gjcsjpj;
    }

    public String getYxryqz() {
        return yxryqz;
    }

    public void setYxryqz(String yxryqz) {
        this.yxryqz = yxryqz;
    }

    public String getZcrqz() {
        return zcrqz;
    }

    public void setZcrqz(String zcrqz) {
        this.zcrqz = zcrqz;
    }

    public String getJcrqz() {
        return jcrqz;
    }

    public void setJcrqz(String jcrqz) {
        this.jcrqz = jcrqz;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }
}
