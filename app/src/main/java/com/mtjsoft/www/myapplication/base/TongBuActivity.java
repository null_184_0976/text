package com.mtjsoft.www.myapplication.base;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.data.MtjDataManager;
import com.mtjsoft.www.myapplication.imp.HHCallBackImp;
import com.mtjsoft.www.myapplication.model.FangWuJianChaModel;
import com.mtjsoft.www.myapplication.model.FangWuTaiZhangModel;
import com.mtjsoft.www.myapplication.model.FangXunTaiZhangModel;
import com.mtjsoft.www.myapplication.model.XdcDModel;
import com.mtjsoft.www.myapplication.model.XdcZModel;
import com.mtjsoft.www.myapplication.model.XiaoFangTaiZhangModel;
import com.mtjsoft.www.myapplication.model.YiJiaoQingDanModel;
import com.mtjsoft.www.myapplication.model.YunWeiNModel;
import com.mtjsoft.www.myapplication.model.YunWeiYModel;
import com.mtjsoft.www.myapplication.model.YunWeiZModel;
import com.mtjsoft.www.myapplication.model.ZbYingHuanModel;
import com.mtjsoft.www.myapplication.utils.HHJsonParseUtils;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;

import org.litepal.crud.DataSupport;

import java.util.List;

/**
 * @author mtj  同步页面
 *         Created by Administrator on 2018/3/9.
 */

public class TongBuActivity extends AppCompatActivity implements View.OnClickListener {

    private StringBuffer stringBufferMsg = new StringBuffer();
    private Gson gson = new Gson();
    private boolean isTbing = false;
    private CardView cardView;
    private TextView tbMsgTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tongbu);
        cardView = findViewById(R.id.cv_tving);
        tbMsgTextView = findViewById(R.id.tv_tbMsg);
        Button button = findViewById(R.id.btu_tongbu);
        button.setOnClickListener(this);
        cardView.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btu_tongbu:
                if (isTbing) {
                    Toast.makeText(getBaseContext(), getText(R.string.tbing), Toast.LENGTH_LONG).show();
                    return;
                }
                stringBufferMsg = new StringBuffer();
                cardView.setVisibility(View.VISIBLE);
                isTbing = true;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        tongBuData();
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        handler.sendEmptyMessage(10);
                    }
                }).start();
                break;
            default:
                break;
        }
    }

    /**
     * 同步数据
     */
    private void tongBuData() {
        //蓄电池的数据
        List<XdcZModel> listXdcZ = DataSupport.findAll(XdcZModel.class);
        if (listXdcZ != null && listXdcZ.size() > 0) {
            for (int i = 0; i < listXdcZ.size(); i++) {
                XdcZModel modelZ = listXdcZ.get(i);
                String xdcZ = gson.toJson(modelZ);
                List<XdcDModel> listXdcD = DataSupport.where("xdcZId = ?", modelZ.getId() + "").find(XdcDModel.class);
                String xdcDtList = gson.toJson(listXdcD);
                MtjDataManager.tbXdc(xdcZ, xdcDtList, new HHCallBackImp() {
                    @Override
                    public void onSuccess(Object data) {
                        if (HHJsonParseUtils.getResponceStatus(data.toString()) == 0) {
                            String delId = HHJsonParseUtils.getParamInfo(data.toString(), "data");
                            if (!TextUtils.isEmpty(delId) && TurnsUtils.getInt(delId, 0) > 0) {
                                DataSupport.delete(XdcZModel.class, TurnsUtils.getInt(delId, 0));
                                DataSupport.deleteAll(XdcDModel.class, "xdcZId = ?", TurnsUtils.getInt(delId, 0) + "");
                            }
                        } else {
                            stringBufferMsg.append(HHJsonParseUtils.getResponceMsg(data.toString()));
                            stringBufferMsg.append("；\n");
                        }
                    }

                    @Override
                    public void onError(Object data) {
                        stringBufferMsg.append("蓄电池检测数据同步失败");
                        stringBufferMsg.append("；\n");
                    }
                });
            }
        }
        //运维计划表
        String ywNjh = "", ywYjh = "", ywZjh = "";
        List<YunWeiNModel> nModelList = DataSupport.findAll(YunWeiNModel.class);
        List<YunWeiYModel> yModelList = DataSupport.findAll(YunWeiYModel.class);
        List<YunWeiZModel> zModelList = DataSupport.findAll(YunWeiZModel.class);
        if (nModelList != null && nModelList.size() > 0) {
            ywNjh = gson.toJson(nModelList);
        }
        if (yModelList != null && yModelList.size() > 0) {
            ywYjh = gson.toJson(yModelList);
        }
        if (zModelList != null && zModelList.size() > 0) {
            ywZjh = gson.toJson(zModelList);
        }
        if (!(TextUtils.isEmpty(ywNjh) && TextUtils.isEmpty(ywYjh) && TextUtils.isEmpty(ywZjh))) {
            MtjDataManager.tbYwJh(ywNjh, ywYjh, ywZjh, new HHCallBackImp() {
                @Override
                public void onSuccess(Object data) {
                    if (HHJsonParseUtils.getResponceStatus(data.toString()) == 0) {
                        DataSupport.deleteAll(YunWeiNModel.class);
                        DataSupport.deleteAll(YunWeiYModel.class);
                        DataSupport.deleteAll(YunWeiZModel.class);
                    } else {
                        stringBufferMsg.append(HHJsonParseUtils.getResponceMsg(data.toString()));
                        stringBufferMsg.append("；\n");
                    }
                }

                @Override
                public void onError(Object data) {
                    stringBufferMsg.append("运维计划表数据同步失败");
                    stringBufferMsg.append("；\n");
                }
            });
        }
        //专用工器具及备品备件移交清单
        List<YiJiaoQingDanModel> yiJiaoQingDanModelList = DataSupport.findAll(YiJiaoQingDanModel.class);
        if (yiJiaoQingDanModelList != null && yiJiaoQingDanModelList.size() > 0) {
            MtjDataManager.tbYjQd(gson.toJson(yiJiaoQingDanModelList), new HHCallBackImp() {
                @Override
                public void onSuccess(Object data) {
                    if (HHJsonParseUtils.getResponceStatus(data.toString()) == 0) {
                        DataSupport.deleteAll(YiJiaoQingDanModel.class);
                    } else {
                        stringBufferMsg.append(HHJsonParseUtils.getResponceMsg(data.toString()));
                        stringBufferMsg.append("；\n");
                    }
                }

                @Override
                public void onError(Object data) {
                    stringBufferMsg.append("专用工器具及备品备件移交清单同步失败");
                    stringBufferMsg.append("；\n");
                }
            });
        }
        //防误装置
        String fwzztz = "", fwzzjcjl = "";
        List<FangWuTaiZhangModel> fangWuTaiZhangModelList = DataSupport.findAll(FangWuTaiZhangModel.class);
        List<FangWuJianChaModel> fangWuJianChaModelList = DataSupport.findAll(FangWuJianChaModel.class);
        if (fangWuTaiZhangModelList != null && fangWuTaiZhangModelList.size() > 0) {
            fwzztz = gson.toJson(fangWuTaiZhangModelList);
        }
        if (fangWuJianChaModelList != null && fangWuJianChaModelList.size() > 0) {
            fwzzjcjl = gson.toJson(fangWuJianChaModelList);
        }
        if (!(TextUtils.isEmpty(fwzztz) && TextUtils.isEmpty(fwzzjcjl))) {
            MtjDataManager.tbFwZZ(fwzztz, fwzzjcjl, new HHCallBackImp() {
                @Override
                public void onSuccess(Object data) {
                    if (HHJsonParseUtils.getResponceStatus(data.toString()) == 0) {
                        DataSupport.deleteAll(FangWuTaiZhangModel.class);
                        DataSupport.deleteAll(FangWuJianChaModel.class);
                    } else {
                        stringBufferMsg.append(HHJsonParseUtils.getResponceMsg(data.toString()));
                        stringBufferMsg.append("；\n");
                    }
                }

                @Override
                public void onError(Object data) {
                    stringBufferMsg.append("防误装置数据同步失败");
                    stringBufferMsg.append("；\n");
                }
            });
        }
        //消防台账记录
        List<XiaoFangTaiZhangModel> xiaoFangTaiZhangModelList = DataSupport.findAll(XiaoFangTaiZhangModel.class);
        if (xiaoFangTaiZhangModelList != null && xiaoFangTaiZhangModelList.size() > 0) {
            MtjDataManager.tbXiaoFangTZ(gson.toJson(xiaoFangTaiZhangModelList), new HHCallBackImp() {
                @Override
                public void onSuccess(Object data) {
                    if (HHJsonParseUtils.getResponceStatus(data.toString()) == 0) {
                        DataSupport.deleteAll(XiaoFangTaiZhangModel.class);
                    } else {
                        stringBufferMsg.append(HHJsonParseUtils.getResponceMsg(data.toString()));
                        stringBufferMsg.append("；\n");
                    }
                }

                @Override
                public void onError(Object data) {
                    stringBufferMsg.append("消防台账记录同步失败");
                    stringBufferMsg.append("；\n");
                }
            });
        }
        //防汛物资台账
        List<FangXunTaiZhangModel> fangXunTaiZhangModelList = DataSupport.findAll(FangXunTaiZhangModel.class);
        if (fangXunTaiZhangModelList != null && fangXunTaiZhangModelList.size() > 0) {
            MtjDataManager.tbFangXunTZ(gson.toJson(fangXunTaiZhangModelList), new HHCallBackImp() {
                @Override
                public void onSuccess(Object data) {
                    if (HHJsonParseUtils.getResponceStatus(data.toString()) == 0) {
                        DataSupport.deleteAll(FangXunTaiZhangModel.class);
                    } else {
                        stringBufferMsg.append(HHJsonParseUtils.getResponceMsg(data.toString()));
                        stringBufferMsg.append("；\n");
                    }
                }

                @Override
                public void onError(Object data) {
                    stringBufferMsg.append("防汛物资台账同步失败");
                    stringBufferMsg.append("；\n");
                }
            });
        }
        //周边隐患台账
        List<ZbYingHuanModel> zbYingHuanModelList = DataSupport.findAll(ZbYingHuanModel.class);
        if (zbYingHuanModelList != null && zbYingHuanModelList.size() > 0) {
            MtjDataManager.tbZbYH(gson.toJson(zbYingHuanModelList), new HHCallBackImp() {
                @Override
                public void onSuccess(Object data) {
                    if (HHJsonParseUtils.getResponceStatus(data.toString()) == 0) {
                        DataSupport.deleteAll(ZbYingHuanModel.class);
                    } else {
                        stringBufferMsg.append(HHJsonParseUtils.getResponceMsg(data.toString()));
                        stringBufferMsg.append("；\n");
                    }
                }

                @Override
                public void onError(Object data) {
                    stringBufferMsg.append("防误装置数据同步失败");
                    stringBufferMsg.append("；\n");
                }
            });
        }
    }

    /**
     * 同步完成
     */
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            isTbing = false;
            Toast.makeText(getBaseContext(), getText(R.string.tb_su), Toast.LENGTH_LONG).show();
            if (cardView != null) {
                cardView.setVisibility(View.GONE);
            }
            if (tbMsgTextView != null) {
                if (TextUtils.isEmpty(stringBufferMsg.toString())) {
                    tbMsgTextView.setText(getString(R.string.tb_su));
                } else {
                    tbMsgTextView.setText(stringBufferMsg.toString());
                }
            }
        }
    };
}
