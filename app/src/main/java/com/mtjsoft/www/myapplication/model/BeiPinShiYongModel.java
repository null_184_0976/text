package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 备品备件使用记录
 *         Created by Administrator on 2018/3/11.
 */

public class BeiPinShiYongModel extends DataSupport {
    private int id;//主键id
    private String bzmc;//		班站名称
    private String mc;//名称String
    private String ggxh;//		规格型号
    private String sl;//数量
    private String dw;//单位
    private String bcsl;//		补充数量(入库情况)
    private String rksj;//		入库时间(入库情况)
    private String zrr;//	责任人(入库情况)
    private String lysl;//		领用数量(出库情况)
    private String lysj;//		领用时间(出库情况)
    private String lyr;//	领用人(出库情况)

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBzmc() {
        return bzmc;
    }

    public void setBzmc(String bzmc) {
        this.bzmc = bzmc;
    }

    public String getMc() {
        return mc;
    }

    public void setMc(String mc) {
        this.mc = mc;
    }

    public String getGgxh() {
        return ggxh;
    }

    public void setGgxh(String ggxh) {
        this.ggxh = ggxh;
    }

    public String getSl() {
        return sl;
    }

    public void setSl(String sl) {
        this.sl = sl;
    }

    public String getDw() {
        return dw;
    }

    public void setDw(String dw) {
        this.dw = dw;
    }

    public String getBcsl() {
        return bcsl;
    }

    public void setBcsl(String bcsl) {
        this.bcsl = bcsl;
    }

    public String getRksj() {
        return rksj;
    }

    public void setRksj(String rksj) {
        this.rksj = rksj;
    }

    public String getZrr() {
        return zrr;
    }

    public void setZrr(String zrr) {
        this.zrr = zrr;
    }

    public String getLysl() {
        return lysl;
    }

    public void setLysl(String lysl) {
        this.lysl = lysl;
    }

    public String getLysj() {
        return lysj;
    }

    public void setLysj(String lysj) {
        this.lysj = lysj;
    }

    public String getLyr() {
        return lyr;
    }

    public void setLyr(String lyr) {
        this.lyr = lyr;
    }
}
