package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 安全工器具检查记录模板
 *         Created by Administrator on 2018/3/13.
 */

public class AnQuanJianChaModel extends DataSupport {
    private int id;//主键id
    private String mc;//名称
    private int yf;//月份
    private String nr;//		内容;
    private String jcr;// 检查人（注：检查人在每月对应位置填写检查情况，如无问题打“√”。）

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMc() {
        return mc;
    }

    public void setMc(String mc) {
        this.mc = mc;
    }

    public int getYf() {
        return yf;
    }

    public void setYf(int yf) {
        this.yf = yf;
    }

    public String getNr() {
        return nr;
    }

    public void setNr(String nr) {
        this.nr = nr;
    }

    public String getJcr() {
        return jcr;
    }

    public void setJcr(String jcr) {
        this.jcr = jcr;
    }
}
