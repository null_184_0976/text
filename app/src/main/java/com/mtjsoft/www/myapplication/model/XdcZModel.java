package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj
 *         Created by Administrator on 2018/3/8.
 *         蓄电池基本信息表
 */

public class XdcZModel extends DataSupport {
    private int id;//int(11)	蓄电池检测记录基本信息表主键id
    private String zm;//varchar(255)	站名
    private String yxbh;//varchar(255)	运行编号
    private String qdcs;//varchar(255)	全电池数（只）
    private String xh;//varchar(255)	型号
    private String jcrq;//varchar(255)	检查日期
    private String hjwd;//varchar(10)	环境温度（℃）
    private String scqdcfcdy;//	varchar(255)	实测全电池浮充电压（V）
    private String zlxtjcqdcfcdy;//	varchar(255)	直流系统监测全电池浮充电压（V）
    private String fhdl;//	varchar(255)	负荷电流（A）
    private String fcdl;//varchar(255)	浮充电流（A）
    private String zmxdddyz;//varchar(255)	正母线对地电压值（V）
    private String fmxdddyz;//varchar(255)	负母线对地电压值（V）
    private String csfl;//	varchar(255)	测试分类
    private String csfzr;//	varchar(255)	测试负责人
    private String cscy;//varchar(255)	测试成员
    private String fxjl;//	varchar(255)	分析结论
    //点选到第几个
    private String clickTextId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getZm() {
        return zm;
    }

    public void setZm(String zm) {
        this.zm = zm;
    }

    public String getYxbh() {
        return yxbh;
    }

    public void setYxbh(String yxbh) {
        this.yxbh = yxbh;
    }

    public String getQdcs() {
        return qdcs;
    }

    public void setQdcs(String qdcs) {
        this.qdcs = qdcs;
    }

    public String getXh() {
        return xh;
    }

    public void setXh(String xh) {
        this.xh = xh;
    }

    public String getJcrq() {
        return jcrq;
    }

    public void setJcrq(String jcrq) {
        this.jcrq = jcrq;
    }

    public String getHjwd() {
        return hjwd;
    }

    public void setHjwd(String hjwd) {
        this.hjwd = hjwd;
    }

    public String getScqdcfcdy() {
        return scqdcfcdy;
    }

    public void setScqdcfcdy(String scqdcfcdy) {
        this.scqdcfcdy = scqdcfcdy;
    }

    public String getZlxtjcqdcfcdy() {
        return zlxtjcqdcfcdy;
    }

    public void setZlxtjcqdcfcdy(String zlxtjcqdcfcdy) {
        this.zlxtjcqdcfcdy = zlxtjcqdcfcdy;
    }

    public String getFhdl() {
        return fhdl;
    }

    public void setFhdl(String fhdl) {
        this.fhdl = fhdl;
    }

    public String getFcdl() {
        return fcdl;
    }

    public void setFcdl(String fcdl) {
        this.fcdl = fcdl;
    }

    public String getZmxdddyz() {
        return zmxdddyz;
    }

    public void setZmxdddyz(String zmxdddyz) {
        this.zmxdddyz = zmxdddyz;
    }

    public String getFmxdddyz() {
        return fmxdddyz;
    }

    public void setFmxdddyz(String fmxdddyz) {
        this.fmxdddyz = fmxdddyz;
    }

    public String getCsfl() {
        return csfl;
    }

    public void setCsfl(String csfl) {
        this.csfl = csfl;
    }

    public String getCsfzr() {
        return csfzr;
    }

    public void setCsfzr(String csfzr) {
        this.csfzr = csfzr;
    }

    public String getCscy() {
        return cscy;
    }

    public void setCscy(String cscy) {
        this.cscy = cscy;
    }

    public String getFxjl() {
        return fxjl;
    }

    public void setFxjl(String fxjl) {
        this.fxjl = fxjl;
    }

    public String getClickTextId() {
        return clickTextId;
    }

    public void setClickTextId(String clickTextId) {
        this.clickTextId = clickTextId;
    }
}
