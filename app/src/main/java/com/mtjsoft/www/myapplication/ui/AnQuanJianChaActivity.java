package com.mtjsoft.www.myapplication.ui;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.AnQuanJianChaModel;
import com.mtjsoft.www.myapplication.model.AnQuanTaiZhangModel;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mtj 安全工器具检查记录
 *         Created by Administrator on 2018/3/11.
 */

public class AnQuanJianChaActivity extends BaseActivity {

    private static final int CHOOSENAME = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private int col = 0;
    private int row = 0;
    private List<AnQuanTaiZhangModel> listSize;
    private List<AnQuanJianChaModel> anQuanJianChaModelList;

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setVisibility(View.GONE);
        setTopName("安全工器具检查记录");
        addTableTitle();
        readData();
        addCenterView(row);
        addBottomView();
        setData();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        removeTopLayoutViewAll();
        //标题
        String[] titles = getResources().getStringArray(R.array.anquan_jiancha);
        col = titles.length;
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,1);
        for (int i = 0; i < col; i++) {
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            drawTextView.setLayoutParams(layoutParams);
            drawTextView.setText(titles[i]);
            view.addView(drawTextView);
        }
        addTopLayoutView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(int row) {
        for (int j = 0; j < row; j++) {
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            for (int i = 0; i < col; i++) {
                DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,1);
                if (i == 0) {
                    drawEditText.setText((pLayout.getChildCount() + 1) + "");
                    drawEditText.setEnabled(false);
                } else if (i == 1) {
                    drawEditText.setEnabled(false);
                }
                drawEditText.setLayoutParams(layoutParams);
                view.addView(drawEditText);
            }
            pLayout.addView(view);
        }
    }

    /**
     * 添加底部签名
     */
    private void addBottomView() {
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < col; i++) {
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,1);
            if (i == 0) {
                drawTextView.setText("签名");
            }
            if (i > 1) {
                drawTextView.setTag("CHOOSENAME");
                drawTextView.setOnClickListener(this);
            }
            drawTextView.setLayoutParams(layoutParams);
            view.addView(drawTextView);
        }
        pLayout.addView(view);
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        if ("CHOOSENAME".equals(v.getTag())) {
            Intent intent = new Intent(this, LineLockActivity.class);
            intent.putExtra("isChoose", true);
            startActivityForResult(intent, CHOOSENAME);
            return;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSENAME:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 读取数据
     */
    private void readData() {
        listSize = DataSupport.findAll(AnQuanTaiZhangModel.class);
        if (listSize != null && listSize.size() > 0) {
            row = listSize.size();
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        LinearLayout mingChengLayout = (LinearLayout) pLayout.getChildAt(pLayout.getChildCount() - 1);
        if (listSize != null && listSize.size() > 0) {
            for (int i = 0, size = listSize.size(); i < size; i++) {
                AnQuanTaiZhangModel taiZhangModel = listSize.get(i);
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                ((DrawEditText) linearLayout.getChildAt(1)).setText(taiZhangModel.getMc());
                anQuanJianChaModelList = DataSupport.where("mc = ?", taiZhangModel.getMc()).find(AnQuanJianChaModel.class);
                if (anQuanJianChaModelList != null && anQuanJianChaModelList.size() > 0) {
                    for (int j = 0, plCh = anQuanJianChaModelList.size(); j < plCh; j++) {
                        AnQuanJianChaModel model = anQuanJianChaModelList.get(j);
                        if ((j + 2) < 14) {
                            ((DrawEditText) linearLayout.getChildAt(j + 2)).setText(model.getNr());
                            ((DrawTextView) mingChengLayout.getChildAt(j + 2)).setText(model.getJcr());
                        }
                    }
                }
            }
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        DataSupport.deleteAll(AnQuanJianChaModel.class);
        int plCh = pLayout.getChildCount();
        LinearLayout qianMingLayout = (LinearLayout) pLayout.getChildAt(plCh - 1);
        List<AnQuanJianChaModel> anQuanJianChaModelList = new ArrayList<>();
        for (int i = 0; i < plCh - 1; i++) {
            LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
            for (int j = 1; j <= 12; j++) {
                AnQuanJianChaModel model = new AnQuanJianChaModel();
                model.setMc(((DrawEditText) linearLayout.getChildAt(1)).getText().toString());
                model.setYf(j);
                model.setNr(((DrawEditText) linearLayout.getChildAt(j + 1)).getText().toString());
                model.setJcr(((DrawTextView) qianMingLayout.getChildAt(j + 1)).getText().toString());
                anQuanJianChaModelList.add(model);
            }
        }
        DataSupport.saveAll(anQuanJianChaModelList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
