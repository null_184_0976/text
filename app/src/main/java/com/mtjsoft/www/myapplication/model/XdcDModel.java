package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj  使用litepal数据库
 *         Created by Administrator on 2018/3/8.
 *         蓄电池电压值表
 */

public class XdcDModel extends DataSupport {
    private int id;
    private int xdcZId;
    private int xh;
    private String dy;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getXdcZId() {
        return xdcZId;
    }

    public void setXdcZId(int xdcZId) {
        this.xdcZId = xdcZId;
    }

    public int getXh() {
        return xh;
    }

    public void setXh(int xh) {
        this.xh = xh;
    }

    public String getDy() {
        return dy;
    }

    public void setDy(String dy) {
        this.dy = dy;
    }
}
