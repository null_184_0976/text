package com.mtjsoft.www.myapplication.ui;

import android.content.Intent;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.YunWeiShiGuModel;
import com.mtjsoft.www.myapplication.utils.HHDensityUtils;
import com.mtjsoft.www.myapplication.utils.HHInputUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mtj 事故预想记录
 *         Created by Administrator on 2018/3/11.
 */

public class YunWeiShiGuActivity extends BaseActivity {

    private static final int CHOOSEBIANDIAN = 1001;
    private static final int CHOOSEGJ = 1002;
    private static final int CHOOSENAME = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private List<DrawTextView> drawTextViewList = new ArrayList<>();
    private List<DrawEditText> drawEditTextList = new ArrayList<>();

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setVisibility(View.GONE);
        setTopName("事故预想记录");
        addCenterView("班站名称：", true, "主持人：", true);
        addCenterView("活动时间：", true, "记录时间：", true);
        addCenterView("参加人员：", false);
        addBottomLayout("预想事故情况");
        addBottomLayout("运行方式");
        addBottomLayout("事故处理过程");
        addCenterView("答题时间：", true, "答题人：", true);
        addBottomLayout("评价");
        addCenterView("班组评议人", true, "评议时间", true);
        addCenterView("培训专责", false, "评价时间", true);
        addBottomLayout("备注");
        setData();
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(String leftName, boolean leftIsText, String rightName, boolean rightIsText) {
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < 4; i++) {
            DrawTextView drawTextView = null;
            DrawEditText drawEditText = null;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            switch (i) {
                case 0:
                    drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(leftName);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                    break;
                case 1:
                    if (leftIsText) {
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setLayoutParams(layoutParams);
                        drawTextView.setOnClickListener(this);
                        drawTextView.setId(drawTextViewList.size());
                        drawTextViewList.add(drawTextView);
                        view.addView(drawTextView);
                    } else {
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        drawEditTextList.add(drawEditText);
                        view.addView(drawEditText);
                    }
                    break;
                case 2:
                    drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(rightName);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                    break;
                case 3:
                    if (rightIsText) {
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setLayoutParams(layoutParams);
                        drawTextView.setOnClickListener(this);
                        drawTextView.setId(drawTextViewList.size());
                        drawTextViewList.add(drawTextView);
                        view.addView(drawTextView);
                    } else {
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        drawEditTextList.add(drawEditText);
                        view.addView(drawEditText);
                    }
                    break;
                default:
                    break;
            }
        }
        pLayout.addView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(String leftName, boolean leftIsText) {
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < 2; i++) {
            DrawTextView drawTextView = null;
            DrawEditText drawEditText = null;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            switch (i) {
                case 0:
                    drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(leftName);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                    break;
                case 1:
                    layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 3);
                    if (leftIsText) {
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setLayoutParams(layoutParams);
                        drawTextView.setOnClickListener(this);
                        drawTextView.setId(drawTextViewList.size());
                        drawTextViewList.add(drawTextView);
                        view.addView(drawTextView);
                    } else {
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        drawEditTextList.add(drawEditText);
                        view.addView(drawEditText);
                    }
                    break;
                default:
                    break;
            }
        }
        pLayout.addView(view);
    }

    private void addBottomLayout(String bottomName) {
        DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText(bottomName);
        drawTextView.setPadding(16, 0, 0, 0);
        drawTextView.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
        pLayout.addView(drawTextView);
        //
        DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
        drawEditText.setMinHeight(HHDensityUtils.dip2px(getBaseContext(), 200));
        drawEditText.setLayoutParams(layoutParams);
        drawEditTextList.add(drawEditText);
        pLayout.addView(drawEditText);
    }


    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        Intent intent = null;
        switch (v.getId()) {
            case 0:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 1);
                startActivityForResult(intent, CHOOSEBIANDIAN);
                break;
            case 2:
            case 3:
            case 4:
            case 7:
            case 8:
                HHInputUtils.chooseTime(true, clickText);
                break;
            case 1:
            case 5:
            case 6:
                intent = new Intent(this, LineLockActivity.class);
                intent.putExtra("isChoose", true);
                startActivityForResult(intent, CHOOSENAME);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSENAME:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        YunWeiShiGuModel model = DataSupport.findLast(YunWeiShiGuModel.class);
        if (model != null) {
            drawEditTextList.get(0).setText(model.getCjry());
            drawEditTextList.get(1).setText(model.getYxsgqk());
            drawEditTextList.get(2).setText(model.getYxfs());
            drawEditTextList.get(3).setText(model.getSgclgc());
            drawEditTextList.get(4).setText(model.getPj());
            drawEditTextList.get(5).setText(model.getPxzz());
            drawEditTextList.get(6).setText(model.getBz());
            //
            drawTextViewList.get(0).setText(model.getBz());
            drawTextViewList.get(1).setText(model.getZcr());
            drawTextViewList.get(2).setText(model.getHdsj());
            drawTextViewList.get(3).setText(model.getJlsj());
            drawTextViewList.get(4).setText(model.getDtsj());
            drawTextViewList.get(5).setText(model.getDtr());
            drawTextViewList.get(6).setText(model.getBzpyr());
            drawTextViewList.get(7).setText(model.getPysj());
            drawTextViewList.get(8).setText(model.getPjsj());
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        YunWeiShiGuModel model = new YunWeiShiGuModel();
        model.setCjry(drawEditTextList.get(0).getText().toString());
        model.setYxsgqk(drawEditTextList.get(1).getText().toString());
        model.setYxfs(drawEditTextList.get(2).getText().toString());
        model.setSgclgc(drawEditTextList.get(3).getText().toString());
        model.setPj(drawEditTextList.get(4).getText().toString());
        model.setPxzz(drawEditTextList.get(5).getText().toString());
        model.setBz(drawEditTextList.get(6).getText().toString());
        //
        model.setBz(drawTextViewList.get(0).getText().toString());
        model.setZcr(drawTextViewList.get(1).getText().toString());
        model.setHdsj(drawTextViewList.get(2).getText().toString());
        model.setJlsj(drawTextViewList.get(3).getText().toString());
        model.setDtsj(drawTextViewList.get(4).getText().toString());
        model.setDtr(drawTextViewList.get(5).getText().toString());
        model.setBzpyr(drawTextViewList.get(6).getText().toString());
        model.setPysj(drawTextViewList.get(7).getText().toString());
        model.setPjsj(drawTextViewList.get(8).getText().toString());
        model.save();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
