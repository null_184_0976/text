package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 在线监测装置台账及告警值设定、修改记录
 *         Created by Administrator on 2018/3/13.
 */

public class ZaiXianJianCeModel extends DataSupport {
    private int id;//主键id
    private String zm;//		站名
    private String zzmc;//装置名称
    private String azwz;//安装位置
    private String cj;//厂家
    private String xh;//型号
    private String tyrq;//投运日期
    private String cszsd;//	初始值设定
    private String cs;//次数
    private String xgdz;//修改定值

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getZm() {
        return zm;
    }

    public void setZm(String zm) {
        this.zm = zm;
    }

    public String getZzmc() {
        return zzmc;
    }

    public void setZzmc(String zzmc) {
        this.zzmc = zzmc;
    }

    public String getAzwz() {
        return azwz;
    }

    public void setAzwz(String azwz) {
        this.azwz = azwz;
    }

    public String getCj() {
        return cj;
    }

    public void setCj(String cj) {
        this.cj = cj;
    }

    public String getXh() {
        return xh;
    }

    public void setXh(String xh) {
        this.xh = xh;
    }

    public String getTyrq() {
        return tyrq;
    }

    public void setTyrq(String tyrq) {
        this.tyrq = tyrq;
    }

    public String getCszsd() {
        return cszsd;
    }

    public void setCszsd(String cszsd) {
        this.cszsd = cszsd;
    }

    public String getCs() {
        return cs;
    }

    public void setCs(String cs) {
        this.cs = cs;
    }

    public String getXgdz() {
        return xgdz;
    }

    public void setXgdz(String xgdz) {
        this.xgdz = xgdz;
    }
}
