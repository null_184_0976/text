package com.mtjsoft.www.myapplication.ui;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import android.widget.Toast;

import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.adapter.MyAdapter;

import com.mtjsoft.www.myapplication.utils.CustomPopWindow;
import com.mtjsoft.www.myapplication.utils.Picture;


;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    /**
     * 新版界面相关信息：九宫格风格
     */
    private GridView mGridView;
    private String IMAGE_ITEM = "imgage_item";
    private String TEXT_ITEM = "text_item";


    private CustomPopWindow yunweimCustomPopWindow;

    //数据源
    private String[] list = new String[]{"工作日志", "巡视记录", "缺陷记录", "试验记录", "保护自动装置",
            "跳闸记录", "避雷器记录", "测温记录", "运维分析", "反事故演习",
            "解锁钥匙", "蓄电池", "事故预想", "记录检查"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mGridView = (GridView) findViewById(R.id.gridview);


        MyAdapter saImageItems = new MyAdapter(this, mGridView);

        // 设置GridView的adapter。GridView继承于AbsListView。
        mGridView.setAdapter(saImageItems);
        mGridView.setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> adapter, View view, int position, long rowid) {
        // 根据元素位置获取对应的值
     /*   Picture textView = (Picture) adapter.getItemAtPosition(position);
        String itemText = textView.getTitle();*/
        Picture textView = (Picture) adapter.getItemAtPosition(position);
        String itemText = textView.getTitle();


        if ("运维计划".equals(itemText)) {

            showPopMenu(view);
        }


        if ("生产准备".equals(itemText)) {
            Intent intent = new Intent(this.getApplicationContext(), YiJiaoQingDanActivity.class);
            startActivity(intent);

        }


        if ("运行规程".equals(itemText)) {
            Intent intent = new Intent(this.getApplicationContext(), GuiChengShenPiActivity.class);
            startActivity(intent);

        }

        if ("外来人员".equals(itemText)) {
            Intent intent = new Intent(this.getApplicationContext(), WaiLaiJiLuActivity.class);
            startActivity(intent);
        }

        if ("备品备件".equals(itemText)) {
            bpbjshowPopMenu(view);
        }


        if ("变电站设备载流参数表".equals(itemText)) {
            Intent intent = new Intent(this.getApplicationContext(), SheBeiZaiLiuActivity.class);
            startActivity(intent);
        }


        if ("图纸资料".equals(itemText)) {
              /*  Intent intent = new Intent(this.getApplicationContext(), Main2Activity.class);
            startActivity(intent);*/
            Toast.makeText(this, "等会就做", Toast.LENGTH_SHORT).show();
        }


        if ("安全工器具".equals(itemText)) {
            aqgqjshowPopMenu(view);
        }


        if ("仪表工具".equals(itemText)) {
            ybgjhowPopMenu(view);
        }


        if ("生产用车".equals(itemText)) {
            Intent intent = new Intent(this.getApplicationContext(), ShenChanYongCheActivity.class);
            startActivity(intent);
        }

        if ("维护工作".equals(itemText)) {
            Intent intent = new Intent(this.getApplicationContext(), BiaoZhunWeiHuActivity.class);
            startActivity(intent);
        }

        if ("消防设施".equals(itemText)) {
            xfssshowPopMenu(view);
        }


        if ("防汛设施".equals(itemText)) {
            fxssshowPopMenu(view);
        }


        if ("周边隐患".equals(itemText)) {
            Intent intent = new Intent(this.getApplicationContext(), ZbYingHuanActivity.class);
            startActivity(intent);
        }

        if ("设备巡视".equals(itemText)) {
              /*  Intent intent = new Intent(this.getApplicationContext(), Main2Activity.class);
            startActivity(intent);*/
            Toast.makeText(this, "等会就做", Toast.LENGTH_SHORT).show();
        }

        if ("危险点".equals(itemText)) {
            Intent intent = new Intent(this.getApplicationContext(), WeiXianDianActivity.class);
            startActivity(intent);
        }


        if ("防误闭锁".equals(itemText)) {
            fwbsshowPopMenu(view);
        }


        if ("在线监测".equals(itemText)) {
            Intent intent = new Intent(this.getApplicationContext(), ZaiXianJianCeActivity.class);
            startActivity(intent);
        }


        if ("机器人".equals(itemText)) {
            Intent intent = new Intent(this.getApplicationContext(), ZhiNengXunJianActivity.class);
            startActivity(intent);
        }

        if ("运维记录".equals(itemText)) {
              /*  Intent intent = new Intent(this.getApplicationContext(), Main2Activity.class);
            startActivity(intent);*/

//            selectTypeDiag(list);
            selectTypeDiag(Arrays.asList(list));

        }

    }

    /**
     *20、备品备件（台账记录/使用记录/检查记录）
     */
    private void bpbjshowPopMenu(View view) {
        View contentView = LayoutInflater.from(this).inflate(R.layout.layout_bpbj_popu, null);
        //处理popWindow 显示内容
        bpbjhandleLogic(contentView);
        //创建并显示popWindow
        yunweimCustomPopWindow = new CustomPopWindow.PopupWindowBuilder(this)
                .setView(contentView)
                .create()
                .showAsDropDown(view, 30, 0);


    }

    /**
     20、备品备件（台账记录/使用记录/检查记录）
     *
     * @param contentView
     */
    private void bpbjhandleLogic(View contentView) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (yunweimCustomPopWindow != null) {
                    yunweimCustomPopWindow.dissmiss();
                }

                switch (v.getId()) {
                    case R.id.ll_bpbj_jcjl:
                        Intent intent = new Intent(MainActivity.this, BeiPinJianChaActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.ll_bpbj_syjl:
                        Intent intent1 = new Intent(MainActivity.this, BeiPinShiYongActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.ll_bpbj_tzjl:
                        Intent intent2 = new Intent(MainActivity.this, BeiPinTaiZhangActivity.class);
                        startActivity(intent2);
                        break;

                }

            }
        };
        contentView.findViewById(R.id.ll_bpbj_jcjl).setOnClickListener(listener);
        contentView.findViewById(R.id.ll_bpbj_syjl).setOnClickListener(listener);
        contentView.findViewById(R.id.ll_bpbj_tzjl).setOnClickListener(listener);

    }


    /**
     * 15、安全工器具（台账记录/检查记录/试验计划）
     */
    private void aqgqjshowPopMenu(View view) {
        View contentView = LayoutInflater.from(this).inflate(R.layout.layout_aqgqj_popu, null);
        //处理popWindow 显示内容
        aqgqjhandleLogic(contentView);
        //创建并显示popWindow
        yunweimCustomPopWindow = new CustomPopWindow.PopupWindowBuilder(this)
                .setView(contentView)
                .create()
                .showAsDropDown(view, 30, 0);


    }

    /**
     * 15、安全工器具（台账记录/检查记录/试验计划）
     *
     * @param contentView
     */
    private void aqgqjhandleLogic(View contentView) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (yunweimCustomPopWindow != null) {
                    yunweimCustomPopWindow.dissmiss();
                }

                switch (v.getId()) {
                    case R.id.ll_aqgqj_syjh:
                        Intent intent = new Intent(MainActivity.this, AnQuanShiYanActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.ll_aqgqj_jcjl:
                        Intent intent1 = new Intent(MainActivity.this, AnQuanJianChaActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.ll_aqgqj_tzjl:
                        Intent intent2 = new Intent(MainActivity.this, AnQuanTaiZhangActivity.class);
                        startActivity(intent2);
                        break;

                }

            }
        };
        contentView.findViewById(R.id.ll_aqgqj_syjh).setOnClickListener(listener);
        contentView.findViewById(R.id.ll_aqgqj_jcjl).setOnClickListener(listener);
        contentView.findViewById(R.id.ll_aqgqj_tzjl).setOnClickListener(listener);

    }


    /**
     * 17、仪表工具（台账记录/借用记录）
     */
    private void ybgjhowPopMenu(View view) {
        View contentView = LayoutInflater.from(this).inflate(R.layout.layout_ybgj_popu, null);
        //处理popWindow 显示内容
        ybgjhandleLogic(contentView);
        //创建并显示popWindow
        yunweimCustomPopWindow = new CustomPopWindow.PopupWindowBuilder(this)
                .setView(contentView)
                .create()
                .showAsDropDown(view, 30, 0);


    }

    /**
     * 17、仪表工具（台账记录/借用记录）
     *
     * @param contentView
     */
    private void ybgjhandleLogic(View contentView) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (yunweimCustomPopWindow != null) {
                    yunweimCustomPopWindow.dissmiss();
                }

                switch (v.getId()) {
                    case R.id.ll_ybgj_jyjl:
                        Intent intent = new Intent(MainActivity.this, YiQiJieYongActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.ll_ybgj_tzjl:
                        Intent intent1 = new Intent(MainActivity.this, YiQiTaiZhangActivity.class);
                        startActivity(intent1);
                        break;

                }

            }
        };
        contentView.findViewById(R.id.ll_ybgj_jyjl).setOnClickListener(listener);
        contentView.findViewById(R.id.ll_ybgj_tzjl).setOnClickListener(listener);


    }

    /**
     * 12、防汛设施（物资台账/检查表）
     */
    private void fxssshowPopMenu(View view) {
        View contentView = LayoutInflater.from(this).inflate(R.layout.layout_fsss_popu, null);
        //处理popWindow 显示内容
        fxsshandleLogic(contentView);
        //创建并显示popWindow
        yunweimCustomPopWindow = new CustomPopWindow.PopupWindowBuilder(this)
                .setView(contentView)
                .create()
                .showAsDropDown(view, 30, 0);


    }

    /**
     * 12、防汛设施（物资台账/检查表）
     *
     * @param contentView
     */
    private void fxsshandleLogic(View contentView) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (yunweimCustomPopWindow != null) {
                    yunweimCustomPopWindow.dissmiss();
                }

                switch (v.getId()) {
                    case R.id.ll_fxss_wztz:
                        Intent intent = new Intent(MainActivity.this, FangXunTaiZhangActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.ll_fxss_jcb:
                        Intent intent1 = new Intent(MainActivity.this, FangXunJianChaActivity.class);
                        startActivity(intent1);
                        break;

                }

            }
        };
        contentView.findViewById(R.id.ll_fxss_jcb).setOnClickListener(listener);
        contentView.findViewById(R.id.ll_fxss_wztz).setOnClickListener(listener);


    }

    /**
     * 10、消防设施（台账记录/检查表）
     */
    private void xfssshowPopMenu(View view) {
        View contentView = LayoutInflater.from(this).inflate(R.layout.layout_sfss_popu, null);
        //处理popWindow 显示内容
        xfsshandleLogic(contentView);
        //创建并显示popWindow
        yunweimCustomPopWindow = new CustomPopWindow.PopupWindowBuilder(this)
                .setView(contentView)
                .create()
                .showAsDropDown(view, 30, 0);


    }

    /**
     * 10、消防设施（台账记录/检查表）
     *
     * @param contentView
     */
    private void xfsshandleLogic(View contentView) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (yunweimCustomPopWindow != null) {
                    yunweimCustomPopWindow.dissmiss();
                }

                switch (v.getId()) {
                    case R.id.ll_sfss_jcb:
                        Intent intent = new Intent(MainActivity.this, XiaoFangJianChaActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.ll_sfss_tzjl:
                        Intent intent1 = new Intent(MainActivity.this, XiaoFangTaiZhangActivity.class);
                        startActivity(intent1);
                        break;

                }

            }
        };
        contentView.findViewById(R.id.ll_sfss_jcb).setOnClickListener(listener);
        contentView.findViewById(R.id.ll_sfss_tzjl).setOnClickListener(listener);


    }

    /**
     * 6、防误闭锁（装置台账/检查记录）
     */
    private void fwbsshowPopMenu(View view) {
        View contentView = LayoutInflater.from(this).inflate(R.layout.layout_fwbs_popu, null);
        //处理popWindow 显示内容
        fwbshandleLogic(contentView);
        //创建并显示popWindow
        yunweimCustomPopWindow = new CustomPopWindow.PopupWindowBuilder(this)
                .setView(contentView)
                .create()
                .showAsDropDown(view, 30, 0);


    }

    /**
     * 6、防误闭锁（装置台账/检查记录）
     *
     * @param contentView
     */
    private void fwbshandleLogic(View contentView) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (yunweimCustomPopWindow != null) {
                    yunweimCustomPopWindow.dissmiss();
                }

                switch (v.getId()) {
                    case R.id.ll_fwbs_jcjl:
                        Intent intent = new Intent(MainActivity.this, FangWuTaiZhangActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.ll_fwbs_zztz:
                        Intent intent1 = new Intent(MainActivity.this, FangWuJianChaActivity.class);
                        startActivity(intent1);
                        break;

                }

            }
        };
        contentView.findViewById(R.id.ll_fwbs_zztz).setOnClickListener(listener);
        contentView.findViewById(R.id.ll_fwbs_jcjl).setOnClickListener(listener);


    }


    /**
     * 1、运维计划（年计划/月计划/周计划）
     */
    private void showPopMenu(View view) {
        View contentView = LayoutInflater.from(this).inflate(R.layout.lv_item_popu, null);
        //处理popWindow 显示内容
        handleLogic(contentView);
        //创建并显示popWindow
        yunweimCustomPopWindow = new CustomPopWindow.PopupWindowBuilder(this)
                .setView(contentView)
                .create()
                .showAsDropDown(view, 30, 0);


    }

    /**
     * 1、运维计划（年计划/月计划/周计划）
     *
     * @param contentView
     */
    private void handleLogic(View contentView) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (yunweimCustomPopWindow != null) {
                    yunweimCustomPopWindow.dissmiss();
                }

                switch (v.getId()) {
                    case R.id.ll_njh:
                        Intent intent = new Intent(MainActivity.this, YunWeiNianActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.ll_yjh:
                        Intent intent1 = new Intent(MainActivity.this, YunWeiYueActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.ll_zjh:
                        Intent intent2 = new Intent(MainActivity.this, YunWeiZhouActivity.class);
                        startActivity(intent2);
                        break;

                }

            }
        };
        contentView.findViewById(R.id.ll_njh).setOnClickListener(listener);
        contentView.findViewById(R.id.ll_yjh).setOnClickListener(listener);
        contentView.findViewById(R.id.ll_zjh).setOnClickListener(listener);

    }


    /**
     * //运维记录下拉表
     */

    public void selectTypeDiag(final List<String> list) {
        int len = list.size();
        final String items[] = new String[len];//不能直接用list.size，会出错
        for (int i = 0; i < list.size(); i++) {
            items[i] = list.get(i);
        }
        //dialog参数设置
        AlertDialog.Builder builder = new AlertDialog.Builder(this);  //先得到构造器
        builder.setTitle("运维记录表"); //设置标题
        //builder.setMessage("是否确认退出?"); //设置内容
        //builder.setIcon(R.mipmap.ic_launcher);//设置图标，图片id即可
        //设置列表显示，注意设置了列表显示就不要设置builder.setMessage()了，否则列表不起作用。
//        builder.setSingleChoiceItems()
        builder.setSingleChoiceItems(items, len, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String listText = list.get(which);

                if ("工作日志".equals(listText)) {
                    Intent intent = new Intent(MainActivity.this, YunWeiRiZhiActivity.class);
                    startActivity(intent);
                }
                if ("巡视记录".equals(listText)) {
                    Intent intent = new Intent(MainActivity.this, YunWeiXunShiJiLuActivity.class);
                    startActivity(intent);
                }
                if ("缺陷记录".equals(listText)) {
                    Intent intent = new Intent(MainActivity.this, YunWeiQueXianActivity.class);
                    startActivity(intent);
                }
                if ("试验记录".equals(listText)) {
                    Intent intent = new Intent(MainActivity.this, YunWeiShiYanActivity.class);
                    startActivity(intent);
                }
                if ("保护自动装置".equals(listText)) {
                    Intent intent = new Intent(MainActivity.this, YunWeiZiDongActivity.class);
                    startActivity(intent);
                }
                if ("跳闸记录".equals(listText)) {
                    Intent intent = new Intent(MainActivity.this, DuanLuTiaoZhaActivity.class);
                    startActivity(intent);


                }
                if ("避雷器记录".equals(listText)) {
                    Intent intent = new Intent(MainActivity.this, BiLeiJiLuActivity.class);
                    startActivity(intent);
                }
                if ("测温记录".equals(listText)) {
                    Intent intent = new Intent(MainActivity.this, YunWeiCeWenActivity.class);
                    startActivity(intent);
                }
                if ("运维分析".equals(listText)) {
                    Intent intent = new Intent(MainActivity.this, YunWeiFenXiActivity.class);
                    startActivity(intent);
                }
                if ("反事故演习".equals(listText)) {
                    Intent intent = new Intent(MainActivity.this, YunWeiYanXiActivity.class);
                    startActivity(intent);
                }
                if ("解锁钥匙".equals(listText)) {
                    Intent intent = new Intent(MainActivity.this, YunWeiJieSuoActivity.class);
                    startActivity(intent);
                }
                if ("蓄电池".equals(listText)) {
                    Intent intent = new Intent(MainActivity.this, XuDianChiActivity.class);
                    startActivity(intent);
                }
                if ("事故预想".equals(listText)) {
                    Intent intent = new Intent(MainActivity.this, YunWeiShiGuActivity.class);
                    startActivity(intent);
                }
                if ("记录检查".equals(listText)) {
                    Intent intent = new Intent(MainActivity.this, YunWeiTaiZhangActivity.class);
                    startActivity(intent);
                }
                dialog.dismiss();

            }
        });
        builder.create();
        builder.create().show();
    }

}