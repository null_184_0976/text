package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 测温部位记录
 *         Created by Administrator on 2018/3/20.
 */

public class YunWeiCeWenDModel extends DataSupport {
    private int id;//主键id
    private String sbcw_id;//	主表id
    private String frbw;//发热部位
    private String Axwd;//A相温度（℃）
    private String Bxwd;//B相温度（℃）
    private String Cxwd;//C相温度（℃）
    private String eddl;//额定电流（A）
    private String fhdl;//负荷电流（A）

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSbcw_id() {
        return sbcw_id;
    }

    public void setSbcw_id(String sbcw_id) {
        this.sbcw_id = sbcw_id;
    }

    public String getFrbw() {
        return frbw;
    }

    public void setFrbw(String frbw) {
        this.frbw = frbw;
    }

    public String getAxwd() {
        return Axwd;
    }

    public void setAxwd(String axwd) {
        Axwd = axwd;
    }

    public String getBxwd() {
        return Bxwd;
    }

    public void setBxwd(String bxwd) {
        Bxwd = bxwd;
    }

    public String getCxwd() {
        return Cxwd;
    }

    public void setCxwd(String cxwd) {
        Cxwd = cxwd;
    }

    public String getEddl() {
        return eddl;
    }

    public void setEddl(String eddl) {
        this.eddl = eddl;
    }

    public String getFhdl() {
        return fhdl;
    }

    public void setFhdl(String fhdl) {
        this.fhdl = fhdl;
    }
}
