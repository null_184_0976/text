package com.mtjsoft.www.myapplication.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;

public class UserInfoUtils {
    private static final String CONFIG_NAME = "DianWangDemo";
    public static final String FRIST = "frist";
    public static final String USER_NAME = "userName";
    public static final String LOST_DIAN_CHI_COUNT = "lost_dian_chi_count";

    /**
     * 获取SharedPreferences
     *
     * @param context
     * @return
     */
    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(CONFIG_NAME, Context.MODE_PRIVATE);
    }

    /**
     * 获取Editor
     *
     * @param context
     * @return
     */
    private static Editor getEditor(Context context) {
        return getSharedPreferences(context).edit();
    }

    /**
     * 判断用户是第一次进入
     *
     * @param context
     * @return
     */
    public static boolean isFrist(Context context) {
        SharedPreferences preferences = getSharedPreferences(context);
        String isFrist = preferences.getString(FRIST, "");
        if (TextUtils.isEmpty(isFrist)) {
            return true;
        }
        return false;
    }

    /**
     * 获取用户的配置信息
     *
     * @param context
     * @param paramName
     * @return
     */
    public static String getUserInfo(Context context, String paramName) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        return sharedPreferences.getString(paramName, "");
    }

    /**
     * 保存用户的配置信息
     *
     * @param context
     * @param paramName
     * @param paramValue
     */
    public static void saveUserInfo(Context context, String paramName,
                                    String paramValue) {
        Editor edit = getEditor(context);
        edit.putString(paramName, paramValue);
        edit.commit();
    }

    /**
     * 重置信息
     *
     * @param mContext
     * @return
     */
    public static void resetUserInfo(Context mContext) {
        // 载入配置文件
        SharedPreferences sp = mContext.getSharedPreferences(CONFIG_NAME, 0);
        // 写入配置文件
        Editor spEd = sp.edit();
        spEd.putString(FRIST, "");
        spEd.commit();
    }

}
