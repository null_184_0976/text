package com.mtjsoft.www.myapplication.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.model.AnQuanShiYanModel;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mtj 安全工器具试验计划表
 *         Created by Administrator on 2018/3/11.
 */

public class AnQuanShiYanActivity extends BaseActivity {

    private static final int CHOOSEBIANDIAN = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private int col = 0;
    private int row = 20;
    private List<AnQuanShiYanModel> anQuanShiYanModelList;
    //
    private DrawEditText dwEditText;
    private DrawTextView zhanTextView;

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setOnClickListener(this);
        setTopName("安全工器具试验计划表");
        addTableTitle();
        readData();
        addCenterView(row);
        setData();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        removeTopLayoutViewAll();
        String[] topsname = getResources().getStringArray(R.array.anquan_shiyan_top);
        LinearLayout viewTop = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,1);
        for (int i = 0; i < 4; i++) {
            switch (i) {
                case 0:
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(topsname[0]);
                    drawTextView.setLayoutParams(layoutParams);
                    viewTop.addView(drawTextView);
                    break;
                case 1:
                    dwEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    dwEditText.setLayoutParams(layoutParams);
                    viewTop.addView(dwEditText);
                    break;
                case 2:
                    DrawTextView drawTextView2 = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView2.setText(topsname[1]);
                    drawTextView2.setLayoutParams(layoutParams);
                    viewTop.addView(drawTextView2);
                    break;
                case 3:
                    zhanTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    zhanTextView.setLayoutParams(layoutParams);
                    zhanTextView.setTag("chooseBDZ");
                    zhanTextView.setOnClickListener(this);
                    viewTop.addView(zhanTextView);
                    break;
                default:
                    break;
            }
        }
        addTopLayoutView(viewTop);
        //标题
        String[] titles = getResources().getStringArray(R.array.anquan_shiyan);
        col = titles.length;
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < col; i++) {
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            if ((i + 1) % 2 == 0) {
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,2);
            } else {
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,1);
            }
            drawTextView.setLayoutParams(layoutParams);
            drawTextView.setText(titles[i]);
            view.addView(drawTextView);
        }
        addTopLayoutView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(int row) {
        for (int j = 0; j < row; j++) {
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            for (int i = 0; i < col; i++) {
                if (i == 1 || i == 3) {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,2);
                    drawEditText.setLayoutParams(layoutParams);
                    view.addView(drawEditText);
                } else if (i == 2 || i == 6) {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,1);
                    drawEditText.setLayoutParams(layoutParams);
                    view.addView(drawEditText);
                } else {
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,2);
                    drawTextView.setTag(i);
                    if (i == 0) {
                        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,1);
                        drawTextView.setText((pLayout.getChildCount() + 1) + "");
                    }
                    if (i == 4) {
                        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,1);
                    }
                    drawTextView.setOnClickListener(this);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                }
            }
            pLayout.addView(view);
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        if (v.getId() == R.id.iv_add_row) {
            addCenterView(5);
            return;
        }
        if ("chooseBDZ".equals(v.getTag())) {
            Intent intent = new Intent(this, ChooseActivity.class);
            intent.putExtra("type", 1);
            startActivityForResult(intent, CHOOSEBIANDIAN);
            return;
        }
        int tag = (int) v.getTag();
        switch (tag) {
            case 4:
            case 5:
            case 7:
                chooseTime();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 选择日期
     */
    private void chooseTime() {
        //时间选择器
        TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                clickText.setText(TurnsUtils.convertToString(date,"yyyy-MM-dd"));
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(new boolean[]{true, true, true, false, false, false})
                .build();
        pvTime.show();
    }

    /**
     * 读取数据
     */
    private void readData() {
        anQuanShiYanModelList = DataSupport.findAll(AnQuanShiYanModel.class);
        if (anQuanShiYanModelList != null && anQuanShiYanModelList.size() > row) {
            row = anQuanShiYanModelList.size();
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        if (anQuanShiYanModelList != null && anQuanShiYanModelList.size() > 0) {
            for (int i = 0, plCh = anQuanShiYanModelList.size(); i < plCh; i++) {
                AnQuanShiYanModel model = anQuanShiYanModelList.get(i);
                dwEditText.setText(model.getDw());
                zhanTextView.setText(model.getBdzmc());
                if (pLayout.getChildAt(i) instanceof LinearLayout) {
                    LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                    ((DrawEditText) linearLayout.getChildAt(1)).setText(model.getGqjmc());
                    ((DrawEditText) linearLayout.getChildAt(2)).setText(model.getBh());
                    ((DrawEditText) linearLayout.getChildAt(3)).setText(model.getGgxh());
                    ((DrawTextView) linearLayout.getChildAt(4)).setText(model.getCcrq());
                    ((DrawTextView) linearLayout.getChildAt(5)).setText(model.getScsyrq());
                    ((DrawEditText) linearLayout.getChildAt(6)).setText(model.getSyzq());
                    ((DrawTextView) linearLayout.getChildAt(7)).setText(model.getXcsyrq());
                }
            }
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        DataSupport.deleteAll(AnQuanShiYanModel.class);
        List<AnQuanShiYanModel> anQuanShiYanModelList = new ArrayList<>();
        for (int i = 0, plCh = pLayout.getChildCount(); i < plCh; i++) {
            AnQuanShiYanModel model = new AnQuanShiYanModel();
            model.setBdzmc(zhanTextView.getText().toString());
            model.setDw(dwEditText.getText().toString().trim());
            if (pLayout.getChildAt(i) instanceof LinearLayout) {
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                DrawEditText drawTextView = (DrawEditText) linearLayout.getChildAt(1);
                if (!TextUtils.isEmpty(drawTextView.getText().toString())) {
                    model.setGqjmc(drawTextView.getText().toString());
                    model.setBh(((DrawEditText) linearLayout.getChildAt(2)).getText().toString());
                    model.setGgxh(((DrawEditText) linearLayout.getChildAt(3)).getText().toString());
                    model.setCcrq(((DrawTextView) linearLayout.getChildAt(4)).getText().toString());
                    model.setScsyrq(((DrawTextView) linearLayout.getChildAt(5)).getText().toString());
                    model.setSyzq(((DrawEditText) linearLayout.getChildAt(6)).getText().toString());
                    model.setXcsyrq(((DrawTextView) linearLayout.getChildAt(7)).getText().toString());
                    anQuanShiYanModelList.add(model);
                }
            }
        }
        DataSupport.saveAll(anQuanShiYanModelList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
