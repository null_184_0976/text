package com.mtjsoft.www.myapplication.utils;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;

import java.lang.reflect.Method;

/**
 * Created by suoheng002 on 2018/3/21.
 */

public  class PopupwindUtils extends PopupWindow {
    /**
     * 布局文件
     */
    private PopupWindow mPreviewPopup;
    private View view;

    /**
     * @param context 上下文
     * @param layoutResID 布局文件

     */
    public PopupwindUtils(Context context, int layoutResID,int width,int height ) {
        mPreviewPopup = new PopupWindow(context);
        view = LayoutInflater.from(context).inflate(layoutResID, null);
        mPreviewPopup.setWidth(width);
        mPreviewPopup.setHeight(height);
        mPreviewPopup.setContentView(view);
        mPreviewPopup.setBackgroundDrawable(new ColorDrawable(0x00000000));
    }

    /**
     * @param x 显示位置所在x坐标
     * @param y 显示位置所在y坐标
     */
    public void show(int x, int y) {
        mPreviewPopup.showAtLocation(view, Gravity.NO_GRAVITY, x, y);
    }

    /**
     * 关闭
     */
    public void dismiss() {
        if (mPreviewPopup != null) {
            mPreviewPopup.dismiss();
        }
    }

    /**
     * 当点击外部不消失窗口，并且能相应外部控件的点击事件
     */
    public void setPopupWindowTouchModal(boolean touchModal) {
        if (null == mPreviewPopup) {
            return;
        }
        Method method;
        try {
            method = PopupWindow.class.getDeclaredMethod("setTouchModal", boolean.class);
            method.setAccessible(true);
            method.invoke(mPreviewPopup, touchModal);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @author xc
     * @date 2016-3-31 下午2:43:15
     * @describe 初始化需要更新数据或者处理点击事件的控件
     */
    public void init() {

    }

}