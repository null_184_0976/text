package com.mtjsoft.www.myapplication.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.OptionsPickerView;
import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.YunWeiNModel;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author mtj 运维计划年度计划表
 *         Created by Administrator on 2018/3/9.
 */

public class YunWeiNianActivity extends BaseActivity {

    private static final int CHOOSENAME = 1001;
    private static final int CHOOSEWORKTYPE = 1002;
    private static final int CHOOSEBIANDIAN = 1003;
    private static final int CHOOSEZHIXING = 1004;
    private DrawTextView clickText;
    private LinearLayout pLayout;
    private ImageView addImageView;
    private String ban = "___";
    private String nian = "___";
    private List<YunWeiNModel> yunWeiNModelList;
    private int col = 0;
    private int row = 20;

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setOnClickListener(this);
        setTopName(String.format(getString(R.string.ban_nian), ban, nian));
        addTableTitle();
        readData();
        addCenterView(row);
        setData();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        String[] titles = getResources().getStringArray(R.array.yunwei_nian);
        col = titles.length;
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < col; i++) {
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
            if (i == 0 || i == col - 1) {
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            }
            drawTextView.setLayoutParams(layoutParams);
            drawTextView.setText(titles[i]);
            view.addView(drawTextView);
        }
        addTopLayoutView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(int row) {
        for (int j = 0; j < row; j++) {
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            for (int i = 0; i < col; i++) {
                if (i == 4) {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                    drawEditText.setLayoutParams(layoutParams);
                    drawEditText.setEnabled(true);
                    drawEditText.setTag(i);
                    view.addView(drawEditText);
                } else {
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                    drawTextView.setTag(i);
                    if (i == 0) {
                        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                        drawTextView.setText((pLayout.getChildCount() + 1) + "");
                    } else if (i == col - 1) {
                        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                        drawTextView.setOnClickListener(this);
                    } else {
                        drawTextView.setOnClickListener(this);
                    }
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                }
            }
            pLayout.addView(view);
        }
    }

    @Override
    public void onClick(final View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        if (v.getId() == R.id.iv_add_row) {
            addCenterView(5);
            return;
        }
        if (v.getId() == R.id.tv_base_top_name) {
            chooseOptions();
            return;
        }
        Intent intent = null;
        int tag = (int) v.getTag();
        switch (tag) {
            case 1:
                chooseTime();
                break;
            case 2:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 1);
                startActivityForResult(intent, CHOOSEBIANDIAN);
                break;
            case 3:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 0);
                startActivityForResult(intent, CHOOSEWORKTYPE);
                break;
            case 5:
                intent = new Intent(this, LineLockActivity.class);
                intent.putExtra("isChoose", true);
                startActivityForResult(intent, CHOOSENAME);
                break;
            case 6:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 2);
                startActivityForResult(intent, CHOOSEZHIXING);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSENAME:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                case CHOOSEWORKTYPE:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            String haveName = clickText.getText().toString();
                            if (TextUtils.isEmpty(haveName)) {
                                clickText.setText(name);
                            } else {
                                haveName = haveName.replace(name, "");
                                clickText.setText(name + "," + haveName);
                            }
                        }
                    }
                    break;
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                case CHOOSEZHIXING:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 选择日期
     */
    private void chooseTime() {
        //时间选择器
        TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                clickText.setText(TurnsUtils.convertToString(date,"yyyy-MM-dd"));
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(new boolean[]{true, true, true, false, false, false})
                .build();
        pvTime.show();
    }

    /**
     * 表名选择
     */
    private void chooseOptions() {
        //添加数据源
        int option1 = 0;
        int option2 = 0;
        final List<String> options1Items = new ArrayList<>();
        final List<String> options2Items = new ArrayList<>();
        String[] bans = getResources().getStringArray(R.array.bans);
        options1Items.addAll(Arrays.asList(bans));
        for (int i = 0; i < 30; i++) {
            options2Items.add((TurnsUtils.getSysYear() + i) + "");
        }
        OptionsPickerView pvOptions = new OptionsPickerView.Builder(this, new OptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int options2, int options3, View v) {
                //返回的分别是三个级别的选中位置
                ban = options1Items.get(options1);
                nian = options2Items.get(options2);
                clickText.setText(String.format(getString(R.string.ban_nian), ban, nian));
            }
        })
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(false)
                //设置选择的三级单位
                .setLabels(getString(R.string.ban), getString(R.string.nian), "")
                //循环与否
                .setCyclic(false, false, false)
                //设置默认选中项
                .setSelectOptions(option1, option2, 0)
                //点击外部dismiss default true
                .setOutSideCancelable(true)
                //是否显示为对话框样式
                .isDialog(true)
                .build();
        pvOptions.setNPicker(options1Items, options2Items, null);
        pvOptions.show();
    }

    /**
     * 读取数据
     */
    private void readData() {
        yunWeiNModelList = DataSupport.findAll(YunWeiNModel.class);
        if (yunWeiNModelList != null && yunWeiNModelList.size() > row) {
            row = yunWeiNModelList.size();
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        if (yunWeiNModelList != null && yunWeiNModelList.size() > 0) {
            for (int i = 0, plCh = yunWeiNModelList.size(); i < plCh; i++) {
                YunWeiNModel yunWeiNModel = yunWeiNModelList.get(i);
                ban = yunWeiNModel.getBzmc();
                nian = yunWeiNModel.getNf();
                setTopName(String.format(getString(R.string.ban_nian), ban, nian));
                if (pLayout.getChildAt(i) instanceof LinearLayout) {
                    LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                    ((DrawTextView) linearLayout.getChildAt(1)).setText(yunWeiNModel.getRq());
                    ((DrawTextView) linearLayout.getChildAt(2)).setText(yunWeiNModel.getBdz());
                    ((DrawTextView) linearLayout.getChildAt(3)).setText(yunWeiNModel.getGzlb());
                    ((DrawEditText) linearLayout.getChildAt(4)).setText(yunWeiNModel.getGznr());
                    ((DrawTextView) linearLayout.getChildAt(5)).setText(yunWeiNModel.getZrr());
                    ((DrawTextView) linearLayout.getChildAt(6)).setText(yunWeiNModel.getZxqk());
                }
            }
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        DataSupport.deleteAll(YunWeiNModel.class);
        List<YunWeiNModel> yunWeiNModelList = new ArrayList<>();
        for (int i = 0, plCh = pLayout.getChildCount(); i < plCh; i++) {
            YunWeiNModel yunWeiNModel = new YunWeiNModel();
            yunWeiNModel.setBzmc(ban);
            yunWeiNModel.setNf(nian);
            if (pLayout.getChildAt(i) instanceof LinearLayout) {
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                DrawTextView drawTextView = (DrawTextView) linearLayout.getChildAt(1);
                if (!TextUtils.isEmpty(drawTextView.getText().toString())) {
                    yunWeiNModel.setRq(drawTextView.getText().toString());
                    yunWeiNModel.setBdz(((DrawTextView) linearLayout.getChildAt(2)).getText().toString());
                    yunWeiNModel.setGzlb(((DrawTextView) linearLayout.getChildAt(3)).getText().toString());
                    yunWeiNModel.setGznr(((DrawEditText) linearLayout.getChildAt(4)).getText().toString());
                    yunWeiNModel.setZrr(((DrawTextView) linearLayout.getChildAt(5)).getText().toString());
                    yunWeiNModel.setZxqk(((DrawTextView) linearLayout.getChildAt(6)).getText().toString());
                    yunWeiNModelList.add(yunWeiNModel);
                }
            }
        }
        DataSupport.saveAll(yunWeiNModelList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
