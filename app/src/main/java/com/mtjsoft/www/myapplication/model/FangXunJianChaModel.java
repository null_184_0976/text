package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 防汛工作检查表模板
 *         Created by Administrator on 2018/3/13.
 */

public class FangXunJianChaModel extends DataSupport {
    private int id;//主键id
    private String zm;//站名
    private String jcrq;//		检查日期
    private String jcxm;//		检查项目
    private String jcnr;//		检查内容
    private String jcbz;//		检查标准
    private String jcr;//	检查人
    private String jcjg;//		检查结果

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getZm() {
        return zm;
    }

    public void setZm(String zm) {
        this.zm = zm;
    }

    public String getJcrq() {
        return jcrq;
    }

    public void setJcrq(String jcrq) {
        this.jcrq = jcrq;
    }

    public String getJcxm() {
        return jcxm;
    }

    public void setJcxm(String jcxm) {
        this.jcxm = jcxm;
    }

    public String getJcnr() {
        return jcnr;
    }

    public void setJcnr(String jcnr) {
        this.jcnr = jcnr;
    }

    public String getJcbz() {
        return jcbz;
    }

    public void setJcbz(String jcbz) {
        this.jcbz = jcbz;
    }

    public String getJcr() {
        return jcr;
    }

    public void setJcr(String jcr) {
        this.jcr = jcr;
    }

    public String getJcjg() {
        return jcjg;
    }

    public void setJcjg(String jcjg) {
        this.jcjg = jcjg;
    }
}
