package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 专用工器具及备品备件移交清单
 *         Created by Administrator on 2018/3/10.
 */

public class YiJiaoQingDanModel extends DataSupport {
    private int id;//主键id
    private String bzmc;//班站名称
    private String zybpmc;//	专用工器具/备品备件名称
    private String sl;//数量
    private String yjrq;//移交日期
    private String yjrqz;//	移交人签字
    private String jsrqz;//	接收人签字
    private String bz;//备注

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBzmc() {
        return bzmc;
    }

    public void setBzmc(String bzmc) {
        this.bzmc = bzmc;
    }

    public String getZybpmc() {
        return zybpmc;
    }

    public void setZybpmc(String zybpmc) {
        this.zybpmc = zybpmc;
    }

    public String getSl() {
        return sl;
    }

    public void setSl(String sl) {
        this.sl = sl;
    }

    public String getYjrq() {
        return yjrq;
    }

    public void setYjrq(String yjrq) {
        this.yjrq = yjrq;
    }

    public String getYjrqz() {
        return yjrqz;
    }

    public void setYjrqz(String yjrqz) {
        this.yjrqz = yjrqz;
    }

    public String getJsrqz() {
        return jsrqz;
    }

    public void setJsrqz(String jsrqz) {
        this.jsrqz = jsrqz;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }
}
