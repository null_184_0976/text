package com.mtjsoft.www.myapplication.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.BeiPinShiYongModel;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mtj 备品备件使用记录
 *         Created by Administrator on 2018/3/11.
 */

public class BeiPinShiYongActivity extends BaseActivity {

    private static final int CHOOSENAME = 1001;
    private static final int CHOOSESHU = 1002;
    private static final int CHOOSEBIANDIAN = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private int col = 11;
    private int row = 20;
    private List<BeiPinShiYongModel> beiPinShiYongModelList;
    private String ban = "___";

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setOnClickListener(this);
        setTopName(String.format(getString(R.string.beipin_sy_title), ban));
        addTableTitle();
        readData();
        addCenterView(row);
        setData();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.layout_beipin_shiyong_top, null);
        addTopLayoutView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(int row) {
        for (int j = 0; j < row; j++) {
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            for (int i = 0; i < col; i++) {
                LinearLayout.LayoutParams layoutParams = null;
                if (i == 1) {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,2);
                    drawEditText.setLayoutParams(layoutParams);
                    view.addView(drawEditText);
                } else if (i == 2 || i == 4) {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,1);
                    drawEditText.setLayoutParams(layoutParams);
                    view.addView(drawEditText);
                } else {
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,1);
                    drawTextView.setTag(i);
                    if (i == 0) {
                        drawTextView.setText((pLayout.getChildCount() + 1) + "");
                    }
                    drawTextView.setOnClickListener(this);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                }
            }
            pLayout.addView(view);
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        if (v.getId() == R.id.iv_add_row) {
            addCenterView(5);
            return;
        }
        if (v.getId() == R.id.tv_base_top_name) {
            Intent intent = new Intent(this, ChooseActivity.class);
            intent.putExtra("type", 1);
            startActivityForResult(intent, CHOOSEBIANDIAN);
            return;
        }
        Intent intent = null;
        int tag = (int) v.getTag();
        switch (tag) {
            case 3:
            case 5:
            case 8:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 4);
                startActivityForResult(intent, CHOOSESHU);
                break;
            case 6:
            case 9:
                chooseTime();
                break;
            case 7:
            case 10:
                intent = new Intent(this, LineLockActivity.class);
                intent.putExtra("isChoose", true);
                startActivityForResult(intent, CHOOSENAME);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSENAME:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            ban = name;
                            setTopName(String.format(getString(R.string.beipin_sy_title), ban));
                        }
                    }
                    break;
                case CHOOSESHU:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 选择日期
     */
    private void chooseTime() {
        //时间选择器
        TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                clickText.setText(TurnsUtils.convertToString(date,"yyyy-MM-dd"));
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(new boolean[]{true, true, true, false, false, false})
                .build();
        pvTime.show();
    }


    /**
     * 读取数据
     */
    private void readData() {
        beiPinShiYongModelList = DataSupport.findAll(BeiPinShiYongModel.class);
        if (beiPinShiYongModelList != null && beiPinShiYongModelList.size() > row) {
            row = beiPinShiYongModelList.size();
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        if (beiPinShiYongModelList != null && beiPinShiYongModelList.size() > 0) {
            for (int i = 0, plCh = beiPinShiYongModelList.size(); i < plCh; i++) {
                BeiPinShiYongModel model = beiPinShiYongModelList.get(i);
                ban = model.getBzmc();
                setTopName(String.format(getString(R.string.beipin_sy_title), ban));
                if (pLayout.getChildAt(i) instanceof LinearLayout) {
                    LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                    ((DrawEditText) linearLayout.getChildAt(1)).setText(model.getMc());
                    ((DrawEditText) linearLayout.getChildAt(2)).setText(model.getGgxh());
                    ((DrawTextView) linearLayout.getChildAt(3)).setText(model.getSl());
                    ((DrawEditText) linearLayout.getChildAt(4)).setText(model.getDw());
                    ((DrawTextView) linearLayout.getChildAt(5)).setText(model.getBcsl());
                    ((DrawTextView) linearLayout.getChildAt(6)).setText(model.getRksj());
                    ((DrawTextView) linearLayout.getChildAt(7)).setText(model.getZrr());
                    ((DrawTextView) linearLayout.getChildAt(8)).setText(model.getLysl());
                    ((DrawTextView) linearLayout.getChildAt(9)).setText(model.getLysj());
                    ((DrawTextView) linearLayout.getChildAt(10)).setText(model.getLyr());
                }
            }
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        DataSupport.deleteAll(BeiPinShiYongModel.class);
        List<BeiPinShiYongModel> modelList = new ArrayList<>();
        for (int i = 0, plCh = pLayout.getChildCount(); i < plCh; i++) {
            BeiPinShiYongModel model = new BeiPinShiYongModel();
            model.setBzmc(ban);
            if (pLayout.getChildAt(i) instanceof LinearLayout) {
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                DrawEditText drawTextView = (DrawEditText) linearLayout.getChildAt(1);
                if (!TextUtils.isEmpty(drawTextView.getText().toString())) {
                    model.setMc(drawTextView.getText().toString());
                    model.setGgxh(((DrawEditText) linearLayout.getChildAt(2)).getText().toString());
                    model.setSl(((DrawTextView) linearLayout.getChildAt(3)).getText().toString());
                    model.setDw(((DrawEditText) linearLayout.getChildAt(4)).getText().toString());
                    model.setBcsl(((DrawTextView) linearLayout.getChildAt(5)).getText().toString());
                    model.setRksj(((DrawTextView) linearLayout.getChildAt(6)).getText().toString());
                    model.setZrr(((DrawTextView) linearLayout.getChildAt(7)).getText().toString());
                    model.setLysl(((DrawTextView) linearLayout.getChildAt(8)).getText().toString());
                    model.setLysj(((DrawTextView) linearLayout.getChildAt(9)).getText().toString());
                    model.setLyr(((DrawTextView) linearLayout.getChildAt(10)).getText().toString());
                    modelList.add(model);
                }
            }
        }
        DataSupport.saveAll(modelList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
