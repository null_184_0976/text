package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 设备巡视记录
 *         Created by Administrator on 2018/3/14.
 */

public class YunWeiXunShiModel extends DataSupport {
    private int id;//主键id
    private String bdz;//变电站
    private String dydj;//电压等级
    private String xsrq;//巡视日期
    private String bdzlb;//	变电站类别
    private String xslx;//巡视类型
    private String tq;//天气
    private String qw;//气温（℃）
    private String xsbz;//巡视班组
    private String xsr;//巡视人
    private String sfsyxjyxs;//		是否使用巡检仪巡视
    private String xskssj;//	巡视开始时间
    private String xsjssj;//	巡视结束时间
    private String xsnr;//巡视内容
    private String xsjg;//巡视结果
    private String bz;//备注

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBdz() {
        return bdz;
    }

    public void setBdz(String bdz) {
        this.bdz = bdz;
    }

    public String getDydj() {
        return dydj;
    }

    public void setDydj(String dydj) {
        this.dydj = dydj;
    }

    public String getXsrq() {
        return xsrq;
    }

    public void setXsrq(String xsrq) {
        this.xsrq = xsrq;
    }

    public String getBdzlb() {
        return bdzlb;
    }

    public void setBdzlb(String bdzlb) {
        this.bdzlb = bdzlb;
    }

    public String getXslx() {
        return xslx;
    }

    public void setXslx(String xslx) {
        this.xslx = xslx;
    }

    public String getTq() {
        return tq;
    }

    public void setTq(String tq) {
        this.tq = tq;
    }

    public String getQw() {
        return qw;
    }

    public void setQw(String qw) {
        this.qw = qw;
    }

    public String getXsbz() {
        return xsbz;
    }

    public void setXsbz(String xsbz) {
        this.xsbz = xsbz;
    }

    public String getXsr() {
        return xsr;
    }

    public void setXsr(String xsr) {
        this.xsr = xsr;
    }

    public String getSfsyxjyxs() {
        return sfsyxjyxs;
    }

    public void setSfsyxjyxs(String sfsyxjyxs) {
        this.sfsyxjyxs = sfsyxjyxs;
    }

    public String getXskssj() {
        return xskssj;
    }

    public void setXskssj(String xskssj) {
        this.xskssj = xskssj;
    }

    public String getXsjssj() {
        return xsjssj;
    }

    public void setXsjssj(String xsjssj) {
        this.xsjssj = xsjssj;
    }

    public String getXsnr() {
        return xsnr;
    }

    public void setXsnr(String xsnr) {
        this.xsnr = xsnr;
    }

    public String getXsjg() {
        return xsjg;
    }

    public void setXsjg(String xsjg) {
        this.xsjg = xsjg;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }
}
