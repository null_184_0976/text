package com.mtjsoft.www.myapplication.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.FangXunJianChaModel;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mtj 防汛工作检查表
 *         Created by Administrator on 2018/3/11.
 */

public class FangXunJianChaActivity extends BaseActivity {

    private static final int CHOOSEName = 1002;
    private static final int CHOOSEBIANDIAN = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private int col = 0;
    private List<FangXunJianChaModel> fangXunJianChaModelList;
    private List<List<FangXunJianChaModel>> listRowsData = new ArrayList<>();
    private String zhan = "___";
    private DrawTextView riqiTextView;

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setVisibility(View.GONE);
        setTopName(String.format(getString(R.string.fangxunjiancha_title), zhan));
        addTableTitle();
        readData();
        addCenterView();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        removeTopLayoutViewAll();
        LinearLayout topRiQi = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < 2; i++) {
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            drawTextView.setLayoutParams(layoutParams);
            if (i == 0) {
                drawTextView.setText("检查日期");
            } else {
                drawTextView.setTag("chooseTime");
                drawTextView.setOnClickListener(this);
                riqiTextView = drawTextView;
            }
            topRiQi.addView(drawTextView);
        }
        addTopLayoutView(topRiQi);
        String[] titles = getResources().getStringArray(R.array.fangxun_jiancha);
        col = titles.length;
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < col; i++) {
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
            if (i == 0) {
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            } else if (i == 3) {
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 3);
            }
            drawTextView.setLayoutParams(layoutParams);
            drawTextView.setText(titles[i]);
            view.addView(drawTextView);
        }
        addTopLayoutView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView() {
        if (listRowsData != null && listRowsData.size() > 0) {
            for (int i = 0; i < listRowsData.size(); i++) {
                LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
                //添加序号
                DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                ViewGroup.LayoutParams layoutParams = null;
                drawTextView.setText((pLayout.getChildCount() + 1) + "");
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                drawTextView.setLayoutParams(layoutParams);
                view.addView(drawTextView);
                //添加检查项目
                DrawTextView typeTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                typeTextView.setLayoutParams(layoutParams);
                view.addView(typeTextView);
                //添加右边竖直排列
                LinearLayout shuView = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_vertical_linearlayout_f_f, null);
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 9);
                shuView.setLayoutParams(layoutParams);
                List<FangXunJianChaModel> rowList = listRowsData.get(i);
                for (int j = 0; j < rowList.size(); j++) {
                    FangXunJianChaModel model = rowList.get(j);
                    if (!TextUtils.isEmpty(model.getZm())) {
                        zhan = model.getZm();
                        setTopName(String.format(getString(R.string.fangxunjiancha_title), model.getZm()));
                    }
                    if (!TextUtils.isEmpty(model.getJcrq())) {
                        riqiTextView.setText(model.getJcrq());
                    }
                    typeTextView.setText(model.getJcxm());
                    LinearLayout hengView = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
                    //检查内容
                    DrawTextView dianTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                    dianTextView.setLayoutParams(layoutParams);
                    dianTextView.setText(model.getJcnr());
                    hengView.addView(dianTextView);
                    //检查标准
                    DrawTextView cuoshiTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 3);
                    cuoshiTextView.setText(model.getJcbz());
                    cuoshiTextView.setLayoutParams(layoutParams);
                    hengView.addView(cuoshiTextView);
                    //检查人
                    DrawTextView renTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                    renTextView.setText(model.getJcr());
                    renTextView.setLayoutParams(layoutParams);
                    renTextView.setOnClickListener(this);
                    renTextView.setTag("chooseName");
                    hengView.addView(renTextView);
                    //结果
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                    drawEditText.setText(model.getJcjg());
                    drawEditText.setLayoutParams(layoutParams);
                    hengView.addView(drawEditText);
                    //
                    shuView.addView(hengView);
                }
                view.addView(shuView);
                pLayout.addView(view);
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        if (v.getId() == R.id.tv_base_top_name) {
            Intent intent = new Intent(this, ChooseActivity.class);
            intent.putExtra("type", 1);
            startActivityForResult(intent, CHOOSEBIANDIAN);
            return;
        }
        if ("chooseTime".equals(v.getTag())) {
            chooseTime();
            return;
        }
        if ("chooseName".equals(v.getTag())) {
            Intent intent = new Intent(this, LineLockActivity.class);
            intent.putExtra("isChoose", true);
            startActivityForResult(intent, CHOOSEName);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            zhan = name;
                            setTopName(String.format(getString(R.string.fangxunjiancha_title), zhan));
                        }
                    }
                    break;
                case CHOOSEName:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 选择日期
     */
    private void chooseTime() {
        //时间选择器
        TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                riqiTextView.setText(TurnsUtils.convertToString(date,"yyyy-MM-dd"));
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(new boolean[]{true, true, true, false, false, false})
                .build();
        pvTime.show();
    }

    /**
     * 读取数据
     */
    private void readData() {
        String type = "0";
        fangXunJianChaModelList = DataSupport.findAll(FangXunJianChaModel.class);
        if (fangXunJianChaModelList != null && fangXunJianChaModelList.size() > 0) {
            for (int i = 0; i < fangXunJianChaModelList.size(); i++) {
                if (!type.equals(fangXunJianChaModelList.get(i).getJcxm())) {
                    type = fangXunJianChaModelList.get(i).getJcxm();
                    List<FangXunJianChaModel> rowList = DataSupport.where("jcxm = ?", type).find(FangXunJianChaModel.class);
                    listRowsData.add(rowList);
                }
            }
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        DataSupport.deleteAll(FangXunJianChaModel.class);
        List<FangXunJianChaModel> fangXunJianChaModelList = new ArrayList<>();
        for (int i = 0, plCh = pLayout.getChildCount(); i < plCh; i++) {
            LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
            LinearLayout verLayout = (LinearLayout) linearLayout.getChildAt(2);
            for (int j = 0; j < verLayout.getChildCount(); j++) {
                LinearLayout hengLayout = (LinearLayout) verLayout.getChildAt(j);
                FangXunJianChaModel model = new FangXunJianChaModel();
                model.setZm(zhan);
                model.setJcrq(riqiTextView.getText().toString());
                model.setJcxm(((DrawTextView) linearLayout.getChildAt(1)).getText().toString());
                model.setJcnr(((DrawTextView) hengLayout.getChildAt(0)).getText().toString());
                model.setJcbz(((DrawTextView) hengLayout.getChildAt(1)).getText().toString());
                model.setJcr(((DrawTextView) hengLayout.getChildAt(2)).getText().toString());
                model.setJcjg(((DrawEditText) hengLayout.getChildAt(3)).getText().toString());
                fangXunJianChaModelList.add(model);
            }
        }
        DataSupport.saveAll(fangXunJianChaModelList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
