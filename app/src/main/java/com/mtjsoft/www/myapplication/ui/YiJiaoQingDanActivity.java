package com.mtjsoft.www.myapplication.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.OptionsPickerView;
import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.YiJiaoQingDanModel;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author mtj 专用工器具及备品备件移交清单
 *         Created by Administrator on 2018/3/9.
 */

public class YiJiaoQingDanActivity extends BaseActivity {

    private static final int CHOOSESHU = 1001;
    private static final int CHOOSENAME = 1002;
    private static final int CHOOSEGONGJU = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private int col = 0;
    private int row = 20;
    private List<YiJiaoQingDanModel> yiJiaoQingDanModelList;
    private String ban = "___";

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setOnClickListener(this);
        setTopName(String.format(getString(R.string.yijiao_title),ban));
        addTableTitle();
        readData();
        addCenterView(row);
        setData();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        String[] titles = getResources().getStringArray(R.array.yijiao_qingdan);
        col = titles.length;
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < col; i++) {
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
            if (i == 0 || i == 2 || i == col - 1) {
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            } else if (i == 1) {
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 3);
            }
            drawTextView.setLayoutParams(layoutParams);
            drawTextView.setText(titles[i]);
            view.addView(drawTextView);
        }
        addTopLayoutView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(int row) {
        for (int j = 0; j < row; j++) {
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            for (int i = 0; i < col; i++) {
                LinearLayout.LayoutParams  layoutParams = null;
                if (i == 0) {
                    layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText((pLayout.getChildCount() + 1) + "");
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                } else if (i == 1) {
                    layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 3);
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setTag(i);
                    drawTextView.setOnClickListener(this);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                } else if (i == 2) {
                    layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setTag(i);
                    drawTextView.setOnClickListener(this);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                } else if (i == col - 1) {
                    layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                    DrawEditText drawDrawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    drawDrawEditText.setLayoutParams(layoutParams);
                    view.addView(drawDrawEditText);
                } else {
                    layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setTag(i);
                    drawTextView.setOnClickListener(this);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                }
            }
            pLayout.addView(view);
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        if (v.getId() == R.id.iv_add_row) {
            addCenterView(5);
            return;
        }
        if (v.getId() == R.id.tv_base_top_name) {
            chooseOptions();
            return;
        }
        Intent intent = null;
        int tag = (int) v.getTag();
        switch (tag) {
            case 1:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 3);
                startActivityForResult(intent, CHOOSEGONGJU);
                break;
            case 2:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 4);
                startActivityForResult(intent, CHOOSESHU);
                break;
            case 3:
                chooseTime();
                break;
            case 4:
            case 5:
                intent = new Intent(this, LineLockActivity.class);
                intent.putExtra("isChoose", true);
                startActivityForResult(intent, CHOOSENAME);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSENAME:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                case CHOOSEGONGJU:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                case CHOOSESHU:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 选择日期
     */
    private void chooseTime() {
        //时间选择器
        TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                clickText.setText(TurnsUtils.convertToString(date,"yyyy-MM-dd"));
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(new boolean[]{true, true, true, false, false, false})
                .build();
        pvTime.show();
    }

    /**
     * 班名选择
     */
    private void chooseOptions() {
        int option1 = 0;
        final List<String> options1Items = new ArrayList<>();
        String[] bans = getResources().getStringArray(R.array.bans);
        options1Items.addAll(Arrays.asList(bans));
        OptionsPickerView pvOptions = new OptionsPickerView.Builder(this, new OptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int options2, int options3, View v) {
                //返回的分别是三个级别的选中位置
                ban = options1Items.get(options1);
                setTopName(String.format(getString(R.string.yijiao_title),ban));
            }
        })
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(false)
                //设置选择的三级单位
                .setLabels(getString(R.string.ban), "", "")
                //循环与否
                .setCyclic(false, false, false)
                //设置默认选中项
                .setSelectOptions(option1, 0, 0)
                //点击外部dismiss default true
                .setOutSideCancelable(true)
                //是否显示为对话框样式
                .isDialog(true)
                .build();
        pvOptions.setNPicker(options1Items, null, null);
        pvOptions.show();
    }

    /**
     * 读取数据
     */
    private void readData() {
        yiJiaoQingDanModelList = DataSupport.findAll(YiJiaoQingDanModel.class);
        if (yiJiaoQingDanModelList != null && yiJiaoQingDanModelList.size() > row) {
            row = yiJiaoQingDanModelList.size();
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        if (yiJiaoQingDanModelList != null && yiJiaoQingDanModelList.size() > 0) {
            for (int i = 0, plCh = yiJiaoQingDanModelList.size(); i < plCh; i++) {
                YiJiaoQingDanModel model = yiJiaoQingDanModelList.get(i);
                ban = model.getBzmc();
                setTopName(String.format(getString(R.string.yijiao_title),ban));
                if (pLayout.getChildAt(i) instanceof LinearLayout) {
                    LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                    ((DrawTextView) linearLayout.getChildAt(1)).setText(model.getZybpmc());
                    ((DrawTextView) linearLayout.getChildAt(2)).setText(model.getSl());
                    ((DrawTextView) linearLayout.getChildAt(3)).setText(model.getYjrq());
                    ((DrawTextView) linearLayout.getChildAt(4)).setText(model.getYjrqz());
                    ((DrawTextView) linearLayout.getChildAt(5)).setText(model.getJsrqz());
                    ((DrawEditText) linearLayout.getChildAt(6)).setText(model.getBz());
                }
            }
            setTopName(String.format(getString(R.string.yijiao_title),ban));
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        DataSupport.deleteAll(YiJiaoQingDanModel.class);
        List<YiJiaoQingDanModel> yiJiaoQingDanModelList = new ArrayList<>();
        for (int i = 0, plCh = pLayout.getChildCount(); i < plCh; i++) {
            YiJiaoQingDanModel model = new YiJiaoQingDanModel();
            model.setBzmc(ban);
            if (pLayout.getChildAt(i) instanceof LinearLayout) {
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                DrawTextView drawTextView = (DrawTextView) linearLayout.getChildAt(1);
                if (!TextUtils.isEmpty(drawTextView.getText().toString())) {
                    model.setZybpmc(drawTextView.getText().toString());
                    model.setSl(((DrawTextView) linearLayout.getChildAt(2)).getText().toString());
                    model.setYjrq(((DrawTextView) linearLayout.getChildAt(3)).getText().toString());
                    model.setYjrqz(((DrawTextView) linearLayout.getChildAt(4)).getText().toString());
                    model.setJsrqz(((DrawTextView) linearLayout.getChildAt(5)).getText().toString());
                    model.setBz(((DrawEditText) linearLayout.getChildAt(6)).getText().toString());
                    yiJiaoQingDanModelList.add(model);
                }
            }
        }
        DataSupport.saveAll(yiJiaoQingDanModelList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
