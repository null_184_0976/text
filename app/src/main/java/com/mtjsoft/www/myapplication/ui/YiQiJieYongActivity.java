package com.mtjsoft.www.myapplication.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.YiQiJieYongModel;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mtj 仪器仪表借用记录
 *         Created by Administrator on 2018/3/11.
 */

public class YiQiJieYongActivity extends BaseActivity {

    private static final int CHOOSEName = 1001;
    private static final int CHOOSESHU = 1002;
    private static final int CHOOSEBIANDIAN = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private int col = 0;
    private int row = 20;
    private List<YiQiJieYongModel> yiQiJieYongModelList;
    private String zhan = "___";

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setOnClickListener(this);
        setTopName(String.format(getString(R.string.yiqijieyong_title), zhan));
        addTableTitle();
        readData();
        addCenterView(row);
        setData();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        removeTopLayoutViewAll();
        String[] titles = getResources().getStringArray(R.array.yiqi_jieyong);
        col = titles.length;
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < col; i++) {
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            if (i == 1 || i == 2 || i == 3) {
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
            }
            drawTextView.setLayoutParams(layoutParams);
            drawTextView.setText(titles[i]);
            view.addView(drawTextView);
        }
        addTopLayoutView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(int row) {
        for (int j = 0; j < row; j++) {
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            for (int i = 0; i < col; i++) {
                if (i == 1 || i == 2 || i == 3) {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                    drawEditText.setLayoutParams(layoutParams);
                    view.addView(drawEditText);
                } else if (i == 5) {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                    drawEditText.setLayoutParams(layoutParams);
                    view.addView(drawEditText);
                } else {
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                    drawTextView.setTag(i);
                    if (i == 0) {
                        drawTextView.setText((pLayout.getChildCount() + 1) + "");
                    }
                    drawTextView.setOnClickListener(this);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                }
            }
            pLayout.addView(view);
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        if (v.getId() == R.id.iv_add_row) {
            addCenterView(5);
            return;
        }
        if (v.getId() == R.id.tv_base_top_name) {
            Intent intent = new Intent(this, ChooseActivity.class);
            intent.putExtra("type", 1);
            startActivityForResult(intent, CHOOSEBIANDIAN);
            return;
        }
        Intent intent = null;
        int tag = (int) v.getTag();
        switch (tag) {
            case 4:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 4);
                startActivityForResult(intent, CHOOSESHU);
                break;
            case 6:
                intent = new Intent(this, LineLockActivity.class);
                intent.putExtra("isChoose", true);
                startActivityForResult(intent, CHOOSEName);
                break;
            case 7:
            case 8:
                chooseTime();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            zhan = name;
                            clickText.setText(String.format(getString(R.string.yiqijieyong_title), zhan));
                        }
                    }
                    break;
                case CHOOSESHU:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name);
                        }
                    }
                    break;
                case CHOOSEName:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 选择日期
     */
    private void chooseTime() {
        //时间选择器
        TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                clickText.setText(TurnsUtils.convertToString(date,"yyyy-MM-dd"));
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(new boolean[]{true, true, true, false, false, false})
                .build();
        pvTime.show();
    }

    /**
     * 读取数据
     */
    private void readData() {
        yiQiJieYongModelList = DataSupport.findAll(YiQiJieYongModel.class);
        if (yiQiJieYongModelList != null && yiQiJieYongModelList.size() > row) {
            row = yiQiJieYongModelList.size();
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        if (yiQiJieYongModelList != null && yiQiJieYongModelList.size() > 0) {
            for (int i = 0, plCh = yiQiJieYongModelList.size(); i < plCh; i++) {
                YiQiJieYongModel model = yiQiJieYongModelList.get(i);
                zhan = model.getBzmc();
                setTopName(String.format(getString(R.string.yiqijieyong_title), zhan));
                if (pLayout.getChildAt(i) instanceof LinearLayout) {
                    LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                    ((DrawEditText) linearLayout.getChildAt(1)).setText(model.getMc());
                    ((DrawEditText) linearLayout.getChildAt(2)).setText(model.getGgxh());
                    ((DrawEditText) linearLayout.getChildAt(3)).setText(model.getYt());
                    ((DrawTextView) linearLayout.getChildAt(4)).setText(model.getSl());
                    ((DrawEditText) linearLayout.getChildAt(5)).setText(model.getDw());
                    ((DrawTextView) linearLayout.getChildAt(6)).setText(model.getLyr());
                    ((DrawTextView) linearLayout.getChildAt(7)).setText(model.getLyrq());
                    ((DrawTextView) linearLayout.getChildAt(8)).setText(model.getGhrq());
                }
            }
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        DataSupport.deleteAll(YiQiJieYongModel.class);
        List<YiQiJieYongModel> yiQiJieYongModelList = new ArrayList<>();
        for (int i = 0, plCh = pLayout.getChildCount(); i < plCh; i++) {
            YiQiJieYongModel model = new YiQiJieYongModel();
            model.setBzmc(zhan);
            if (pLayout.getChildAt(i) instanceof LinearLayout) {
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                DrawEditText drawTextView = (DrawEditText) linearLayout.getChildAt(1);
                if (!TextUtils.isEmpty(drawTextView.getText().toString())) {
                    model.setMc(drawTextView.getText().toString());
                    model.setGgxh(((DrawEditText) linearLayout.getChildAt(2)).getText().toString());
                    model.setYt(((DrawEditText) linearLayout.getChildAt(3)).getText().toString());
                    model.setSl(((DrawTextView) linearLayout.getChildAt(4)).getText().toString());
                    model.setDw(((DrawEditText) linearLayout.getChildAt(5)).getText().toString());
                    model.setLyr(((DrawTextView) linearLayout.getChildAt(6)).getText().toString());
                    model.setLyrq(((DrawTextView) linearLayout.getChildAt(7)).getText().toString());
                    model.setGhrq(((DrawTextView) linearLayout.getChildAt(8)).getText().toString());
                    yiQiJieYongModelList.add(model);
                }
            }
        }
        DataSupport.saveAll(yiQiJieYongModelList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
