package com.mtjsoft.www.myapplication.ui;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.model.WeiXianDianModel;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mtj 巡视危险点分析与预防控制措施
 *         Created by Administrator on 2018/3/11.
 */

public class WeiXianDianActivity extends BaseActivity {

    private ImageView addImageView;
    private LinearLayout pLayout;
    private int col = 0;
    private List<WeiXianDianModel> listSize;
    private List<List<WeiXianDianModel>> listRowDatas = new ArrayList<>();

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setVisibility(View.GONE);
        setTopName("巡视危险点分析与预防控制措施");
        addTableTitle();
        readData();
        addCenterView();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        removeTopLayoutViewAll();
        //标题
        String[] titles = getResources().getStringArray(R.array.weixiandian);
        col = titles.length;
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        LinearLayout.LayoutParams  layoutParams = null;
        for (int i = 0; i < col; i++) {
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            if (i == 0) {
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            } else if (i == col - 1) {
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 5);
            } else {
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
            }
            drawTextView.setLayoutParams(layoutParams);
            drawTextView.setText(titles[i]);
            view.addView(drawTextView);
        }
        addTopLayoutView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView() {
        if (listRowDatas != null && listRowDatas.size() > 0) {
            for (int i = 0; i < listRowDatas.size(); i++) {
                LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
                //添加序号
                DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                ViewGroup.LayoutParams layoutParams = null;
                drawTextView.setText((pLayout.getChildCount() + 1) + "");
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                drawTextView.setLayoutParams(layoutParams);
                view.addView(drawTextView);
                //添加防范类型
                DrawTextView typeTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                typeTextView.setLayoutParams(layoutParams);
                view.addView(typeTextView);
                //添加右边竖直排列
                LinearLayout shuView = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_vertical_linearlayout_f_f, null);
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 7);
                shuView.setLayoutParams(layoutParams);
                List<WeiXianDianModel> rowList = listRowDatas.get(i);
                for (int j = 0; j < rowList.size(); j++) {
                    WeiXianDianModel model = rowList.get(j);
                    typeTextView.setText(model.getFflx());
                    LinearLayout hengView = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
                    //危险点
                    DrawTextView dianTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                    dianTextView.setLayoutParams(layoutParams);
                    dianTextView.setText(model.getWxd());
                    hengView.addView(dianTextView);
                    //措施
                    DrawTextView cuoshiTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 5);
                    cuoshiTextView.setText(model.getYfkzcs());
                    cuoshiTextView.setLayoutParams(layoutParams);
                    hengView.addView(cuoshiTextView);
                    shuView.addView(hengView);
                }
                view.addView(shuView);
                pLayout.addView(view);
            }
        }
    }

    @Override
    public void onClick(View v) {
    }

    /**
     * 读取数据
     */
    private void readData() {
        String type = "0";
        listSize = DataSupport.findAll(WeiXianDianModel.class);
        if (listSize != null && listSize.size() > 0) {
            for (int i = 0; i < listSize.size(); i++) {
                if (!type.equals(listSize.get(i).getFflx())) {
                    type = listSize.get(i).getFflx();
                    List<WeiXianDianModel> rowList = DataSupport.where("fflx = ?", type).find(WeiXianDianModel.class);
                    listRowDatas.add(rowList);
                }
            }
        }
    }
}
