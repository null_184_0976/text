package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 变电站现场运行规程编制（修订）审批表
 *         Created by Administrator on 2018/3/14.
 */

public class GuiChengShenPiModel extends DataSupport {
    private int id;//主键id
    private String bh;//编号
    private String gcmc;//规程名称
    private String xbyy;//修编原因
    private String xbnr;//修编内容（修编范围及主要条款）
    private String bxry;//编写人员（签字）
    private String bxfzr;//	编写负责人
    private String bxqzrq;//	编写签字日期
    private String hsry;//会审人员（签字）
    private String hsqzrq;//	会审签字日期
    private String azbyj;//	安质部意见
    private String azshr;//	安质审核人
    private String azqzrq;//	安质签字日期
    private String tkzxyj;//	调控中心意见
    private String tkshr;//	调控审核人
    private String tkqzrq;//	调控签字日期
    private String yjbyj;//	运检部意见
    private String yjbshr;//	运检部审核人
    private String yjqzrq;//	运检签字日期
    private String fgldyj;//	分管领导意见
    private String fgqfr;//	分管签发人
    private String fgqzrq;//	分管签字日期

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBh() {
        return bh;
    }

    public void setBh(String bh) {
        this.bh = bh;
    }

    public String getGcmc() {
        return gcmc;
    }

    public void setGcmc(String gcmc) {
        this.gcmc = gcmc;
    }

    public String getXbyy() {
        return xbyy;
    }

    public void setXbyy(String xbyy) {
        this.xbyy = xbyy;
    }

    public String getXbnr() {
        return xbnr;
    }

    public void setXbnr(String xbnr) {
        this.xbnr = xbnr;
    }

    public String getBxry() {
        return bxry;
    }

    public void setBxry(String bxry) {
        this.bxry = bxry;
    }

    public String getBxfzr() {
        return bxfzr;
    }

    public void setBxfzr(String bxfzr) {
        this.bxfzr = bxfzr;
    }

    public String getBxqzrq() {
        return bxqzrq;
    }

    public void setBxqzrq(String bxqzrq) {
        this.bxqzrq = bxqzrq;
    }

    public String getHsry() {
        return hsry;
    }

    public void setHsry(String hsry) {
        this.hsry = hsry;
    }

    public String getHsqzrq() {
        return hsqzrq;
    }

    public void setHsqzrq(String hsqzrq) {
        this.hsqzrq = hsqzrq;
    }

    public String getAzbyj() {
        return azbyj;
    }

    public void setAzbyj(String azbyj) {
        this.azbyj = azbyj;
    }

    public String getAzshr() {
        return azshr;
    }

    public void setAzshr(String azshr) {
        this.azshr = azshr;
    }

    public String getAzqzrq() {
        return azqzrq;
    }

    public void setAzqzrq(String azqzrq) {
        this.azqzrq = azqzrq;
    }

    public String getTkzxyj() {
        return tkzxyj;
    }

    public void setTkzxyj(String tkzxyj) {
        this.tkzxyj = tkzxyj;
    }

    public String getTkshr() {
        return tkshr;
    }

    public void setTkshr(String tkshr) {
        this.tkshr = tkshr;
    }

    public String getTkqzrq() {
        return tkqzrq;
    }

    public void setTkqzrq(String tkqzrq) {
        this.tkqzrq = tkqzrq;
    }

    public String getYjbyj() {
        return yjbyj;
    }

    public void setYjbyj(String yjbyj) {
        this.yjbyj = yjbyj;
    }

    public String getYjbshr() {
        return yjbshr;
    }

    public void setYjbshr(String yjbshr) {
        this.yjbshr = yjbshr;
    }

    public String getYjqzrq() {
        return yjqzrq;
    }

    public void setYjqzrq(String yjqzrq) {
        this.yjqzrq = yjqzrq;
    }

    public String getFgldyj() {
        return fgldyj;
    }

    public void setFgldyj(String fgldyj) {
        this.fgldyj = fgldyj;
    }

    public String getFgqfr() {
        return fgqfr;
    }

    public void setFgqfr(String fgqfr) {
        this.fgqfr = fgqfr;
    }

    public String getFgqzrq() {
        return fgqzrq;
    }

    public void setFgqzrq(String fgqzrq) {
        this.fgqzrq = fgqzrq;
    }
}
