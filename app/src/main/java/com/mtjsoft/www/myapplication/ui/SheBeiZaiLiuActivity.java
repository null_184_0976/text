package com.mtjsoft.www.myapplication.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.model.SheBeiZaiLiuModel;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mtj 变电站设备载流参数表
 *         Created by Administrator on 2018/3/9.
 */

public class SheBeiZaiLiuActivity extends BaseActivity {

    private static final int CHOOSEName = 1001;
    private static final int CHOOSEZM = 1002;
    private static final int CHOOSEBIANDIAN = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private int col = 0;
    private int row = 20;
    private List<SheBeiZaiLiuModel> sheBeiZaiLiuModelList;
    private String zhan = "___";
    private String riqi = "";
    private DrawTextView riqiTextView;

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setOnClickListener(this);
        setTopName(String.format(getString(R.string.shebei_zailiu_title), zhan));
        addTableTitle();
        readData();
        addCenterView(row);
        setData();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        removeTopLayoutViewAll();
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.layout_shebei_zailiu_top, null);
        riqiTextView = view.findViewById(R.id.tv_zailiu_riqi);
        riqiTextView.setOnClickListener(this);
        addTopLayoutView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(int row) {
        for (int j = 0; j < row; j++) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            for (int i = 0; i < 15; i++) {
                DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                drawEditText.setLayoutParams(layoutParams);
                view.addView(drawEditText);
            }
            pLayout.addView(view);
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        if (v.getId() == R.id.iv_add_row) {
            addCenterView(5);
            return;
        }
        if (v.getId() == R.id.tv_base_top_name) {
            Intent intent = new Intent(this, ChooseActivity.class);
            intent.putExtra("type", 1);
            startActivityForResult(intent, CHOOSEBIANDIAN);
            return;
        }
        if (v.getId() == R.id.tv_zailiu_riqi) {
            chooseTime();
            return;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            zhan = name;
                            clickText.setText(String.format(getString(R.string.shebei_zailiu_title), zhan));
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 选择日期
     */
    private void chooseTime() {
        //时间选择器
        TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                riqi = TurnsUtils.convertToString(date,"yyyy-MM-dd");
                riqiTextView.setText("日期:" + riqi);
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(new boolean[]{true, true, true, false, false, false})
                .build();
        pvTime.show();
    }

    /**
     * 读取数据
     */
    private void readData() {
        sheBeiZaiLiuModelList = DataSupport.findAll(SheBeiZaiLiuModel.class);
        if (sheBeiZaiLiuModelList != null && sheBeiZaiLiuModelList.size() > row) {
            row = sheBeiZaiLiuModelList.size();
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        if (sheBeiZaiLiuModelList != null && sheBeiZaiLiuModelList.size() > 0) {
            for (int i = 0, plCh = sheBeiZaiLiuModelList.size(); i < plCh; i++) {
                SheBeiZaiLiuModel model = sheBeiZaiLiuModelList.get(i);
                zhan = model.getBdzmc();
                setTopName(String.format(getString(R.string.shebei_zailiu_title), zhan));
                riqi = model.getRq();
                riqiTextView.setText("日期:" + riqi);
                if (pLayout.getChildAt(i) instanceof LinearLayout) {
                    LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                    ((DrawEditText) linearLayout.getChildAt(0)).setText(model.getSbmc());
                    ((DrawEditText) linearLayout.getChildAt(1)).setText(model.getKgbh());
                    ((DrawEditText) linearLayout.getChildAt(2)).setText(model.getDlq());
                    ((DrawEditText) linearLayout.getChildAt(3)).setText(model.getGlkg());
                    ((DrawEditText) linearLayout.getChildAt(4)).setText(model.getEd());
                    ((DrawEditText) linearLayout.getChildAt(5)).setText(model.getBhCT());
                    ((DrawEditText) linearLayout.getChildAt(6)).setText(model.getJcCT());
                    ((DrawEditText) linearLayout.getChildAt(7)).setText(model.getZbq());
                    ((DrawEditText) linearLayout.getChildAt(8)).setText(model.getNdyxggxh());
                    ((DrawEditText) linearLayout.getChildAt(9)).setText(model.getNdyx());
                    ((DrawEditText) linearLayout.getChildAt(10)).setText(model.getSdxlzlggxh());
                    ((DrawEditText) linearLayout.getChildAt(11)).setText(model.getXl());
                    ((DrawEditText) linearLayout.getChildAt(12)).setText(model.getBh());
                    ((DrawEditText) linearLayout.getChildAt(13)).setText(model.getZxzlyj());
                    ((DrawEditText) linearLayout.getChildAt(14)).setText(model.getZdyxdl());
                }
            }
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        DataSupport.deleteAll(SheBeiZaiLiuModel.class);
        List<SheBeiZaiLiuModel> sheBeiZaiLiuModelList = new ArrayList<>();
        for (int i = 0, plCh = pLayout.getChildCount(); i < plCh; i++) {
            SheBeiZaiLiuModel model = new SheBeiZaiLiuModel();
            model.setBdzmc(zhan);
            model.setRq(riqi);
            if (pLayout.getChildAt(i) instanceof LinearLayout) {
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                DrawEditText drawEditText = (DrawEditText) linearLayout.getChildAt(0);
                if (!TextUtils.isEmpty(drawEditText.getText().toString())) {
                    model.setSbmc(drawEditText.getText().toString());
                    model.setKgbh(((DrawEditText) linearLayout.getChildAt(1)).getText().toString());
                    model.setDlq(((DrawEditText) linearLayout.getChildAt(2)).getText().toString());
                    model.setGlkg(((DrawEditText) linearLayout.getChildAt(3)).getText().toString());
                    model.setEd(((DrawEditText) linearLayout.getChildAt(4)).getText().toString());
                    model.setBhCT(((DrawEditText) linearLayout.getChildAt(5)).getText().toString());
                    model.setJcCT(((DrawEditText) linearLayout.getChildAt(6)).getText().toString());
                    model.setZbq(((DrawEditText) linearLayout.getChildAt(7)).getText().toString());
                    model.setNdyxggxh(((DrawEditText) linearLayout.getChildAt(8)).getText().toString());
                    model.setNdyx(((DrawEditText) linearLayout.getChildAt(9)).getText().toString());
                    model.setSdxlzlggxh(((DrawEditText) linearLayout.getChildAt(10)).getText().toString());
                    model.setXl(((DrawEditText) linearLayout.getChildAt(11)).getText().toString());
                    model.setBh(((DrawEditText) linearLayout.getChildAt(12)).getText().toString());
                    model.setZxzlyj(((DrawEditText) linearLayout.getChildAt(13)).getText().toString());
                    model.setZdyxdl(((DrawEditText) linearLayout.getChildAt(14)).getText().toString());
                    sheBeiZaiLiuModelList.add(model);
                }
            }
        }
        DataSupport.saveAll(sheBeiZaiLiuModelList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
