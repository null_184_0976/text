package com.mtjsoft.www.myapplication.base;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.utils.HHDensityUtils;
import com.mtjsoft.www.myapplication.utils.HHScreenUtils;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

/**
 * @author mtj
 * Created by Administrator on 2018/3/6.
 */

public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener{
    private DrawTextView topMsgTextView;
    private DrawTextView topNameTextView;
    private DrawTextView bottomMsgTextView;
    private LinearLayout topLinearLayout;
    private LinearLayout bottomLinearLayout;
    private ScrollView centerScrollView;
    private int width = 0;
    private int height = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_activity);
        topMsgTextView = findViewById(R.id.tv_base_top_msg);
        topNameTextView = findViewById(R.id.tv_base_top_name);
        topLinearLayout = findViewById(R.id.ll_base_top);
        bottomLinearLayout = findViewById(R.id.ll_base_bottom);
        bottomMsgTextView = findViewById(R.id.tv_base_bottom_msg);
        centerScrollView = findViewById(R.id.scl_base_center);
        centerScrollView.addView(initViewResId());
        //顶底提示框
        topMsgTextView.setPaintColorById(R.color.main_base_color);
        bottomMsgTextView.setPaintColorById(R.color.main_base_color);
        topMsgTextView.setVisibility(View.GONE);
        bottomMsgTextView.setVisibility(View.GONE);
        //
        topNameTextView.setOnClickListener(this);
        //
        width = HHScreenUtils.getScreenWidth(getBaseContext()) - HHDensityUtils.dip2px(getBaseContext(), 20);
        height = HHScreenUtils.getScreenWidth(getBaseContext()) - HHDensityUtils.dip2px(getBaseContext(), 45);
        initView();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        width = HHScreenUtils.getScreenWidth(getBaseContext()) - HHDensityUtils.dip2px(getBaseContext(), 20);
        height = HHScreenUtils.getScreenWidth(getBaseContext()) - HHDensityUtils.dip2px(getBaseContext(), 20);
    }

    protected abstract View initViewResId();

    protected abstract void initView();

    public int getScWidth(){
        return width;
    }

    public int getScHeight(){
        return height;
    }

    public void setTopMsg(String msg){
        topMsgTextView.setText(msg);
    }

    public void setTopName(String name){
        topNameTextView.setText(name);
    }

    public void setBottomMsg(String msg){
        bottomMsgTextView.setText(msg);
    }

    public void setTopMsgViewHide(boolean isShow){
        if (isShow) {
            topMsgTextView.setVisibility(View.VISIBLE);
        } else {
            topMsgTextView.setVisibility(View.GONE);
        }
    }

    public void setTopNameViewHide(boolean isShow){
        if (isShow) {
            topNameTextView.setVisibility(View.VISIBLE);
        } else {
            topNameTextView.setVisibility(View.GONE);
        }
    }

    public void setBottomMsgViewHide(boolean isShow){
        if (isShow) {
            bottomMsgTextView.setVisibility(View.VISIBLE);
        } else {
            bottomMsgTextView.setVisibility(View.GONE);
        }
    }

    public void addTopLayoutView(View view){
        topLinearLayout.addView(view);
    }

    public void removeTopLayoutViewAll(){
        topLinearLayout.removeAllViews();
    }

    public void addBottomLayoutView(View view){
        bottomLinearLayout.addView(view);
    }

    public void removeBottomLayoutViewAll(){
        bottomLinearLayout.removeAllViews();
    }

    public LinearLayout getTopLinearLayout(){
        return topLinearLayout;
    }

    public LinearLayout getBottomLinearLayout(){
        return bottomLinearLayout;
    }

    public DrawTextView getTopNameTextView(){
        return topNameTextView;
    }
}
