package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 站防汛物资台账
 *         Created by Administrator on 2018/3/10.
 */

public class FangXunTaiZhangModel extends DataSupport {
    private int id;//主键id
    private String zm;//站名
    private String wpmc;//		物品名称
    private String cfsl;//		存放数量
    private String cfwz;//		存放位置
    private String fzr;//	负责人
    private String bz;//备注

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getZm() {
        return zm;
    }

    public void setZm(String zm) {
        this.zm = zm;
    }

    public String getWpmc() {
        return wpmc;
    }

    public void setWpmc(String wpmc) {
        this.wpmc = wpmc;
    }

    public String getCfsl() {
        return cfsl;
    }

    public void setCfsl(String cfsl) {
        this.cfsl = cfsl;
    }

    public String getCfwz() {
        return cfwz;
    }

    public void setCfwz(String cfwz) {
        this.cfwz = cfwz;
    }

    public String getFzr() {
        return fzr;
    }

    public void setFzr(String fzr) {
        this.fzr = fzr;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }
}
