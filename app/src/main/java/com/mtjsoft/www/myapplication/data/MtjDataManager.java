package com.mtjsoft.www.myapplication.data;

import android.util.Log;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.mtjsoft.www.myapplication.imp.HHCallBackImp;

/**
 * 接口管理
 * Created by Administrator on 2018年3月9日
 */

public class MtjDataManager {

    private static String IP = "http://10.0.0.6/";
    private static String tag = "mtj";
    private static HHCallBackImp hhCallBackImp;

    /**
     * 回调结果
     */
    private static class stringCallback extends StringCallback {

        @Override
        public void onSuccess(Response<String> response) {
            if (hhCallBackImp != null) {
                hhCallBackImp.onSuccess(response.body().toString());
                Log.i(tag, "result== " + response.body().toString());
            }
        }

        @Override
        public void onError(Response<String> response) {
            super.onError(response);
            if (hhCallBackImp != null) {
                hhCallBackImp.onError(null);
                Log.i(tag, "result== ");
            }
        }
    }

    /**
     * 蓄电池
     *
     * @param xdcZ        蓄电池基本表数据
     * @param xdcDtList   蓄电池电压值列表
     * @param callBackImp 回调
     */
    public static void tbXdc(String xdcZ, String xdcDtList, final HHCallBackImp callBackImp) {
        hhCallBackImp = callBackImp;
        OkGo.<String>post(IP + "inspection/xdc_z/data_up.do")
                .params("xdcZ", xdcZ)
                .params("xdcDtList", xdcDtList)
                .execute(new stringCallback());
    }

    /**
     * 运维计划表
     *
     * @param ywNjh
     * @param ywYjh
     * @param ywZjh
     * @param callBackImp
     */
    public static void tbYwJh(String ywNjh, String ywYjh, String ywZjh, final HHCallBackImp callBackImp) {
        hhCallBackImp = callBackImp;
        OkGo.<String>post(IP + "inspection/yw_njh/data_up.do")
                .params("ywNjh", ywNjh)
                .params("ywYjh", ywYjh)
                .params("ywZjh", ywZjh)
                .execute(new stringCallback());
    }

    /**
     * 专用工器具及备品备件移交清单
     *
     * @param yjqd
     * @param callBackImp
     */
    public static void tbYjQd(String yjqd, final HHCallBackImp callBackImp) {
        hhCallBackImp = callBackImp;
        OkGo.<String>post(IP + "inspection/yjqd/data_up.do")
                .params("yjqd", yjqd)
                .execute(new stringCallback());
    }

    /**
     * 防误装置
     *
     * @param fwzztz
     * @param fwzzjcjl
     * @param callBackImp
     */
    public static void tbFwZZ(String fwzztz, String fwzzjcjl, final HHCallBackImp callBackImp) {
        hhCallBackImp = callBackImp;
        OkGo.<String>post(IP + "inspection/fwzztz/data_up.do")
                .params("fwzztz", fwzztz)
                .params("fwzzjcjl", fwzzjcjl)
                .execute(new stringCallback());
    }

    /**
     * 消防设施（台账记录
     *
     * @param fwzztz
     * @param callBackImp
     */
    public static void tbXiaoFangTZ(String fwzztz, final HHCallBackImp callBackImp) {
        hhCallBackImp = callBackImp;
        OkGo.<String>post(IP + "inspection/fwzztz/data_up.do")
                .params("fwzztz", fwzztz)
                .execute(new stringCallback());
    }

    /**
     * 防汛物资台账
     * @param fwzztz
     * @param callBackImp
     */
    public static void tbFangXunTZ(String fwzztz, final HHCallBackImp callBackImp) {
        hhCallBackImp = callBackImp;
        OkGo.<String>post(IP + "inspection/fwzztz/data_up.do")
                .params("fwzztz", fwzztz)
                .execute(new stringCallback());
    }

    /**
     * 12、周边隐患（周边隐患台账）
     * @param fwzztz
     * @param callBackImp
     */
    public static void tbZbYH(String fwzztz, final HHCallBackImp callBackImp) {
        hhCallBackImp = callBackImp;
        OkGo.<String>post(IP + "inspection/fwzztz/data_up.do")
                .params("fwzztz", fwzztz)
                .execute(new stringCallback());
    }
}
