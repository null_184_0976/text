package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * Created by Administrator on 2018/3/10.
 */

public class YiQiTaiZhangModel extends DataSupport {
    private int id;//主键id
    private String bzmc;//		班站名称
    private String mc;//名称
    private String ggxh;//		规格型号
    private String bh;//编号
    private String sccj;//		生产厂家
    private String ccrq;//		出厂日期

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBzmc() {
        return bzmc;
    }

    public void setBzmc(String bzmc) {
        this.bzmc = bzmc;
    }

    public String getMc() {
        return mc;
    }

    public void setMc(String mc) {
        this.mc = mc;
    }

    public String getGgxh() {
        return ggxh;
    }

    public void setGgxh(String ggxh) {
        this.ggxh = ggxh;
    }

    public String getBh() {
        return bh;
    }

    public void setBh(String bh) {
        this.bh = bh;
    }

    public String getSccj() {
        return sccj;
    }

    public void setSccj(String sccj) {
        this.sccj = sccj;
    }

    public String getCcrq() {
        return ccrq;
    }

    public void setCcrq(String ccrq) {
        this.ccrq = ccrq;
    }
}
