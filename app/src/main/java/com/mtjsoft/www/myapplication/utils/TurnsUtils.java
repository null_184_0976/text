package com.mtjsoft.www.myapplication.utils;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 字符串转换成不同类型的数值
 *
 * @author mtj
 */
public class TurnsUtils {

    /**
     * String转换成int
     *
     * @param str      需要转换的数据源
     * @param defValue 默认值
     */
    public static int getInt(String str, int defValue) {
        try {
            return Integer.parseInt(str);
        } catch (Exception e) {
        }
        return defValue;
    }

    /**
     * String转换成float
     *
     * @param str      需要转换的数据源
     * @param defValue 默认值
     */
    public static float getFloat(String str, float defValue) {
        try {
            return Float.parseFloat(str);
        } catch (Exception e) {
        }
        return defValue;
    }

    /**
     * String转换成double
     *
     * @param str      需要转换的数据源
     * @param defValue 默认值
     */
    public static double getDouble(String str, double defValue) {
        try {
            return Double.parseDouble(str);
        } catch (Exception e) {
        }
        return defValue;
    }

    /**
     * 获取年份
     *
     * @return
     */
    public static int getSysYear() {
        Calendar date = Calendar.getInstance();
        String year = String.valueOf(date.get(Calendar.YEAR));
        return getInt(year, 2017);
    }

    /**
     * 把一个Date对象转换成相应格式的字符串
     *
     * @param date      时间
     * @param outFormat 输出的格式
     * @return 返回转换的字符串
     */
    @SuppressLint("SimpleDateFormat")
    public static String convertToString(Date date, String outFormat) {
        SimpleDateFormat format = new SimpleDateFormat(outFormat);
//        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        return format.format(date);
    }
}
