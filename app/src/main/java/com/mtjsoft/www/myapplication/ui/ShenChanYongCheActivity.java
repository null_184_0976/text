package com.mtjsoft.www.myapplication.ui;

import android.annotation.TargetApi;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.model.ShengChanYCModel;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mtj 生产用车使用记录
 *         Created by Administrator on 2018/3/9.
 */

public class ShenChanYongCheActivity extends BaseActivity {

    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private int col = 0;
    private int row = 20;
    private List<ShengChanYCModel> shengChanYCModelList;
    private String title = "生产用车使用记录";

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setOnClickListener(this);
        setTopName(title);
        addTableTitle();
        readData();
        addCenterView(row);
        setData();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        removeTopLayoutViewAll();
        String[] titles = getResources().getStringArray(R.array.shengchan_yongche);
        col = titles.length;
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < col; i++) {
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
            if (i == 3 || i == 4) {
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            }
            drawTextView.setLayoutParams(layoutParams);
            drawTextView.setText(titles[i]);
            view.addView(drawTextView);
        }
        addTopLayoutView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(int row) {
        for (int j = 0; j < row; j++) {
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            for (int i = 0; i < col; i++) {
                if (i == 0 || i == 1 || i == 2) {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                    drawEditText.setLayoutParams(layoutParams);
                    view.addView(drawEditText);
                } else if (i == 3 || i == 4) {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                    drawEditText.setLayoutParams(layoutParams);
                    view.addView(drawEditText);
                } else {
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                    drawTextView.setTag(i);
                    drawTextView.setOnClickListener(this);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                }
            }
            pLayout.addView(view);
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        if (v.getId() == R.id.iv_add_row) {
            addCenterView(5);
            return;
        }
        int tag = (int) v.getTag();
        switch (tag) {
            case 6:
            case 5:
                chooseTime();
                break;
            default:
                break;
        }
    }

    /**
     * 选择日期
     */
    private void chooseTime() {
        //时间选择器
        TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                clickText.setText(TurnsUtils.convertToString(date,"yyyy-MM-dd HH:mm"));
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(new boolean[]{true, true, true, true, true, false})
                .build();
        pvTime.show();
    }

    /**
     * 读取数据
     */
    private void readData() {
        shengChanYCModelList = DataSupport.findAll(ShengChanYCModel.class);
        if (shengChanYCModelList != null && shengChanYCModelList.size() > row) {
            row = shengChanYCModelList.size();
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        if (shengChanYCModelList != null && shengChanYCModelList.size() > 0) {
            for (int i = 0, plCh = shengChanYCModelList.size(); i < plCh; i++) {
                ShengChanYCModel model = shengChanYCModelList.get(i);
                if (pLayout.getChildAt(i) instanceof LinearLayout) {
                    LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                    ((DrawEditText) linearLayout.getChildAt(0)).setText(model.getCph());
                    ((DrawEditText) linearLayout.getChildAt(1)).setText(model.getCcyt());
                    ((DrawEditText) linearLayout.getChildAt(2)).setText(model.getJsyqz());
                    ((DrawEditText) linearLayout.getChildAt(3)).setText(model.getCfgls());
                    ((DrawEditText) linearLayout.getChildAt(4)).setText(model.getFhgls());
                    ((DrawTextView) linearLayout.getChildAt(5)).setText(model.getCcsj());
                    ((DrawTextView) linearLayout.getChildAt(6)).setText(model.getFhsj());
                }
            }
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        DataSupport.deleteAll(ShengChanYCModel.class);
        List<ShengChanYCModel> shengChanYCModelList = new ArrayList<>();
        for (int i = 0, plCh = pLayout.getChildCount(); i < plCh; i++) {
            ShengChanYCModel model = new ShengChanYCModel();
            if (pLayout.getChildAt(i) instanceof LinearLayout) {
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                DrawEditText drawTextView = (DrawEditText) linearLayout.getChildAt(0);
                if (!TextUtils.isEmpty(drawTextView.getText().toString())) {
                    model.setCph(drawTextView.getText().toString());
                    model.setCcyt(((DrawEditText) linearLayout.getChildAt(1)).getText().toString());
                    model.setJsyqz(((DrawEditText) linearLayout.getChildAt(2)).getText().toString());
                    model.setCfgls(((DrawEditText) linearLayout.getChildAt(3)).getText().toString());
                    model.setFhgls(((DrawEditText) linearLayout.getChildAt(4)).getText().toString());
                    model.setCcsj(((DrawTextView) linearLayout.getChildAt(5)).getText().toString());
                    model.setFhsj(((DrawTextView) linearLayout.getChildAt(6)).getText().toString());
                    shengChanYCModelList.add(model);
                }
            }
        }
        DataSupport.saveAll(shengChanYCModelList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
