package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author  mtj 智能巡检记录
 * Created by Administrator on 2018/3/13.
 */

public class ZhiNengXunJianModel extends DataSupport {
    private int id;//主键id
    private String bzmc;//班组名称
    private String sbbh;//设备编号
    private String sydd;//使用地点
    private String cj;//厂家
    private String xh;//型号
    private String bh;//编号
    private String tyrq;//投运日期
    private String cszsd;//	初始值设定
    private String cs;//次数
    private String xgdz;//修改定值

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBzmc() {
        return bzmc;
    }

    public void setBzmc(String bzmc) {
        this.bzmc = bzmc;
    }

    public String getSbbh() {
        return sbbh;
    }

    public void setSbbh(String sbbh) {
        this.sbbh = sbbh;
    }

    public String getSydd() {
        return sydd;
    }

    public void setSydd(String sydd) {
        this.sydd = sydd;
    }

    public String getCj() {
        return cj;
    }

    public void setCj(String cj) {
        this.cj = cj;
    }

    public String getXh() {
        return xh;
    }

    public void setXh(String xh) {
        this.xh = xh;
    }

    public String getBh() {
        return bh;
    }

    public void setBh(String bh) {
        this.bh = bh;
    }

    public String getTyrq() {
        return tyrq;
    }

    public void setTyrq(String tyrq) {
        this.tyrq = tyrq;
    }

    public String getCszsd() {
        return cszsd;
    }

    public void setCszsd(String cszsd) {
        this.cszsd = cszsd;
    }

    public String getCs() {
        return cs;
    }

    public void setCs(String cs) {
        this.cs = cs;
    }

    public String getXgdz() {
        return xgdz;
    }

    public void setXgdz(String xgdz) {
        this.xgdz = xgdz;
    }
}
