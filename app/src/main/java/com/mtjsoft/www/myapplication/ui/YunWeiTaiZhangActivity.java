package com.mtjsoft.www.myapplication.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.YunWeiTaiZhangModel;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mtj 运维记录及台账检查记录
 *         Created by Administrator on 2018/3/9.
 */

public class YunWeiTaiZhangActivity extends BaseActivity {

    private static final int CHOOSEName = 1001;
    private static final int CHOOSEBIANDIAN = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private int col = 0;
    private int row = 20;
    private List<YunWeiTaiZhangModel> yunWeiTaiZhangModelList;

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setOnClickListener(this);
        setTopName("运维记录及台账检查记录");
        addTableTitle();
        readData();
        addCenterView(row);
        setData();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        removeTopLayoutViewAll();
        String[] titles = getResources().getStringArray(R.array.yunwei_taizhang);
        col = titles.length;
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < col; i++) {
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
            if (i == 0 || i == 1 || i == 7 || i == 6) {
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            }
            drawTextView.setLayoutParams(layoutParams);
            drawTextView.setText(titles[i]);
            view.addView(drawTextView);
        }
        addTopLayoutView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(int row) {
        for (int j = 0; j < row; j++) {
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            for (int i = 0; i < col; i++) {
                if (i == 0 || i == 1 || i == 6 || i == 7) {
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                    drawTextView.setTag(i);
                    if (i == 0) {
                        drawTextView.setText((pLayout.getChildCount() + 1) + "");
                    }
                    drawTextView.setOnClickListener(this);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                } else if (i == 2){
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                    drawTextView.setTag(i);
                    drawTextView.setOnClickListener(this);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                } else {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                    drawEditText.setLayoutParams(layoutParams);
                    view.addView(drawEditText);
                }
            }
            pLayout.addView(view);
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        if (v.getId() == R.id.iv_add_row) {
            addCenterView(5);
            return;
        }
        Intent intent = null;
        int tag = (int) v.getTag();
        switch (tag) {
            case 1:
                chooseTime();
                break;
            case 2:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 1);
                startActivityForResult(intent, CHOOSEBIANDIAN);
                break;
            case 6:
            case 7:
                intent = new Intent(this, LineLockActivity.class);
                intent.putExtra("isChoose", true);
                startActivityForResult(intent, CHOOSEName);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name);
                        }
                    }
                    break;
                case CHOOSEName:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 选择日期
     */
    private void chooseTime() {
        //时间选择器
        TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                clickText.setText(TurnsUtils.convertToString(date,"yyyy-MM-dd"));
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(new boolean[]{true, true, true, false, false, false})
                .build();
        pvTime.show();
    }

    /**
     * 读取数据
     */
    private void readData() {
        yunWeiTaiZhangModelList = DataSupport.findAll(YunWeiTaiZhangModel.class);
        if (yunWeiTaiZhangModelList != null && yunWeiTaiZhangModelList.size() > row) {
            row = yunWeiTaiZhangModelList.size();
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        if (yunWeiTaiZhangModelList != null && yunWeiTaiZhangModelList.size() > 0) {
            for (int i = 0, plCh = yunWeiTaiZhangModelList.size(); i < plCh; i++) {
                YunWeiTaiZhangModel model = yunWeiTaiZhangModelList.get(i);
                if (pLayout.getChildAt(i) instanceof LinearLayout) {
                    LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                    ((DrawTextView) linearLayout.getChildAt(1)).setText(model.getRq());
                    ((DrawTextView) linearLayout.getChildAt(2)).setText(model.getBdzmc());
                    ((DrawEditText) linearLayout.getChildAt(3)).setText(model.getJcnr());
                    ((DrawEditText) linearLayout.getChildAt(4)).setText(model.getWtsm());
                    ((DrawEditText) linearLayout.getChildAt(5)).setText(model.getZgqk());
                    ((DrawTextView) linearLayout.getChildAt(6)).setText(model.getBzjcr());
                    ((DrawTextView) linearLayout.getChildAt(7)).setText(model.getYwdwjcr());
                }
            }
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        DataSupport.deleteAll(YunWeiTaiZhangModel.class);
        List<YunWeiTaiZhangModel> yunWeiTaiZhangModelList = new ArrayList<>();
        for (int i = 0, plCh = pLayout.getChildCount(); i < plCh; i++) {
            YunWeiTaiZhangModel model = new YunWeiTaiZhangModel();
            if (pLayout.getChildAt(i) instanceof LinearLayout) {
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                DrawTextView drawTextView = (DrawTextView) linearLayout.getChildAt(1);
                if (!TextUtils.isEmpty(drawTextView.getText().toString())) {
                    model.setRq(drawTextView.getText().toString());
                    model.setBdzmc(((DrawTextView) linearLayout.getChildAt(2)).getText().toString());
                    model.setJcnr(((DrawEditText) linearLayout.getChildAt(3)).getText().toString());
                    model.setWtsm(((DrawEditText) linearLayout.getChildAt(4)).getText().toString());
                    model.setZgqk(((DrawEditText) linearLayout.getChildAt(5)).getText().toString());
                    model.setBzjcr(((DrawTextView) linearLayout.getChildAt(6)).getText().toString());
                    model.setYwdwjcr(((DrawTextView) linearLayout.getChildAt(7)).getText().toString());
                    yunWeiTaiZhangModelList.add(model);
                }
            }
        }
        DataSupport.saveAll(yunWeiTaiZhangModelList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
