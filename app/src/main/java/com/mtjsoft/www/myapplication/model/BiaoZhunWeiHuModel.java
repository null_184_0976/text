package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 标准化作业卡模板
 *         Created by Administrator on 2018/3/14.
 */

public class BiaoZhunWeiHuModel extends DataSupport {
    private int id;//主键id
    private String sbscmc;//	设备双重名称（1号主变压器）
    private String gzsj;//工作时间
    private String zykbh;//	作业卡编号
    private String gjgx;//关键工序
    private String zlbzjyq;//	质量标准及要求
    private String fxbsykzcs;//		风险辨识与控制措施
    private String zxqk;//执行情况
    private String gzryqm;//	工作人员签名
    private String zxpj;//	执行评价
    private String gzfzrqm;//	工作负责人签名

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSbscmc() {
        return sbscmc;
    }

    public void setSbscmc(String sbscmc) {
        this.sbscmc = sbscmc;
    }

    public String getGzsj() {
        return gzsj;
    }

    public void setGzsj(String gzsj) {
        this.gzsj = gzsj;
    }

    public String getZykbh() {
        return zykbh;
    }

    public void setZykbh(String zykbh) {
        this.zykbh = zykbh;
    }

    public String getGjgx() {
        return gjgx;
    }

    public void setGjgx(String gjgx) {
        this.gjgx = gjgx;
    }

    public String getZlbzjyq() {
        return zlbzjyq;
    }

    public void setZlbzjyq(String zlbzjyq) {
        this.zlbzjyq = zlbzjyq;
    }

    public String getFxbsykzcs() {
        return fxbsykzcs;
    }

    public void setFxbsykzcs(String fxbsykzcs) {
        this.fxbsykzcs = fxbsykzcs;
    }

    public String getZxqk() {
        return zxqk;
    }

    public void setZxqk(String zxqk) {
        this.zxqk = zxqk;
    }

    public String getGzryqm() {
        return gzryqm;
    }

    public void setGzryqm(String gzryqm) {
        this.gzryqm = gzryqm;
    }

    public String getZxpj() {
        return zxpj;
    }

    public void setZxpj(String zxpj) {
        this.zxpj = zxpj;
    }

    public String getGzfzrqm() {
        return gzfzrqm;
    }

    public void setGzfzrqm(String gzfzrqm) {
        this.gzfzrqm = gzfzrqm;
    }
}
