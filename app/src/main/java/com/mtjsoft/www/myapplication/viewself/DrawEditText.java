package com.mtjsoft.www.myapplication.viewself;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.utils.HHDensityUtils;

/**
 * @author mtj
 * Created by Administrator on 2018/3/6.
 */

public class DrawEditText extends android.support.v7.widget.AppCompatEditText{

    private int width;
    private int height;
    private int pWidth = 0;
    private boolean left = true;
    private boolean top = true;
    private boolean right = true;
    private boolean bottom = true;
    /**
     * 画笔对象的引用
     */
    private Paint paint;
    private int paintColor = R.color.black_text;

    public DrawEditText(Context context) {
        super(context);
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(HHDensityUtils.dip2px(getContext(),1));
        pWidth = HHDensityUtils.dip2px(getContext(),1) / 2;
    }

    public DrawEditText(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(HHDensityUtils.dip2px(getContext(),1));
        pWidth = HHDensityUtils.dip2px(getContext(),1) / 2;
    }

    public DrawEditText(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(HHDensityUtils.dip2px(getContext(),1));
        pWidth = HHDensityUtils.dip2px(getContext(),1) / 2;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        width = getMeasuredWidth();
        height = getMeasuredHeight();
        paint.setColor(getResources().getColor(paintColor));
        if (left) {
            canvas.drawLine(pWidth,0,pWidth,height,paint);
        }
        if (top) {
            canvas.drawLine(0,pWidth,width,pWidth,paint);
        }
        if (right) {
            canvas.drawLine(width - pWidth,0,width - pWidth,height,paint);
        }
        if (bottom) {
            canvas.drawLine(0,height - pWidth,width,height-pWidth,paint);
        }
    }

    /**
     * 设置显示的边缘背景线
     * @param left
     * @param top
     * @param right
     * @param bottom
     */
    public synchronized void setLineShow(boolean left,boolean top,boolean right,boolean bottom){
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
        postInvalidate();
    }

    public synchronized void setPaintColorById(int color){
        paintColor = color;
    }
}
