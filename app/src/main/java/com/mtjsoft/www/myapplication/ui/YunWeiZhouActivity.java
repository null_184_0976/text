package com.mtjsoft.www.myapplication.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.OptionsPickerView;
import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.YunWeiZModel;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author mtj 运维计划周计划表
 *         Created by Administrator on 2018/3/9.
 */

public class YunWeiZhouActivity extends BaseActivity {

    private static final int CHOOSENAME = 1001;
    private static final int CHOOSECHENGYUAN = 1002;
    private static final int CHOOSEBIANDIAN = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private int col = 0;
    private int row = 20;
    private List<YunWeiZModel> yunWeiZModelList;
    private String ban = "___";
    private String zhou = "___";

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setOnClickListener(this);
        setTopName(String.format(getString(R.string.ban_zhou), ban, zhou));
        addTableTitle();
        readData();
        addCenterView(row);
        setData();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        String[] titles = getResources().getStringArray(R.array.yunwei_zhou);
        col = titles.length;
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < col; i++) {
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
            if (i == 0 || i == col - 1) {
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            }
            drawTextView.setLayoutParams(layoutParams);
            drawTextView.setText(titles[i]);
            view.addView(drawTextView);
        }
        addTopLayoutView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(int row) {
        for (int j = 0; j < row; j++) {
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            for (int i = 0; i < col; i++) {
                if (i == 3) {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                    drawEditText.setLayoutParams(layoutParams);
                    drawEditText.setEnabled(true);
                    drawEditText.setTag(i);
                    view.addView(drawEditText);
                } else if (i == col - 1) {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                    drawEditText.setLayoutParams(layoutParams);
                    drawEditText.setEnabled(true);
                    drawEditText.setTag(i);
                    view.addView(drawEditText);
                } else {
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                    drawTextView.setTag(i);
                    if (i == 0) {
                        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                        drawTextView.setText((pLayout.getChildCount() + 1) + "");
                    } else {
                        drawTextView.setOnClickListener(this);
                    }
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                }
            }
            pLayout.addView(view);
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        if (v.getId() == R.id.iv_add_row) {
            addCenterView(5);
            return;
        }
        if (v.getId() == R.id.tv_base_top_name) {
            chooseOptions();
            return;
        }
        Intent intent = null;
        int tag = (int) v.getTag();
        switch (tag) {
            case 1:
                chooseTime();
                break;
            case 2:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 1);
                startActivityForResult(intent, CHOOSEBIANDIAN);
                break;
            case 4:
                intent = new Intent(this, LineLockActivity.class);
                intent.putExtra("isChoose", true);
                startActivityForResult(intent, CHOOSENAME);
                break;
            case 5:
                intent = new Intent(this, LineLockActivity.class);
                intent.putExtra("isChoose", true);
                intent.putExtra("chooseNames", clickText.getText().toString());
                startActivityForResult(intent, CHOOSECHENGYUAN);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSENAME:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                case CHOOSECHENGYUAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name);
                        }
                    }
                    break;
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 选择日期
     */
    private void chooseTime() {
        //时间选择器
        TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                clickText.setText(TurnsUtils.convertToString(date,"yyyy-MM-dd HH:00——mm:00"));
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(new boolean[]{true, true, true, true, true, false})
                .setLabel("年", "月", "日", "时至", "时", "")
                .build();
        pvTime.show();
    }

    /**
     * 表名选择
     */
    private void chooseOptions() {
        //添加数据源
        int option1 = 0;
        int option2 = 0;
        final List<String> options1Items = new ArrayList<>();
        final List<String> options2Items = new ArrayList<>();
        String[] bans = getResources().getStringArray(R.array.bans);
        options1Items.addAll(Arrays.asList(bans));
        for (int i = 1; i < 60; i++) {
            options2Items.add("" + i);
        }
        OptionsPickerView pvOptions = new OptionsPickerView.Builder(this, new OptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int options2, int options3, View v) {
                //返回的分别是三个级别的选中位置
                ban = options1Items.get(options1);
                zhou = options2Items.get(options2);
                clickText.setText(String.format(getString(R.string.ban_zhou), ban, zhou));
            }
        })
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(false)
                //设置选择的三级单位
                .setLabels(getString(R.string.ban), getString(R.string.zhou), "")
                //循环与否
                .setCyclic(false, false, false)
                //设置默认选中项
                .setSelectOptions(option1, option2, 0)
                //点击外部dismiss default true
                .setOutSideCancelable(true)
                //是否显示为对话框样式
                .isDialog(true)
                .build();
        pvOptions.setNPicker(options1Items, options2Items, null);
        pvOptions.show();
    }

    /**
     * 读取数据
     */
    private void readData() {
        yunWeiZModelList = DataSupport.findAll(YunWeiZModel.class);
        if (yunWeiZModelList != null && yunWeiZModelList.size() > row) {
            row = yunWeiZModelList.size();
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        if (yunWeiZModelList != null && yunWeiZModelList.size() > 0) {
            for (int i = 0, plCh = yunWeiZModelList.size(); i < plCh; i++) {
                YunWeiZModel yunWeiZModel = yunWeiZModelList.get(i);
                ban = yunWeiZModel.getBzmc();
                zhou = yunWeiZModel.getZc();
                setTopName(String.format(getString(R.string.ban_zhou), ban, zhou));
                if (pLayout.getChildAt(i) instanceof LinearLayout) {
                    LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                    ((DrawTextView) linearLayout.getChildAt(1)).setText(yunWeiZModel.getSj());
                    ((DrawTextView) linearLayout.getChildAt(2)).setText(yunWeiZModel.getBdz());
                    ((DrawEditText) linearLayout.getChildAt(3)).setText(yunWeiZModel.getGznr());
                    ((DrawTextView) linearLayout.getChildAt(4)).setText(yunWeiZModel.getZrr());
                    ((DrawTextView) linearLayout.getChildAt(5)).setText(yunWeiZModel.getGzry());
                    ((DrawEditText) linearLayout.getChildAt(6)).setText(yunWeiZModel.getBz());
                }
            }
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        DataSupport.deleteAll(YunWeiZModel.class);
        List<YunWeiZModel> yunWeiZModelList = new ArrayList<>();
        for (int i = 0, plCh = pLayout.getChildCount(); i < plCh; i++) {
            YunWeiZModel yunWeiZModel = new YunWeiZModel();
            yunWeiZModel.setBzmc(ban);
            yunWeiZModel.setZc(zhou);
            if (pLayout.getChildAt(i) instanceof LinearLayout) {
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                DrawTextView drawTextView = (DrawTextView) linearLayout.getChildAt(1);
                if (!TextUtils.isEmpty(drawTextView.getText().toString())) {
                    yunWeiZModel.setSj(drawTextView.getText().toString());
                    yunWeiZModel.setBdz(((DrawTextView) linearLayout.getChildAt(2)).getText().toString());
                    yunWeiZModel.setGznr(((DrawEditText) linearLayout.getChildAt(3)).getText().toString());
                    yunWeiZModel.setZrr(((DrawTextView) linearLayout.getChildAt(4)).getText().toString());
                    yunWeiZModel.setGzry(((DrawTextView) linearLayout.getChildAt(5)).getText().toString());
                    yunWeiZModel.setBz(((DrawEditText) linearLayout.getChildAt(6)).getText().toString());
                    yunWeiZModelList.add(yunWeiZModel);
                }
            }
        }
        DataSupport.saveAll(yunWeiZModelList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
