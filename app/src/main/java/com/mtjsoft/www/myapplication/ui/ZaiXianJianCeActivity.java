package com.mtjsoft.www.myapplication.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.model.ZaiXianJianCeModel;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mtj 在线监测装置台账及告警值设定、修改记录
 *         Created by Administrator on 2018/3/9.
 */

public class ZaiXianJianCeActivity extends BaseActivity {

    private static final int CHOOSEBIANDIAN = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private int row = 20;
    private List<ZaiXianJianCeModel> zaiXianJianCeModelList;
    private List<List<ZaiXianJianCeModel>> listRowDatas = new ArrayList<>();
    private String zhan = "___";

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setOnClickListener(this);
        setTopName(String.format(getString(R.string.zaixian_jiance_title), zhan));
        addTableTitle();
        readData();
        addCenterView(row);
        setData();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        removeTopLayoutViewAll();
        String[] titles = getResources().getStringArray(R.array.zaixianjiance);
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < 7; i++) {
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            if (i == 1 || i == 3) {
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
            }
            drawTextView.setLayoutParams(layoutParams);
            drawTextView.setText(titles[i]);
            view.addView(drawTextView);
        }
        LinearLayout shuLayout = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_vertical_linearlayout_f_f, null);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 3);
        shuLayout.setLayoutParams(layoutParams);
        //添加告警值设定修改情况
        DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText(titles[7]);
        shuLayout.addView(drawTextView);
        //
        LinearLayout hengLayout = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_f, null);
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText(titles[8]);
        hengLayout.addView(drawTextView);
        //
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText(titles[9]);
        hengLayout.addView(drawTextView);
        //
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText(titles[10]);
        hengLayout.addView(drawTextView);
        shuLayout.addView(hengLayout);
        view.addView(shuLayout);
        addTopLayoutView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(int row) {
        for (int j = 0; j < row; j++) {
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            for (int i = 0; i < 10; i++) {
                if (i == 5) {
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                    drawTextView.setTag(i);
                    drawTextView.setOnClickListener(this);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                } else if (i == 1 || i == 3) {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                    drawEditText.setLayoutParams(layoutParams);
                    view.addView(drawEditText);
                } else {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                    drawEditText.setTag(i);
                    if (i == 0) {
                        drawEditText.setEnabled(false);
                        drawEditText.setText((pLayout.getChildCount() + 1) + "");
                    }
                    drawEditText.setLayoutParams(layoutParams);
                    view.addView(drawEditText);
                }
            }
            pLayout.addView(view);
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        if (v.getId() == R.id.iv_add_row) {
            addCenterView(5);
            return;
        }
        if (v.getId() == R.id.tv_base_top_name) {
            Intent intent = new Intent(this, ChooseActivity.class);
            intent.putExtra("type", 1);
            startActivityForResult(intent, CHOOSEBIANDIAN);
            return;
        }
        int tag = (int) v.getTag();
        switch (tag) {
            case 5:
                chooseTime();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            zhan = name;
                            setTopName(String.format(getString(R.string.zaixian_jiance_title), zhan));
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 选择日期
     */
    private void chooseTime() {
        //时间选择器
        TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                clickText.setText(TurnsUtils.convertToString(date,"yyyy-MM-dd"));
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(new boolean[]{true, true, true, false, false, false})
                .build();
        pvTime.show();
    }

    /**
     * 读取数据
     */
    private void readData() {
        String type = "0";
        zaiXianJianCeModelList = DataSupport.findAll(ZaiXianJianCeModel.class);
        if (zaiXianJianCeModelList != null && zaiXianJianCeModelList.size() > 0) {
            for (int i = 0; i < zaiXianJianCeModelList.size(); i++) {
                if (!type.equals(zaiXianJianCeModelList.get(i).getZzmc())) {
                    type = zaiXianJianCeModelList.get(i).getZzmc();
                    List<ZaiXianJianCeModel> rowList = DataSupport.where("zzmc = ?", type).find(ZaiXianJianCeModel.class);
                    listRowDatas.add(rowList);
                }
            }
        }
        if (listRowDatas != null && listRowDatas.size() > row) {
            row = listRowDatas.size();
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        if (listRowDatas != null && listRowDatas.size() > 0) {
            for (int i = 0, plCh = listRowDatas.size(); i < plCh; i++) {
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                ZaiXianJianCeModel model = listRowDatas.get(i).get(0);
                if (!TextUtils.isEmpty(model.getZm())) {
                    zhan = model.getZm();
                }
                ((DrawEditText) linearLayout.getChildAt(1)).setText(model.getZzmc());
                ((DrawEditText) linearLayout.getChildAt(2)).setText(model.getAzwz());
                ((DrawEditText) linearLayout.getChildAt(3)).setText(model.getCj());
                ((DrawEditText) linearLayout.getChildAt(4)).setText(model.getXh());
                ((DrawTextView) linearLayout.getChildAt(5)).setText(model.getTyrq());
                ((DrawEditText) linearLayout.getChildAt(6)).setText(model.getCszsd());
                for (int j = 7; j < listRowDatas.get(i).size() + 7; j++) {
                    if (j < 10) {
                        ((DrawEditText) linearLayout.getChildAt(j)).setText(listRowDatas.get(i).get(j - 7).getXgdz());
                    }
                }
            }
            setTopName(String.format(getString(R.string.zaixian_jiance_title), zhan));
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        DataSupport.deleteAll(ZaiXianJianCeModel.class);
        List<ZaiXianJianCeModel> zaiXianJianCeModelList = new ArrayList<>();
        for (int i = 0, plCh = pLayout.getChildCount(); i < plCh; i++) {
            if (pLayout.getChildAt(i) instanceof LinearLayout) {
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                DrawEditText drawTextView = (DrawEditText) linearLayout.getChildAt(1);
                if (!TextUtils.isEmpty(drawTextView.getText().toString())) {
                    //三次加入三个
                    for (int j = 7; j < linearLayout.getChildCount(); j++) {
                        ZaiXianJianCeModel model = new ZaiXianJianCeModel();
                        model.setZm(zhan);
                        model.setZzmc(drawTextView.getText().toString());
                        model.setAzwz(((DrawEditText) linearLayout.getChildAt(2)).getText().toString());
                        model.setCj(((DrawEditText) linearLayout.getChildAt(3)).getText().toString());
                        model.setXh(((DrawEditText) linearLayout.getChildAt(4)).getText().toString());
                        model.setTyrq(((DrawTextView) linearLayout.getChildAt(5)).getText().toString());
                        model.setCszsd(((DrawEditText) linearLayout.getChildAt(6)).getText().toString());
                        model.setCs("" + (j - 6));
                        model.setXgdz(((DrawEditText) linearLayout.getChildAt(j)).getText().toString());
                        zaiXianJianCeModelList.add(model);
                    }
                }
            }
        }
        DataSupport.saveAll(zaiXianJianCeModelList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
