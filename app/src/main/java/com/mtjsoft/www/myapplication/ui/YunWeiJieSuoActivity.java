package com.mtjsoft.www.myapplication.ui;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.YunWeiJieSuoModel;
import com.mtjsoft.www.myapplication.utils.HHInputUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mtj 解锁钥匙使用记录
 *         Created by Administrator on 2018/3/11.
 */

public class YunWeiJieSuoActivity extends BaseActivity {

    private static final int CHOOSEBIANDIAN = 1001;
    private static final int CHOOSEGJ = 1002;
    private static final int CHOOSENAME = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private int row = 20;
    private LinearLayout pLayout;
    private List<YunWeiJieSuoModel> yunWeiJieSuoModelList = new ArrayList<>();
    private List<DrawTextView> drawTextViewList = new ArrayList<>();
    private List<DrawEditText> drawEditTextList = new ArrayList<>();

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setOnClickListener(this);
        setTopName("解锁钥匙使用记录");
        addTopView("变电站名称：", true, "页次：", true);
        addTopLayoutView(View.inflate(getBaseContext(), R.layout.layout_yunwei_jiesuo_top, null));
        readData();
        addCenterViews(row);
        setData();
    }

    /**
     * 添加中间表格
     *
     * @param row
     */
    private void addCenterViews(int row) {
        for (int i = 0; i < row; i++) {
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            for (int j = 0; j < 15; j++) {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                if (j == 3 || j == 4 || j == 5 || j == 6 || j == 7 || j == 9) {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    drawEditText.setLayoutParams(layoutParams);
                    view.addView(drawEditText);
                } else {
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setOnClickListener(this);
                    if (j == 0) {
                        drawTextView.setText("" + (pLayout.getChildCount() + 1));
                    }
                    drawTextView.setLayoutParams(layoutParams);
                    drawTextView.setTag("" + j);
                    view.addView(drawTextView);
                }
            }
            pLayout.addView(view);
        }
    }

    /**
     * 添加头部表格
     */
    private void addTopView(String leftName, boolean leftIsText, String rightName, boolean rightIsText) {
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < 4; i++) {
            DrawTextView drawTextView = null;
            DrawEditText drawEditText = null;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            switch (i) {
                case 0:
                    drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(leftName);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                    break;
                case 1:
                    if (leftIsText) {
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setLayoutParams(layoutParams);
                        drawTextView.setOnClickListener(this);
                        drawTextView.setTag("zhan");
                        drawTextViewList.add(drawTextView);
                        view.addView(drawTextView);
                    } else {
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        drawEditTextList.add(drawEditText);
                        view.addView(drawEditText);
                    }
                    break;
                case 2:
                    drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(rightName);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                    break;
                case 3:
                    if (rightIsText) {
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setLayoutParams(layoutParams);
                        drawTextView.setOnClickListener(this);
                        drawTextView.setTag("ye");
                        drawTextViewList.add(drawTextView);
                        view.addView(drawTextView);
                    } else {
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        drawEditTextList.add(drawEditText);
                        view.addView(drawEditText);
                    }
                    break;
                default:
                    break;
            }
        }
        addTopLayoutView(view);
    }


    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        if (v.getId() == R.id.iv_add_row) {
            addCenterViews(5);
            return;
        }
        Intent intent = null;
        String tag = (String) v.getTag();
        switch (tag) {
            case "zhan":
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 1);
                startActivityForResult(intent, CHOOSEBIANDIAN);
                break;
            case "ye":
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 4);
                startActivityForResult(intent, CHOOSEBIANDIAN);
                break;
            case "1":
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 7);
                startActivityForResult(intent, CHOOSEBIANDIAN);
                break;
            case "2":
            case "13":
                HHInputUtils.chooseTime(true, clickText);
                break;
            case "8":
            case "10":
            case "11":
            case "12":
            case "14":
                intent = new Intent(this, LineLockActivity.class);
                intent.putExtra("isChoose", true);
                startActivityForResult(intent, CHOOSENAME);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSENAME:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 读取数据
     */
    private void readData() {
        yunWeiJieSuoModelList = DataSupport.findAll(YunWeiJieSuoModel.class);
        if (yunWeiJieSuoModelList != null && yunWeiJieSuoModelList.size() > row) {
            row = yunWeiJieSuoModelList.size();
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        if (yunWeiJieSuoModelList != null && yunWeiJieSuoModelList.size() > 0) {
            for (int i = 0; i < yunWeiJieSuoModelList.size(); i++) {
                YunWeiJieSuoModel model = yunWeiJieSuoModelList.get(i);
                drawTextViewList.get(0).setText(model.getBdzmc());
                drawTextViewList.get(1).setText(model.getYc());
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                ((DrawTextView) linearLayout.getChildAt(1)).setText(model.getJslx());
                ((DrawTextView) linearLayout.getChildAt(2)).setText(model.getSysj());
                ((DrawEditText) linearLayout.getChildAt(3)).setText(model.getCznr());
                ((DrawEditText) linearLayout.getChildAt(4)).setText(model.getJsgjmc());
                ((DrawEditText) linearLayout.getChildAt(5)).setText(model.getJs());
                ((DrawEditText) linearLayout.getChildAt(6)).setText(model.getJsdx());
                ((DrawEditText) linearLayout.getChildAt(7)).setText(model.getJsdxhf());
                ((DrawTextView) linearLayout.getChildAt(8)).setText(model.getJsdxhfr());
                ((DrawEditText) linearLayout.getChildAt(9)).setText(model.getJsyy());
                ((DrawTextView) linearLayout.getChildAt(10)).setText(model.getSqry());
                ((DrawTextView) linearLayout.getChildAt(11)).setText(model.getXcqrry());
                ((DrawTextView) linearLayout.getChildAt(12)).setText(model.getPzry());
                ((DrawTextView) linearLayout.getChildAt(13)).setText(model.getJsgjhfsj());
                ((DrawTextView) linearLayout.getChildAt(14)).setText(model.getJsgjhfr());
            }
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        DataSupport.deleteAll(YunWeiJieSuoModel.class);
        List<YunWeiJieSuoModel> yunWeiJieSuoModelList = new ArrayList<>();
        for (int i = 0; i < pLayout.getChildCount(); i++) {
            LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
            String type = ((DrawTextView) linearLayout.getChildAt(1)).getText().toString();
            if (!TextUtils.isEmpty(type)) {
                YunWeiJieSuoModel model = new YunWeiJieSuoModel();
                model.setBdzmc(drawTextViewList.get(0).getText().toString());
                model.setYc(drawTextViewList.get(1).getText().toString());
                model.setJslx(type);
                model.setSysj(((DrawTextView) linearLayout.getChildAt(2)).getText().toString());
                model.setCznr(((DrawEditText) linearLayout.getChildAt(3)).getText().toString());
                model.setJsgjmc(((DrawEditText) linearLayout.getChildAt(4)).getText().toString());
                model.setJs(((DrawEditText) linearLayout.getChildAt(5)).getText().toString());
                model.setJsdx(((DrawEditText) linearLayout.getChildAt(6)).getText().toString());
                model.setJsdxhf(((DrawEditText) linearLayout.getChildAt(7)).getText().toString());
                model.setJsdxhfr(((DrawTextView) linearLayout.getChildAt(8)).getText().toString());
                model.setJsyy(((DrawEditText) linearLayout.getChildAt(9)).getText().toString());
                model.setSqry(((DrawTextView) linearLayout.getChildAt(10)).getText().toString());
                model.setXcqrry(((DrawTextView) linearLayout.getChildAt(11)).getText().toString());
                model.setPzry(((DrawTextView) linearLayout.getChildAt(12)).getText().toString());
                model.setJsgjhfsj(((DrawTextView) linearLayout.getChildAt(13)).getText().toString());
                model.setJsgjhfr(((DrawTextView) linearLayout.getChildAt(14)).getText().toString());
                yunWeiJieSuoModelList.add(model);
            }
        }
        DataSupport.saveAll(yunWeiJieSuoModelList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
