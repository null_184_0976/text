package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 消防设施、器材台账
 *         Created by Administrator on 2018/3/10.
 */

public class XiaoFangTaiZhangModel extends DataSupport {
    private int id;//主键id
    private String bzmc;//		班站名称
    private String mc;//名称
    private String ggxh;//		规格型号
    private String sl;//数量
    private String dw;//单位
    private String mqzt;//		目前状态
    private String sywz;//		使用位置
    private String zrr;//	责任人
    private String bz;//备注

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBzmc() {
        return bzmc;
    }

    public void setBzmc(String bzmc) {
        this.bzmc = bzmc;
    }

    public String getMc() {
        return mc;
    }

    public void setMc(String mc) {
        this.mc = mc;
    }

    public String getGgxh() {
        return ggxh;
    }

    public void setGgxh(String ggxh) {
        this.ggxh = ggxh;
    }

    public String getSl() {
        return sl;
    }

    public void setSl(String sl) {
        this.sl = sl;
    }

    public String getDw() {
        return dw;
    }

    public void setDw(String dw) {
        this.dw = dw;
    }

    public String getMqzt() {
        return mqzt;
    }

    public void setMqzt(String mqzt) {
        this.mqzt = mqzt;
    }

    public String getSywz() {
        return sywz;
    }

    public void setSywz(String sywz) {
        this.sywz = sywz;
    }

    public String getZrr() {
        return zrr;
    }

    public void setZrr(String zrr) {
        this.zrr = zrr;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }
}
