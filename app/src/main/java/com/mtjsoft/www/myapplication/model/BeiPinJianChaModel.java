package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 备品备件检查记录
 *         Created by Administrator on 2018/3/11.
 */

public class BeiPinJianChaModel extends DataSupport {
    private int id;//主键id
    private String bzmc;//班站名称
    private String rq;//日期
    private String mc;//名称
    private String xh;//型号
    private String sl;//数量
    private String jcqk;//检查情况
    private String bzjcr;//		班组检查人
    private String bz;//备注

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBzmc() {
        return bzmc;
    }

    public void setBzmc(String bzmc) {
        this.bzmc = bzmc;
    }

    public String getRq() {
        return rq;
    }

    public void setRq(String rq) {
        this.rq = rq;
    }

    public String getMc() {
        return mc;
    }

    public void setMc(String mc) {
        this.mc = mc;
    }

    public String getXh() {
        return xh;
    }

    public void setXh(String xh) {
        this.xh = xh;
    }

    public String getSl() {
        return sl;
    }

    public void setSl(String sl) {
        this.sl = sl;
    }

    public String getJcqk() {
        return jcqk;
    }

    public void setJcqk(String jcqk) {
        this.jcqk = jcqk;
    }

    public String getBzjcr() {
        return bzjcr;
    }

    public void setBzjcr(String bzjcr) {
        this.bzjcr = bzjcr;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }
}
