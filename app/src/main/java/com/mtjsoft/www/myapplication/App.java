package com.mtjsoft.www.myapplication;

import android.app.Application;

import com.lzy.okgo.OkGo;

import org.litepal.LitePal;

/**
 * Created by Administrator on 2018/3/8.
 */

public class App extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        LitePal.initialize(this);
        OkGo.getInstance().init(this);
    }
}
