package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 变电运维工作日志
 *         Created by Administrator on 2018/3/17.
 */

public class YunWeiRiZhiModel extends DataSupport {
    private int id;//主键id
    private String bdzmc;//	变电站名称
    private String ywbz;//运维班站
    private String tbrq;//填表日期
    private String xq;//星期
    private String tq;//天气
    private String jbsj;//交班时间（当值交班终了时间）
    private String jbr;//交班人
    private String jiebsj;//	接班时间（当值接班终了时间）
    private String jiebr;//	接班人
    private String yxfs;//运行方式
    private String xsrq;//巡视日期
    private String xsgz;//巡视工作
    private String tkrq;//调控日期
    private String tkzl;//调控指令
    private String qxrq;//缺陷日期
    private String qxqk;//缺陷情况
    private String gzprq;//	工作票日期
    private String gzpzx;//	工作票执行
    private String dzrq;//倒闸日期
    private String dzcz;//倒闸操作
    private String bhrq;//保护日期
    private String bhfs;//保护方式
    private String gzrq;//故障日期
    private String gzqk;//故障情况
    private String jdrq;//接地日期
    private String jdxsy;//	接地线使用
    private String jsrq;//解锁日期
    private String jsys;//解锁钥匙使用
    private String wjrq;//文件日期
    private String wjtz;//文件通知
    private String sbrq;//设备日期
    private String sbwh;//设备维护
    private String qtrq;//其他日期
    private String qt;//其他

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBdzmc() {
        return bdzmc;
    }

    public void setBdzmc(String bdzmc) {
        this.bdzmc = bdzmc;
    }

    public String getYwbz() {
        return ywbz;
    }

    public void setYwbz(String ywbz) {
        this.ywbz = ywbz;
    }

    public String getTbrq() {
        return tbrq;
    }

    public void setTbrq(String tbrq) {
        this.tbrq = tbrq;
    }

    public String getXq() {
        return xq;
    }

    public void setXq(String xq) {
        this.xq = xq;
    }

    public String getTq() {
        return tq;
    }

    public void setTq(String tq) {
        this.tq = tq;
    }

    public String getJbsj() {
        return jbsj;
    }

    public void setJbsj(String jbsj) {
        this.jbsj = jbsj;
    }

    public String getJbr() {
        return jbr;
    }

    public void setJbr(String jbr) {
        this.jbr = jbr;
    }

    public String getJiebsj() {
        return jiebsj;
    }

    public void setJiebsj(String jiebsj) {
        this.jiebsj = jiebsj;
    }

    public String getJiebr() {
        return jiebr;
    }

    public void setJiebr(String jiebr) {
        this.jiebr = jiebr;
    }

    public String getYxfs() {
        return yxfs;
    }

    public void setYxfs(String yxfs) {
        this.yxfs = yxfs;
    }

    public String getXsrq() {
        return xsrq;
    }

    public void setXsrq(String xsrq) {
        this.xsrq = xsrq;
    }

    public String getXsgz() {
        return xsgz;
    }

    public void setXsgz(String xsgz) {
        this.xsgz = xsgz;
    }

    public String getTkrq() {
        return tkrq;
    }

    public void setTkrq(String tkrq) {
        this.tkrq = tkrq;
    }

    public String getTkzl() {
        return tkzl;
    }

    public void setTkzl(String tkzl) {
        this.tkzl = tkzl;
    }

    public String getQxrq() {
        return qxrq;
    }

    public void setQxrq(String qxrq) {
        this.qxrq = qxrq;
    }

    public String getQxqk() {
        return qxqk;
    }

    public void setQxqk(String qxqk) {
        this.qxqk = qxqk;
    }

    public String getGzprq() {
        return gzprq;
    }

    public void setGzprq(String gzprq) {
        this.gzprq = gzprq;
    }

    public String getGzpzx() {
        return gzpzx;
    }

    public void setGzpzx(String gzpzx) {
        this.gzpzx = gzpzx;
    }

    public String getDzrq() {
        return dzrq;
    }

    public void setDzrq(String dzrq) {
        this.dzrq = dzrq;
    }

    public String getDzcz() {
        return dzcz;
    }

    public void setDzcz(String dzcz) {
        this.dzcz = dzcz;
    }

    public String getBhrq() {
        return bhrq;
    }

    public void setBhrq(String bhrq) {
        this.bhrq = bhrq;
    }

    public String getBhfs() {
        return bhfs;
    }

    public void setBhfs(String bhfs) {
        this.bhfs = bhfs;
    }

    public String getGzrq() {
        return gzrq;
    }

    public void setGzrq(String gzrq) {
        this.gzrq = gzrq;
    }

    public String getGzqk() {
        return gzqk;
    }

    public void setGzqk(String gzqk) {
        this.gzqk = gzqk;
    }

    public String getJdrq() {
        return jdrq;
    }

    public void setJdrq(String jdrq) {
        this.jdrq = jdrq;
    }

    public String getJdxsy() {
        return jdxsy;
    }

    public void setJdxsy(String jdxsy) {
        this.jdxsy = jdxsy;
    }

    public String getJsrq() {
        return jsrq;
    }

    public void setJsrq(String jsrq) {
        this.jsrq = jsrq;
    }

    public String getJsys() {
        return jsys;
    }

    public void setJsys(String jsys) {
        this.jsys = jsys;
    }

    public String getWjrq() {
        return wjrq;
    }

    public void setWjrq(String wjrq) {
        this.wjrq = wjrq;
    }

    public String getWjtz() {
        return wjtz;
    }

    public void setWjtz(String wjtz) {
        this.wjtz = wjtz;
    }

    public String getSbrq() {
        return sbrq;
    }

    public void setSbrq(String sbrq) {
        this.sbrq = sbrq;
    }

    public String getSbwh() {
        return sbwh;
    }

    public void setSbwh(String sbwh) {
        this.sbwh = sbwh;
    }

    public String getQtrq() {
        return qtrq;
    }

    public void setQtrq(String qtrq) {
        this.qtrq = qtrq;
    }

    public String getQt() {
        return qt;
    }

    public void setQt(String qt) {
        this.qt = qt;
    }
}
