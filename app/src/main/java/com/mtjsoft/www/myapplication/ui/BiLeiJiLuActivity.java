package com.mtjsoft.www.myapplication.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.OptionsPickerView;
import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.BiLeiJiLuModel;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mtj 避雷器动作及泄漏电流记录
 *         Created by Administrator on 2018/3/9.
 */

public class BiLeiJiLuActivity extends BaseActivity {

    private static final int CHOOSEName = 1001;
    private static final int CHOOSENumber = 1002;
    private static final int CHOOSEBIANDIAN = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private int row = 20;
    private int col = 0;
    private DrawTextView zhanDrawTextView;
    private DrawTextView timeDrawTextView;
    private DrawTextView renDrawTextView;
    private DrawEditText qingkDrawEditText;
    private List<BiLeiJiLuModel> biLeiJiLuModelList;

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setOnClickListener(this);
        setTopName("避雷器动作及泄漏电流记录");
        addTableTitle();
        readData();
        addCenterView(row);
        setData();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        removeTopLayoutViewAll();

        String[] topNames = {"* 变电站", "", "* 检查时间", "", "* 检查人", "", "检查情况"};
        LinearLayout topView = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        for (int i = 0; i < topNames.length; i++) {
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            drawTextView.setLayoutParams(layoutParam);
            drawTextView.setText(topNames[i]);
            switch (i) {
                case 1:
                    zhanDrawTextView = drawTextView;
                    drawTextView.setTag("zhan");
                    drawTextView.setOnClickListener(this);
                    break;
                case 3:
                    timeDrawTextView = drawTextView;
                    drawTextView.setTag("time");
                    drawTextView.setOnClickListener(this);
                    break;
                case 5:
                    renDrawTextView = drawTextView;
                    drawTextView.setTag("ren");
                    drawTextView.setOnClickListener(this);
                    break;
                default:
                    break;
            }
            topView.addView(drawTextView);
        }
        qingkDrawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
        qingkDrawEditText.setLayoutParams(layoutParam);
        topView.addView(qingkDrawEditText);
        addTopLayoutView(topView);
        //
        String[] titles = getResources().getStringArray(R.array.bileijilu);
        col = titles.length;
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        for (int i = 0, size = titles.length; i < size; i++) {
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            drawTextView.setLayoutParams(layoutParams);
            drawTextView.setText(titles[i]);
            view.addView(drawTextView);
        }
        addTopLayoutView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(int row) {
        for (int j = 0; j < row; j++) {
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            for (int i = 0; i < col; i++) {
                if (i == 1 || i == 2 || i == 3 || i == 12 || i == 13) {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    drawEditText.setLayoutParams(layoutParams);
                    view.addView(drawEditText);
                } else {
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    if (i == 0) {
                        drawTextView.setText((pLayout.getChildCount() + 1) + "");
                    }
                    drawTextView.setTag(i + "");
                    drawTextView.setLayoutParams(layoutParams);
                    drawTextView.setOnClickListener(this);
                    view.addView(drawTextView);
                }
            }
            pLayout.addView(view);
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        if (v.getId() == R.id.iv_add_row) {
            addCenterView(5);
            return;
        }
        String tag = (String) v.getTag();
        Intent intent = null;
        switch (tag) {
            case "zhan":
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 1);
                startActivityForResult(intent, CHOOSEBIANDIAN);
                break;
            case "time":
                chooseTime();
                break;
            case "ren":
                intent = new Intent(this, LineLockActivity.class);
                intent.putExtra("isChoose", true);
                startActivityForResult(intent, CHOOSEName);
                break;
            case "4":
            case "5":
            case "6":
                chooseOptions();
                break;
            case "7":
            case "8":
            case "9":
            case "10":
            case "11":
            case "14":
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 4);
                startActivityForResult(intent, CHOOSENumber);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name);
                        }
                    }
                    break;
                case CHOOSEName:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                case CHOOSENumber:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 选择日期
     */
    private void chooseTime() {
        //时间选择器
        TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                clickText.setText(TurnsUtils.convertToString(date,"yyyy-MM-dd HH:mm"));
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(new boolean[]{true, true, true, true, true, false})
                .build();
        pvTime.show();
    }

    /**
     * 条件选择
     */
    private void chooseOptions() {
        int option1 = 0;
        int option2 = 0;
        String text = clickText.getText().toString();
        if (!TextUtils.isEmpty(text)) {
            String[] nums = text.split(".");
            if (nums != null && nums.length == 2) {
                option1 = TurnsUtils.getInt(nums[0], 0);
                option2 = TurnsUtils.getInt(nums[1], 0);
            }
        }
        //添加数据源
        final List<String> options1Items = new ArrayList<>();
        final List<String> options2Items = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            if (i < 10) {
                options2Items.add("0" + i);
            } else {
                options2Items.add("" + i);
            }
        }
        for (int i = 0; i < 200; i++) {
            options1Items.add("" + i);
        }
        OptionsPickerView pvOptions = new OptionsPickerView.Builder(this, new OptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int options2, int options3, View v) {
                //返回的分别是三个级别的选中位置
                clickText.setText(options1Items.get(options1) + "." + options2Items.get(options2));
            }
        })
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(false)
                //设置选择的三级单位
                .setLabels(".", "", "")
                //循环与否
                .setCyclic(false, false, false)
                //设置默认选中项
                .setSelectOptions(option1, option2, 0)
                //点击外部dismiss default true
                .setOutSideCancelable(true)
                //是否显示为对话框样式
                .isDialog(true)
                .build();
        pvOptions.setNPicker(options1Items, options2Items, null);
        pvOptions.show();
    }

    /**
     * 读取数据
     */
    private void readData() {
        biLeiJiLuModelList = DataSupport.findAll(BiLeiJiLuModel.class);
        if (biLeiJiLuModelList != null && biLeiJiLuModelList.size() > row) {
            row = biLeiJiLuModelList.size();
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        if (biLeiJiLuModelList != null && biLeiJiLuModelList.size() > 0) {
            for (int i = 0, plCh = biLeiJiLuModelList.size(); i < plCh; i++) {
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                BiLeiJiLuModel model = biLeiJiLuModelList.get(i);
                if (i == 0) {
                    zhanDrawTextView.setText(model.getBdz());
                    timeDrawTextView.setText(model.getJcsj());
                    renDrawTextView.setText(model.getJcr());
                    qingkDrawEditText.setText(model.getJcqk());
                }
                ((DrawEditText) linearLayout.getChildAt(1)).setText(model.getBdzdy());
                ((DrawEditText) linearLayout.getChildAt(2)).setText(model.getSbmc());
                ((DrawEditText) linearLayout.getChildAt(3)).setText(model.getDydj());
                ((DrawTextView) linearLayout.getChildAt(4)).setText(model.getXldl());
                ((DrawTextView) linearLayout.getChildAt(5)).setText(model.getXldlzdz());
                ((DrawTextView) linearLayout.getChildAt(6)).setText(model.getScxldlz());
                ((DrawTextView) linearLayout.getChildAt(7)).setText(model.getSczsz());
                ((DrawTextView) linearLayout.getChildAt(8)).setText(model.getZsz());
                ((DrawTextView) linearLayout.getChildAt(9)).setText(model.getDzcs());
                ((DrawTextView) linearLayout.getChildAt(10)).setText(model.getScdzcs());
                ((DrawTextView) linearLayout.getChildAt(11)).setText(model.getLjdzcs());
                ((DrawEditText) linearLayout.getChildAt(12)).setText(model.getDzqk());
                ((DrawEditText) linearLayout.getChildAt(13)).setText(model.getSjlx());
                ((DrawTextView) linearLayout.getChildAt(14)).setText(model.getBpzdz());
            }
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        DataSupport.deleteAll(BiLeiJiLuModel.class);
        List<BiLeiJiLuModel> biLeiJiLuModelList = new ArrayList<>();
        for (int i = 0, plCh = pLayout.getChildCount(); i < plCh; i++) {
            if (pLayout.getChildAt(i) instanceof LinearLayout) {
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                DrawEditText drawTextView = (DrawEditText) linearLayout.getChildAt(1);
                if (!TextUtils.isEmpty(drawTextView.getText().toString())) {
                    BiLeiJiLuModel model = new BiLeiJiLuModel();
                    model.setBdz(zhanDrawTextView.getText().toString());
                    model.setJcsj(timeDrawTextView.getText().toString());
                    model.setJcr(renDrawTextView.getText().toString());
                    model.setJcqk(qingkDrawEditText.getText().toString());
                    model.setBdzdy(((DrawEditText) linearLayout.getChildAt(1)).getText().toString());
                    model.setSbmc(((DrawEditText) linearLayout.getChildAt(2)).getText().toString());
                    model.setDydj(((DrawEditText) linearLayout.getChildAt(3)).getText().toString());
                    model.setXldl(((DrawTextView) linearLayout.getChildAt(4)).getText().toString());
                    model.setXldlzdz(((DrawTextView) linearLayout.getChildAt(5)).getText().toString());
                    model.setScxldlz(((DrawTextView) linearLayout.getChildAt(6)).getText().toString());
                    model.setSczsz(((DrawTextView) linearLayout.getChildAt(7)).getText().toString());
                    model.setZsz(((DrawTextView) linearLayout.getChildAt(8)).getText().toString());
                    model.setDzcs(((DrawTextView) linearLayout.getChildAt(9)).getText().toString());
                    model.setScdzcs(((DrawTextView) linearLayout.getChildAt(10)).getText().toString());
                    model.setLjdzcs(((DrawTextView) linearLayout.getChildAt(11)).getText().toString());
                    model.setDzqk(((DrawEditText) linearLayout.getChildAt(12)).getText().toString());
                    model.setSjlx(((DrawEditText) linearLayout.getChildAt(13)).getText().toString());
                    model.setBpzdz(((DrawTextView) linearLayout.getChildAt(14)).getText().toString());
                    biLeiJiLuModelList.add(model);
                }
            }
        }
        DataSupport.saveAll(biLeiJiLuModelList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
