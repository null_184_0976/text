package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 设备缺陷记录
 *         Created by Administrator on 2018/3/20.
 */

public class YunWeiQueXianModel extends DataSupport {
    private int id;//主键id
    private String bdz;//变电站
    private String qxsb;//缺陷设备
    private String dydj;//电压等级（设备电压等级）
    private String sbscmc;//	设备双重名称
    private String qxxz;//缺陷性质（危急、严重、一般）
    private String fxfs;//发现方式（巡视、带电检测等）
    private String fxr;//发现人
    private String fxsj;//发现时间
    private String tbr;//填报人
    private String tbsj;//填报时间
    private String qxms;//缺陷描述
    private String dxtz;//典型特征（没有则空白）
    private String hbddqk;//	汇报调度情况（*时*分汇报值班调度员**，值班调度员命令**，无需汇报则空白）
    private String hbjkqk;//	汇报监控情况：（*时*分汇报当值监控员**，当值监控员命令**，无需汇报则空白）
    private String shyj;//审核意见
    private String shqxxz;//	审核缺陷性质
    private String shsj;//审核时间
    private String shr;//审核人
    private String sdyj;//审定意见
    private String sdqxxz;//	审定缺陷性质
    private String sdsj;//审定时间
    private String sdr;//审定人
    private String apyj;//安排意见
    private String apr;//安排人
    private String apsj;//安排时间
    private String qxbw;//缺陷部位（部件）
    private String clfa;//处理方案
    private String qxyy;//缺陷原因
    private String cljg;//处理结果
    private String zryy;//责任原因
    private String jsyy;//技术原因
    private String gzfzr;//	工作负责人
    private String yqyy;//延期原因
    private String xqry;//消缺人员
    private String xqsj;//消缺时间
    private String qxyscljg;//	缺陷验收处理结果
    private String ysyj;//验收意见
    private String qxyshbddqk;//		缺陷验收汇报调度情况：（*时*分汇报值班调度员**，无需汇报则空白）
    private String qxyshbjkqk;//	缺陷验收汇报监控情况：（*时*分汇报当值监控员**，无需汇报则空白）
    private String ysr;//验收人
    private String yssj;//验收时间
    private String bz;//备注

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBdz() {
        return bdz;
    }

    public void setBdz(String bdz) {
        this.bdz = bdz;
    }

    public String getQxsb() {
        return qxsb;
    }

    public void setQxsb(String qxsb) {
        this.qxsb = qxsb;
    }

    public String getDydj() {
        return dydj;
    }

    public void setDydj(String dydj) {
        this.dydj = dydj;
    }

    public String getSbscmc() {
        return sbscmc;
    }

    public void setSbscmc(String sbscmc) {
        this.sbscmc = sbscmc;
    }

    public String getQxxz() {
        return qxxz;
    }

    public void setQxxz(String qxxz) {
        this.qxxz = qxxz;
    }

    public String getFxfs() {
        return fxfs;
    }

    public void setFxfs(String fxfs) {
        this.fxfs = fxfs;
    }

    public String getFxr() {
        return fxr;
    }

    public void setFxr(String fxr) {
        this.fxr = fxr;
    }

    public String getFxsj() {
        return fxsj;
    }

    public void setFxsj(String fxsj) {
        this.fxsj = fxsj;
    }

    public String getTbr() {
        return tbr;
    }

    public void setTbr(String tbr) {
        this.tbr = tbr;
    }

    public String getTbsj() {
        return tbsj;
    }

    public void setTbsj(String tbsj) {
        this.tbsj = tbsj;
    }

    public String getQxms() {
        return qxms;
    }

    public void setQxms(String qxms) {
        this.qxms = qxms;
    }

    public String getDxtz() {
        return dxtz;
    }

    public void setDxtz(String dxtz) {
        this.dxtz = dxtz;
    }

    public String getHbddqk() {
        return hbddqk;
    }

    public void setHbddqk(String hbddqk) {
        this.hbddqk = hbddqk;
    }

    public String getHbjkqk() {
        return hbjkqk;
    }

    public void setHbjkqk(String hbjkqk) {
        this.hbjkqk = hbjkqk;
    }

    public String getShyj() {
        return shyj;
    }

    public void setShyj(String shyj) {
        this.shyj = shyj;
    }

    public String getShqxxz() {
        return shqxxz;
    }

    public void setShqxxz(String shqxxz) {
        this.shqxxz = shqxxz;
    }

    public String getShsj() {
        return shsj;
    }

    public void setShsj(String shsj) {
        this.shsj = shsj;
    }

    public String getShr() {
        return shr;
    }

    public void setShr(String shr) {
        this.shr = shr;
    }

    public String getSdyj() {
        return sdyj;
    }

    public void setSdyj(String sdyj) {
        this.sdyj = sdyj;
    }

    public String getSdqxxz() {
        return sdqxxz;
    }

    public void setSdqxxz(String sdqxxz) {
        this.sdqxxz = sdqxxz;
    }

    public String getSdsj() {
        return sdsj;
    }

    public void setSdsj(String sdsj) {
        this.sdsj = sdsj;
    }

    public String getSdr() {
        return sdr;
    }

    public void setSdr(String sdr) {
        this.sdr = sdr;
    }

    public String getApyj() {
        return apyj;
    }

    public void setApyj(String apyj) {
        this.apyj = apyj;
    }

    public String getApr() {
        return apr;
    }

    public void setApr(String apr) {
        this.apr = apr;
    }

    public String getApsj() {
        return apsj;
    }

    public void setApsj(String apsj) {
        this.apsj = apsj;
    }

    public String getQxbw() {
        return qxbw;
    }

    public void setQxbw(String qxbw) {
        this.qxbw = qxbw;
    }

    public String getClfa() {
        return clfa;
    }

    public void setClfa(String clfa) {
        this.clfa = clfa;
    }

    public String getQxyy() {
        return qxyy;
    }

    public void setQxyy(String qxyy) {
        this.qxyy = qxyy;
    }

    public String getCljg() {
        return cljg;
    }

    public void setCljg(String cljg) {
        this.cljg = cljg;
    }

    public String getZryy() {
        return zryy;
    }

    public void setZryy(String zryy) {
        this.zryy = zryy;
    }

    public String getJsyy() {
        return jsyy;
    }

    public void setJsyy(String jsyy) {
        this.jsyy = jsyy;
    }

    public String getGzfzr() {
        return gzfzr;
    }

    public void setGzfzr(String gzfzr) {
        this.gzfzr = gzfzr;
    }

    public String getYqyy() {
        return yqyy;
    }

    public void setYqyy(String yqyy) {
        this.yqyy = yqyy;
    }

    public String getXqry() {
        return xqry;
    }

    public void setXqry(String xqry) {
        this.xqry = xqry;
    }

    public String getXqsj() {
        return xqsj;
    }

    public void setXqsj(String xqsj) {
        this.xqsj = xqsj;
    }

    public String getQxyscljg() {
        return qxyscljg;
    }

    public void setQxyscljg(String qxyscljg) {
        this.qxyscljg = qxyscljg;
    }

    public String getYsyj() {
        return ysyj;
    }

    public void setYsyj(String ysyj) {
        this.ysyj = ysyj;
    }

    public String getQxyshbddqk() {
        return qxyshbddqk;
    }

    public void setQxyshbddqk(String qxyshbddqk) {
        this.qxyshbddqk = qxyshbddqk;
    }

    public String getQxyshbjkqk() {
        return qxyshbjkqk;
    }

    public void setQxyshbjkqk(String qxyshbjkqk) {
        this.qxyshbjkqk = qxyshbjkqk;
    }

    public String getYsr() {
        return ysr;
    }

    public void setYsr(String ysr) {
        this.ysr = ysr;
    }

    public String getYssj() {
        return yssj;
    }

    public void setYssj(String yssj) {
        this.yssj = yssj;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }
}
