package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 解锁钥匙使用记录
 *         Created by Administrator on 2018/3/17.
 */

public class YunWeiJieSuoModel extends DataSupport {
    private int id;//主键id
    private String bdzmc;//	变电站名称
    private String yc;//页次
    private String jslx;//解锁类型
    private String sysj;//使用时间
    private String cznr;//操作内容（工作、事故处理）
    private String jsgjmc;//	解锁工具名称（钥匙）
    private String js;//解锁（√）
    private String jsdx;//解锁对象
    private String jsdxhf;//	解锁对象恢复（√）
    private String jsdxhfr;//	解锁对象恢复人
    private String jsyy;//解锁原因
    private String sqry;//申请人员
    private String xcqrry;//	现场确认人员
    private String pzry;//批准人员
    private String jsgjhfsj;//	解锁工具恢复时间
    private String jsgjhfr;//	解锁工具恢复人

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBdzmc() {
        return bdzmc;
    }

    public void setBdzmc(String bdzmc) {
        this.bdzmc = bdzmc;
    }

    public String getYc() {
        return yc;
    }

    public void setYc(String yc) {
        this.yc = yc;
    }

    public String getJslx() {
        return jslx;
    }

    public void setJslx(String jslx) {
        this.jslx = jslx;
    }

    public String getSysj() {
        return sysj;
    }

    public void setSysj(String sysj) {
        this.sysj = sysj;
    }

    public String getCznr() {
        return cznr;
    }

    public void setCznr(String cznr) {
        this.cznr = cznr;
    }

    public String getJsgjmc() {
        return jsgjmc;
    }

    public void setJsgjmc(String jsgjmc) {
        this.jsgjmc = jsgjmc;
    }

    public String getJs() {
        return js;
    }

    public void setJs(String js) {
        this.js = js;
    }

    public String getJsdx() {
        return jsdx;
    }

    public void setJsdx(String jsdx) {
        this.jsdx = jsdx;
    }

    public String getJsdxhf() {
        return jsdxhf;
    }

    public void setJsdxhf(String jsdxhf) {
        this.jsdxhf = jsdxhf;
    }

    public String getJsdxhfr() {
        return jsdxhfr;
    }

    public void setJsdxhfr(String jsdxhfr) {
        this.jsdxhfr = jsdxhfr;
    }

    public String getJsyy() {
        return jsyy;
    }

    public void setJsyy(String jsyy) {
        this.jsyy = jsyy;
    }

    public String getSqry() {
        return sqry;
    }

    public void setSqry(String sqry) {
        this.sqry = sqry;
    }

    public String getXcqrry() {
        return xcqrry;
    }

    public void setXcqrry(String xcqrry) {
        this.xcqrry = xcqrry;
    }

    public String getPzry() {
        return pzry;
    }

    public void setPzry(String pzry) {
        this.pzry = pzry;
    }

    public String getJsgjhfsj() {
        return jsgjhfsj;
    }

    public void setJsgjhfsj(String jsgjhfsj) {
        this.jsgjhfsj = jsgjhfsj;
    }

    public String getJsgjhfr() {
        return jsgjhfr;
    }

    public void setJsgjhfr(String jsgjhfr) {
        this.jsgjhfr = jsgjhfr;
    }
}
