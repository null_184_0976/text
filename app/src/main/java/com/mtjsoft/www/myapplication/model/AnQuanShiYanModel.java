package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 安全工器具试验计划表
 *         Created by Administrator on 2018/3/11.
 */

public class AnQuanShiYanModel extends DataSupport {
    private int id;//主键id
    private String dw;//单位
    private String bdzmc;//	变电站名称
    private String gqjmc;//	工器具名称
    private String bh;//编号
    private String ggxh;//规格型号
    private String ccrq;//出厂日期
    private String scsyrq;//	上次试验日期
    private String syzq;//试验周期
    private String xcsyrq;//	下次试验日期

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDw() {
        return dw;
    }

    public void setDw(String dw) {
        this.dw = dw;
    }

    public String getBdzmc() {
        return bdzmc;
    }

    public void setBdzmc(String bdzmc) {
        this.bdzmc = bdzmc;
    }

    public String getGqjmc() {
        return gqjmc;
    }

    public void setGqjmc(String gqjmc) {
        this.gqjmc = gqjmc;
    }

    public String getBh() {
        return bh;
    }

    public void setBh(String bh) {
        this.bh = bh;
    }

    public String getGgxh() {
        return ggxh;
    }

    public void setGgxh(String ggxh) {
        this.ggxh = ggxh;
    }

    public String getCcrq() {
        return ccrq;
    }

    public void setCcrq(String ccrq) {
        this.ccrq = ccrq;
    }

    public String getScsyrq() {
        return scsyrq;
    }

    public void setScsyrq(String scsyrq) {
        this.scsyrq = scsyrq;
    }

    public String getSyzq() {
        return syzq;
    }

    public void setSyzq(String syzq) {
        this.syzq = syzq;
    }

    public String getXcsyrq() {
        return xcsyrq;
    }

    public void setXcsyrq(String xcsyrq) {
        this.xcsyrq = xcsyrq;
    }
}
