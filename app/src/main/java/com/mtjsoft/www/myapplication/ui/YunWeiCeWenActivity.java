package com.mtjsoft.www.myapplication.ui;

import android.content.Intent;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.YunWeiCeWenDModel;
import com.mtjsoft.www.myapplication.model.YunWeiCeWenModel;
import com.mtjsoft.www.myapplication.utils.HHDensityUtils;
import com.mtjsoft.www.myapplication.utils.HHInputUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mtj 设备测温记录
 *         Created by Administrator on 2018/3/11.
 */

public class YunWeiCeWenActivity extends BaseActivity {

    private static final int CHOOSEBIANDIAN = 1001;
    private static final int CHOOSEGJ = 1002;
    private static final int CHOOSENAME = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private List<DrawTextView> drawTextViewList = new ArrayList<>();
    private List<DrawEditText> drawEditTextList = new ArrayList<>();
    private List<DrawEditText> buWeiDrawEditTexts = new ArrayList<>();
    private List<List<DrawEditText>> wenduDrawEditTexts = new ArrayList<>();
    private YunWeiCeWenModel yunWeiCeWenModel;
    private List<YunWeiCeWenDModel> yunWeiCeWenDModelList;

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setOnClickListener(this);
        readData();
        setTopName("设备测温记录");
        addCenterView("变电站", true, "测温时间", true);
        addCenterView("是否是全站（区域）测温", true);
        addCenterView("测温类型", false, "环境温度（℃）", false);
        addCenterView("湿度", false, "设备名称", false);
        addCenterView("间隔单元", false, "风速m/s", false);
        addCenterView("测温人：", true, "测温仪器（型号、编号）", false);
        addBottomLayout("测温范围");
        addBottomLayout("测温位置");
        addBottomLayout("发现问题");
        addBottomLayout("备注");
        DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setText("注：本记录在全站设备普测时可只使用一张，不需填写设备测温位置及A、B、C相温度、额定电流及负荷电流栏；当设备精密测温或测温异常时需要填全各信息。");
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        drawTextView.setLayoutParams(layoutParams);
        addBottomLayoutView(drawTextView);
        if (yunWeiCeWenDModelList != null && yunWeiCeWenDModelList.size() > 0) {
            addBuWeiView(yunWeiCeWenDModelList.size());
        } else {
            addBuWeiView(1);
        }
        setData();
    }

    /**
     * 添加测温部位
     */
    private void addBuWeiView(int row) {
        for (int j = 0; j < row; j++) {
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            for (int i = 0; i < 2; i++) {
                DrawTextView drawTextView = null;
                DrawEditText drawEditText = null;
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                switch (i) {
                    case 0:
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setText("发热部位" + (buWeiDrawEditTexts.size() + 1) + "：");
                        drawTextView.setLayoutParams(layoutParams);
                        view.addView(drawTextView);
                        break;
                    case 1:
                        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 3);
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        buWeiDrawEditTexts.add(drawEditText);
                        view.addView(drawEditText);
                        break;
                    default:
                        break;
                }
            }
            pLayout.addView(view);
            view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            String[] titles = {"A相温度（℃）", "", "B相温度（℃）", "", "C相温度（℃）", "", "额定电流（A）", "", "负荷电流（A）", ""};
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            List<DrawEditText> list = new ArrayList<>();
            for (int i = 0; i < titles.length; i++) {
                DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                drawEditText.setText(titles[i]);
                drawEditText.setLayoutParams(layoutParams);
                if ((i + 1) % 2 == 1) {
                    drawEditText.setEnabled(false);
                } else {
                    list.add(drawEditText);
                }
                view.addView(drawEditText);
            }
            wenduDrawEditTexts.add(list);
            pLayout.addView(view);
        }
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(String leftName, boolean leftIsText, String rightName, boolean rightIsText) {
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < 4; i++) {
            DrawTextView drawTextView = null;
            DrawEditText drawEditText = null;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            switch (i) {
                case 0:
                    drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(leftName);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                    break;
                case 1:
                    if (leftIsText) {
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setLayoutParams(layoutParams);
                        drawTextView.setOnClickListener(this);
                        drawTextView.setId(drawTextViewList.size());
                        drawTextViewList.add(drawTextView);
                        view.addView(drawTextView);
                    } else {
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        drawEditTextList.add(drawEditText);
                        view.addView(drawEditText);
                    }
                    break;
                case 2:
                    drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(rightName);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                    break;
                case 3:
                    if (rightIsText) {
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setLayoutParams(layoutParams);
                        drawTextView.setOnClickListener(this);
                        drawTextView.setId(drawTextViewList.size());
                        drawTextViewList.add(drawTextView);
                        view.addView(drawTextView);
                    } else {
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        drawEditTextList.add(drawEditText);
                        view.addView(drawEditText);
                    }
                    break;
                default:
                    break;
            }
        }
        pLayout.addView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(String leftName, boolean leftIsText) {
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < 2; i++) {
            DrawTextView drawTextView = null;
            DrawEditText drawEditText = null;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            switch (i) {
                case 0:
                    drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(leftName);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                    break;
                case 1:
                    if (leftIsText) {
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setLayoutParams(layoutParams);
                        drawTextView.setOnClickListener(this);
                        drawTextView.setId(drawTextViewList.size());
                        drawTextViewList.add(drawTextView);
                        view.addView(drawTextView);
                    } else {
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        drawEditTextList.add(drawEditText);
                        view.addView(drawEditText);
                    }
                    break;
                default:
                    break;
            }
        }
        pLayout.addView(view);
    }

    private void addBottomLayout(String bottomName) {
        DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText(bottomName);
        drawTextView.setPadding(16, 0, 0, 0);
        drawTextView.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
        pLayout.addView(drawTextView);
        //
        DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
        drawEditText.setMinHeight(HHDensityUtils.dip2px(getBaseContext(), 200));
        drawEditText.setLayoutParams(layoutParams);
        drawEditTextList.add(drawEditText);
        pLayout.addView(drawEditText);
    }


    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        Intent intent = null;
        switch (v.getId()) {
            case R.id.iv_add_row:
                addBuWeiView(1);
                break;
            case 0:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 1);
                startActivityForResult(intent, CHOOSEBIANDIAN);
                break;
            case 1:
                HHInputUtils.chooseTime(true, clickText);
                break;
            case 2:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 6);
                startActivityForResult(intent, CHOOSEBIANDIAN);
                break;
            case 3:
                intent = new Intent(this, LineLockActivity.class);
                intent.putExtra("isChoose", true);
                startActivityForResult(intent, CHOOSENAME);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSENAME:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 读取数据
     */
    private void readData() {
        yunWeiCeWenModel = DataSupport.findLast(YunWeiCeWenModel.class);
        if (yunWeiCeWenModel != null) {
            yunWeiCeWenDModelList = DataSupport.where("sbcw_id = ?", yunWeiCeWenModel.getId() + "").find(YunWeiCeWenDModel.class);
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        if (yunWeiCeWenModel != null) {
            drawEditTextList.get(0).setText(yunWeiCeWenModel.getCwlx());
            drawEditTextList.get(1).setText(yunWeiCeWenModel.getHjwd());
            drawEditTextList.get(2).setText(yunWeiCeWenModel.getSd());
            drawEditTextList.get(3).setText(yunWeiCeWenModel.getSbmc());
            drawEditTextList.get(4).setText(yunWeiCeWenModel.getJgdy());
            drawEditTextList.get(5).setText(yunWeiCeWenModel.getFs());
            drawEditTextList.get(6).setText(yunWeiCeWenModel.getCwyq());
            drawEditTextList.get(7).setText(yunWeiCeWenModel.getCwfw());
            drawEditTextList.get(8).setText(yunWeiCeWenModel.getCwwz());
            drawEditTextList.get(9).setText(yunWeiCeWenModel.getFxwt());
            drawEditTextList.get(10).setText(yunWeiCeWenModel.getBz());
            //
            drawTextViewList.get(0).setText(yunWeiCeWenModel.getBdz());
            drawTextViewList.get(1).setText(yunWeiCeWenModel.getCwsj());
            drawTextViewList.get(2).setText(yunWeiCeWenModel.getSfsqzcw());
            drawTextViewList.get(3).setText(yunWeiCeWenModel.getCwr());
        }
        if (yunWeiCeWenDModelList != null && yunWeiCeWenDModelList.size() > 0) {
            for (int i = 0; i < yunWeiCeWenDModelList.size(); i++) {
                YunWeiCeWenDModel model = yunWeiCeWenDModelList.get(i);
                if (model != null) {
                    buWeiDrawEditTexts.get(i).setText(model.getFrbw());
                    wenduDrawEditTexts.get(i).get(0).setText(model.getAxwd());
                    wenduDrawEditTexts.get(i).get(1).setText(model.getBxwd());
                    wenduDrawEditTexts.get(i).get(2).setText(model.getCxwd());
                    wenduDrawEditTexts.get(i).get(3).setText(model.getEddl());
                    wenduDrawEditTexts.get(i).get(4).setText(model.getFhdl());
                }
            }
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        YunWeiCeWenModel yunWeiCeWenModel = new YunWeiCeWenModel();
        yunWeiCeWenModel.setCwlx(drawEditTextList.get(0).getText().toString());
        yunWeiCeWenModel.setHjwd(drawEditTextList.get(1).getText().toString());
        yunWeiCeWenModel.setSd(drawEditTextList.get(2).getText().toString());
        yunWeiCeWenModel.setSbmc(drawEditTextList.get(3).getText().toString());
        yunWeiCeWenModel.setJgdy(drawEditTextList.get(4).getText().toString());
        yunWeiCeWenModel.setFs(drawEditTextList.get(5).getText().toString());
        yunWeiCeWenModel.setCwyq(drawEditTextList.get(6).getText().toString());
        yunWeiCeWenModel.setCwfw(drawEditTextList.get(7).getText().toString());
        yunWeiCeWenModel.setCwwz(drawEditTextList.get(8).getText().toString());
        yunWeiCeWenModel.setFxwt(drawEditTextList.get(9).getText().toString());
        yunWeiCeWenModel.setBz(drawEditTextList.get(10).getText().toString());
        //
        yunWeiCeWenModel.setBdz(drawTextViewList.get(0).getText().toString());
        yunWeiCeWenModel.setCwsj(drawTextViewList.get(1).getText().toString());
        yunWeiCeWenModel.setSfsqzcw(drawTextViewList.get(2).getText().toString());
        yunWeiCeWenModel.setCwr(drawTextViewList.get(3).getText().toString());
        //
        boolean isSu = yunWeiCeWenModel.save();
        if (isSu) {
            List<YunWeiCeWenDModel> yunWeiCeWenDModelList = new ArrayList<>();
            for (int i = 0; i < buWeiDrawEditTexts.size(); i++) {
                YunWeiCeWenDModel model = new YunWeiCeWenDModel();
                model.setSbcw_id(yunWeiCeWenModel.getId() + "");
                if (!TextUtils.isEmpty(buWeiDrawEditTexts.get(i).getText().toString())) {
                    model.setFrbw(buWeiDrawEditTexts.get(i).getText().toString());
                    List<DrawEditText> list = wenduDrawEditTexts.get(i);
                    model.setAxwd(list.get(0).getText().toString());
                    model.setBxwd(list.get(1).getText().toString());
                    model.setCxwd(list.get(2).getText().toString());
                    model.setEddl(list.get(3).getText().toString());
                    model.setFhdl(list.get(4).getText().toString());
                    yunWeiCeWenDModelList.add(model);
                }
            }
            DataSupport.saveAll(yunWeiCeWenDModelList);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
