package com.mtjsoft.www.myapplication.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.adapter.ChooseBaseAdapter;
import com.mtjsoft.www.myapplication.model.ChooseModel;
import com.mtjsoft.www.myapplication.utils.HHDensityUtils;
import com.mtjsoft.www.myapplication.utils.HHScreenUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mtj 两级选择
 *         传入int type
 *         返回chooseName
 */
public class ChooseActivity extends AppCompatActivity implements View.OnClickListener {
    /**
     * 0:工作类别选择 1://变电站选择 2: 执行情况选择 3 ：专用工具/备用备件名称 4：数量选择 5：班组选择 6:是否 7:解锁类型
     * 8：星期
     */
    private int type = 0;
    //最底部布局
    private LinearLayout layout;
    private LinearLayout linearLayout;
    private ListView listView1;
    private ListView listView2;
    private int width = 0;
    private List<ChooseModel> list1 = new ArrayList<>();
    private List<ChooseModel> list2 = new ArrayList<>();
    private List<ChooseModel> tempList = new ArrayList<>();
    private ChooseBaseAdapter chooseBaseAdapter1;
    private ChooseBaseAdapter chooseBaseAdapter2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 隐藏标题栏
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_choose_doubel);
        type = getIntent().getIntExtra("type", 0);
        width = HHScreenUtils.getScreenWidth(getBaseContext());
        layout = findViewById(R.id.ll_choose_layout);
        layout.setOnClickListener(this);
        linearLayout = findViewById(R.id.ll_choose);
        listView1 = findViewById(R.id.lv_one);
        listView2 = findViewById(R.id.lv_two);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width / 2, width / 2);
        linearLayout.setLayoutParams(layoutParams);
        addShowData();
    }

    /**
     * 设置显示的
     */
    private void addShowData() {
        switch (type) {
            case 0:
                //0:工作类别选择
                String[] typesname = getResources().getStringArray(R.array.work_type);
                showOneList(typesname);
                break;
            case 1:
                //变电站选择
                String[] typesBian = getResources().getStringArray(R.array.yunxingNums);
                showOneList(typesBian);
                break;
            case 2:
                //2: 执行情况选择
                String[] typesZhiXing = getResources().getStringArray(R.array.zhixing_is);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width / 2, HHDensityUtils.dip2px(getBaseContext(), 45) * typesZhiXing.length);
                linearLayout.setLayoutParams(layoutParams);
                showOneList(typesZhiXing);
                break;
            case 3:
                //专用工具/备用备件名称
                String[] typesGongJu = getResources().getStringArray(R.array.gongju_beijian);
                showOneList(typesGongJu);
                break;
            case 4:
                //数量选择
                int count = 500;
                String[] typesShuLiang = new String[count];
                for (int i = 0; i < count; i++) {
                    typesShuLiang[i] = i + "";
                }
                showOneList(typesShuLiang);
                break;
            case 5:
                //班组选择
                String[] typesBans = getResources().getStringArray(R.array.bans);
                showOneList(typesBans);
                break;
            case 6:
                String[] typesUse = getResources().getStringArray(R.array.is_use);
                LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(width / 2, HHDensityUtils.dip2px(getBaseContext(), 45) * typesUse.length);
                linearLayout.setLayoutParams(layoutParams2);
                showOneList(typesUse);
                break;
            case 7:
                String[] typesJiesuo = getResources().getStringArray(R.array.jiesuo_type);
                LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(width / 2, HHDensityUtils.dip2px(getBaseContext(), 45) * typesJiesuo.length);
                linearLayout.setLayoutParams(layoutParams3);
                showOneList(typesJiesuo);
                break;
            case 8:
                String[] typesXingQi = getResources().getStringArray(R.array.xingqi);
                showOneList(typesXingQi);
                break;
            default:
                break;
        }
    }

    /**
     * 显示一个列表
     *
     * @param strings
     */
    private void showOneList(String[] strings) {
        for (int i = 0; i < strings.length; i++) {
            ChooseModel model = new ChooseModel();
            model.setTiaojian(strings[i]);
            list1.add(model);
        }
        listView2.setVisibility(View.GONE);
        chooseBaseAdapter1 = new ChooseBaseAdapter(getBaseContext(), list1, false);
        listView1.setAdapter(chooseBaseAdapter1);
        listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.putExtra("chooseName", list1.get(position).getTiaojian());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_choose_layout:
                finish();
                break;
            default:
                break;
        }
    }
}
