package com.mtjsoft.www.myapplication.model;

import java.util.List;

/**
 * Created by Administrator on 2018/3/8.
 */

public class ChooseModel {
    private String tiaojian = "";
    private List<ChooseModel> listTwo;

    public String getTiaojian() {
        return tiaojian;
    }

    public void setTiaojian(String tiaojian) {
        this.tiaojian = tiaojian;
    }

    public List<ChooseModel> getListTwo() {
        return listTwo;
    }

    public void setListTwo(List<ChooseModel> listTwo) {
        this.listTwo = listTwo;
    }

}
