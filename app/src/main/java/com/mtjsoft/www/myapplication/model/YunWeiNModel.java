package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj  1.运维年计划表
 * Created by Administrator on 2018/3/9.
 */

public class YunWeiNModel extends DataSupport {
    private int id;//主键id
    private String bzmc;//	班组名称
    private String nf;//年份
    private String rq;//日期
    private String bdz;//	变电站
    private String gzlb;//		工作类别
    private String gznr;//		工作内容
    private String zrr;//	责任人
    private String zxqk;//		执行情况

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBzmc() {
        return bzmc;
    }

    public void setBzmc(String bzmc) {
        this.bzmc = bzmc;
    }

    public String getNf() {
        return nf;
    }

    public void setNf(String nf) {
        this.nf = nf;
    }

    public String getRq() {
        return rq;
    }

    public void setRq(String rq) {
        this.rq = rq;
    }

    public String getBdz() {
        return bdz;
    }

    public void setBdz(String bdz) {
        this.bdz = bdz;
    }

    public String getGzlb() {
        return gzlb;
    }

    public void setGzlb(String gzlb) {
        this.gzlb = gzlb;
    }

    public String getGznr() {
        return gznr;
    }

    public void setGznr(String gznr) {
        this.gznr = gznr;
    }

    public String getZrr() {
        return zrr;
    }

    public void setZrr(String zrr) {
        this.zrr = zrr;
    }

    public String getZxqk() {
        return zxqk;
    }

    public void setZxqk(String zxqk) {
        this.zxqk = zxqk;
    }
}
