package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 生产用车使用记录
 *         Created by Administrator on 2018/3/10.
 */

public class ShengChanYCModel extends DataSupport {
    private int id;//主键id
    private String cph;//车牌号
    private String ccsj;//	出车时间
    private String ccyt;//	出车用途
    private String jsyqz;//		驾驶员签字
    private String cfgls;//		出发公里数
    private String fhgls;//		返回公里数
    private String fhsj;//	返回时间

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCph() {
        return cph;
    }

    public void setCph(String cph) {
        this.cph = cph;
    }

    public String getCcsj() {
        return ccsj;
    }

    public void setCcsj(String ccsj) {
        this.ccsj = ccsj;
    }

    public String getCcyt() {
        return ccyt;
    }

    public void setCcyt(String ccyt) {
        this.ccyt = ccyt;
    }

    public String getJsyqz() {
        return jsyqz;
    }

    public void setJsyqz(String jsyqz) {
        this.jsyqz = jsyqz;
    }

    public String getCfgls() {
        return cfgls;
    }

    public void setCfgls(String cfgls) {
        this.cfgls = cfgls;
    }

    public String getFhgls() {
        return fhgls;
    }

    public void setFhgls(String fhgls) {
        this.fhgls = fhgls;
    }

    public String getFhsj() {
        return fhsj;
    }

    public void setFhsj(String fhsj) {
        this.fhsj = fhsj;
    }
}
