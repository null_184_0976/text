package com.mtjsoft.www.myapplication.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.YunWeiXunShiModel;
import com.mtjsoft.www.myapplication.utils.HHDensityUtils;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mtj 设备巡视记录
 *         Created by Administrator on 2018/3/11.
 */

public class YunWeiXunShiJiLuActivity extends BaseActivity {

    private static final int CHOOSEBIANDIAN = 1001;
    private static final int CHOOSEGJ = 1002;
    private static final int CHOOSENAME = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private List<DrawTextView> drawTextViewList = new ArrayList<>();
    private List<DrawEditText> drawEditTextList = new ArrayList<>();

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setVisibility(View.GONE);
        setTopName("设备巡视记录");
        addCenterView("变电站", true, "电压等级", false);
        addCenterView("巡视日期", true, "变电站类别", false);
        addCenterView("巡视类型", false, "天气", false);
        addCenterView("气温（℃）", false, "巡视班组", true);
        addCenterView("巡视人", true, "是否使用巡检仪巡视", true);
        addCenterView("巡视开始时间", true, "巡视结束时间", true);
        addBottomLayout("巡视内容");
        addBottomLayout("巡视结果");
        addBottomLayout("备注");
        setData();
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(String leftName, boolean leftIsText, String rightName, boolean rightIsText) {
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < 4; i++) {
            DrawTextView drawTextView = null;
            DrawEditText drawEditText = null;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            switch (i) {
                case 0:
                    drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(leftName);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                    break;
                case 1:
                    if (leftIsText) {
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setLayoutParams(layoutParams);
                        drawTextView.setOnClickListener(this);
                        drawTextView.setId(drawTextViewList.size());
                        drawTextViewList.add(drawTextView);
                        view.addView(drawTextView);
                    } else {
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        drawEditTextList.add(drawEditText);
                        view.addView(drawEditText);
                    }
                    break;
                case 2:
                    drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(rightName);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                    break;
                case 3:
                    if (rightIsText) {
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setLayoutParams(layoutParams);
                        drawTextView.setOnClickListener(this);
                        drawTextView.setId(drawTextViewList.size());
                        drawTextViewList.add(drawTextView);
                        view.addView(drawTextView);
                    } else {
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        drawEditTextList.add(drawEditText);
                        view.addView(drawEditText);
                    }
                    break;
                default:
                    break;
            }
        }
        pLayout.addView(view);
    }

    private void addBottomLayout(String bottomName) {
        DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText(bottomName);
        drawTextView.setPadding(16, 0, 0, 0);
        drawTextView.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
        pLayout.addView(drawTextView);
        //
        DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
        drawEditText.setMinHeight(HHDensityUtils.dip2px(getBaseContext(), 200));
        drawEditText.setLayoutParams(layoutParams);
        drawEditTextList.add(drawEditText);
        pLayout.addView(drawEditText);
    }


    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        Intent intent = null;
        switch (v.getId()) {
            case 0:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 1);
                startActivityForResult(intent, CHOOSEBIANDIAN);
                break;
            case 1:
                chooseTime(false);
                break;
            case 2:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 5);
                startActivityForResult(intent, CHOOSEBIANDIAN);
                break;
            case 3:
                intent = new Intent(this, LineLockActivity.class);
                intent.putExtra("isChoose", true);
                startActivityForResult(intent, CHOOSENAME);
                break;
            case 4:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 6);
                startActivityForResult(intent, CHOOSEBIANDIAN);
                break;
            case 5:
            case 6:
                chooseTime(true);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSENAME:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 选择日期
     */
    private void chooseTime(boolean isHHmm) {
        String format = "yyyy-MM-dd";
        boolean[] show = new boolean[]{true, true, true, false, false, false};
        if (isHHmm) {
            format = "yyyy-MM-dd HH:mm";
            show = new boolean[]{true, true, true, true, true, false};
        }
        //时间选择器
        final String finalFormat = format;
        TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                clickText.setText(TurnsUtils.convertToString(date,finalFormat));
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(show)
                .build();
        pvTime.show();
    }

    /**
     * 设置数据
     */
    private void setData() {
        YunWeiXunShiModel model = DataSupport.findLast(YunWeiXunShiModel.class);
        if (model != null) {
            drawEditTextList.get(0).setText(model.getDydj());
            drawEditTextList.get(1).setText(model.getBdzlb());
            drawEditTextList.get(2).setText(model.getXslx());
            drawEditTextList.get(3).setText(model.getTq());
            drawEditTextList.get(4).setText(model.getQw());
            drawEditTextList.get(5).setText(model.getXsnr());
            drawEditTextList.get(6).setText(model.getXsjg());
            drawEditTextList.get(7).setText(model.getBz());
            //
            drawTextViewList.get(0).setText(model.getBdz());
            drawTextViewList.get(1).setText(model.getXsrq());
            drawTextViewList.get(2).setText(model.getXsbz());
            drawTextViewList.get(3).setText(model.getXsr());
            drawTextViewList.get(4).setText(model.getSfsyxjyxs());
            drawTextViewList.get(5).setText(model.getXskssj());
            drawTextViewList.get(6).setText(model.getXsjssj());
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        YunWeiXunShiModel model = new YunWeiXunShiModel();
        model.setDydj(drawEditTextList.get(0).getText().toString());
        model.setBdzlb(drawEditTextList.get(1).getText().toString());
        model.setXslx(drawEditTextList.get(2).getText().toString());
        model.setTq(drawEditTextList.get(3).getText().toString());
        model.setQw(drawEditTextList.get(4).getText().toString());
        model.setXsnr(drawEditTextList.get(5).getText().toString());
        model.setXsjg(drawEditTextList.get(6).getText().toString());
        model.setBz(drawEditTextList.get(7).getText().toString());
        //
        model.setBdz(drawTextViewList.get(0).getText().toString());
        model.setXsrq(drawTextViewList.get(1).getText().toString());
        model.setXsbz(drawTextViewList.get(2).getText().toString());
        model.setXsr(drawTextViewList.get(3).getText().toString());
        model.setSfsyxjyxs(drawTextViewList.get(4).getText().toString());
        model.setXskssj(drawTextViewList.get(5).getText().toString());
        model.setXsjssj(drawTextViewList.get(6).getText().toString());
        model.save();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
