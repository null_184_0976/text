package com.mtjsoft.www.myapplication.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.WaiLaiJiLuModel;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mtj 变电站外来人员登记记录
 *         Created by Administrator on 2018/3/11.
 */

public class WaiLaiJiLuActivity extends BaseActivity {

    private static final int CHOOSEName = 1001;
    private static final int CHOOSESHU = 1002;
    private static final int CHOOSEBIANDIAN = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private int col = 0;
    private int row = 20;
    private List<WaiLaiJiLuModel> waiLaiJiLuModelList;
    private String zhan = "___";

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setOnClickListener(this);
        setTopName(String.format(getString(R.string.wailai_title), zhan));
        addTableTitle();
        readData();
        addCenterView(row);
        setData();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        removeTopLayoutViewAll();
        String[] titles = getResources().getStringArray(R.array.wailai_jilu);
        col = titles.length;
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < col; i++) {
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            if (i == 9) {
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
            }
            drawTextView.setLayoutParams(layoutParams);
            drawTextView.setText(titles[i]);
            view.addView(drawTextView);
        }
        addTopLayoutView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(int row) {
        for (int j = 0; j < row; j++) {
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            for (int i = 0; i < col; i++) {
                if (i == 9) {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                    drawEditText.setLayoutParams(layoutParams);
                    view.addView(drawEditText);
                } else if (i == 3 || i == 4 || i == 5 || i == 6 || i == 10) {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                    drawEditText.setLayoutParams(layoutParams);
                    view.addView(drawEditText);
                } else {
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                    drawTextView.setTag(i);
                    drawTextView.setOnClickListener(this);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                }
            }
            pLayout.addView(view);
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        if (v.getId() == R.id.iv_add_row) {
            addCenterView(5);
            return;
        }
        if (v.getId() == R.id.tv_base_top_name) {
            Intent intent = new Intent(this, ChooseActivity.class);
            intent.putExtra("type", 1);
            startActivityForResult(intent, CHOOSEBIANDIAN);
            return;
        }
        Intent intent = null;
        int tag = (int) v.getTag();
        String format = null;
        switch (tag) {
            case 0:
                format = "MM-dd";
                chooseTime(format);
                break;
            case 1:
            case 2:
                format = "HH:mm";
                chooseTime(format);
                break;
            case 7:
                intent = new Intent(this, LineLockActivity.class);
                intent.putExtra("isChoose", true);
                startActivityForResult(intent, CHOOSEName);
                break;
            case 8:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 4);
                startActivityForResult(intent, CHOOSESHU);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            zhan = name;
                            clickText.setText(String.format(getString(R.string.wailai_title), zhan));
                        }
                    }
                    break;
                case CHOOSESHU:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name);
                        }
                    }
                    break;
                case CHOOSEName:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 选择日期
     */
    private void chooseTime(final String format) {
        boolean[] show = new boolean[]{false, false, false, true, true, false};
        if (clickText != null && (int) clickText.getTag() == 0) {
            show = new boolean[]{false, true, true, false, false, false};
        }
        //时间选择器
        TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                if (clickText != null) {
                    clickText.setText(TurnsUtils.convertToString(date,format));
                }
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(show)
                .build();
        pvTime.show();
    }

    /**
     * 读取数据
     */
    private void readData() {
        waiLaiJiLuModelList = DataSupport.findAll(WaiLaiJiLuModel.class);
        if (waiLaiJiLuModelList != null && waiLaiJiLuModelList.size() > row) {
            row = waiLaiJiLuModelList.size();
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        if (waiLaiJiLuModelList != null && waiLaiJiLuModelList.size() > 0) {
            for (int i = 0, plCh = waiLaiJiLuModelList.size(); i < plCh; i++) {
                WaiLaiJiLuModel model = waiLaiJiLuModelList.get(i);
                zhan = model.getBdzmc();
                setTopName(String.format(getString(R.string.wailai_title), zhan));
                if (pLayout.getChildAt(i) instanceof LinearLayout) {
                    LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                    ((DrawTextView) linearLayout.getChildAt(0)).setText(model.getRq());
                    ((DrawTextView) linearLayout.getChildAt(1)).setText(model.getJzsj());
                    ((DrawTextView) linearLayout.getChildAt(2)).setText(model.getLzsj());
                    ((DrawEditText) linearLayout.getChildAt(3)).setText(model.getJzsx());
                    ((DrawEditText) linearLayout.getChildAt(4)).setText(model.getJzyxr());
                    ((DrawEditText) linearLayout.getChildAt(5)).setText(model.getCph());
                    ((DrawEditText) linearLayout.getChildAt(6)).setText(model.getWlryszdw());
                    ((DrawTextView) linearLayout.getChildAt(7)).setText(model.getWlryfzr());
                    ((DrawTextView) linearLayout.getChildAt(8)).setText(model.getRs());
                    ((DrawEditText) linearLayout.getChildAt(9)).setText(model.getGznr());
                    ((DrawEditText) linearLayout.getChildAt(10)).setText(model.getBz());
                }
            }
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        DataSupport.deleteAll(WaiLaiJiLuModel.class);
        List<WaiLaiJiLuModel> waiLaiJiLuModelList = new ArrayList<>();
        for (int i = 0, plCh = pLayout.getChildCount(); i < plCh; i++) {
            WaiLaiJiLuModel model = new WaiLaiJiLuModel();
            model.setBdzmc(zhan);
            if (pLayout.getChildAt(i) instanceof LinearLayout) {
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                DrawTextView drawTextView = (DrawTextView) linearLayout.getChildAt(0);
                if (!TextUtils.isEmpty(drawTextView.getText().toString())) {
                    model.setRq(drawTextView.getText().toString());
                    model.setJzsj(((DrawTextView) linearLayout.getChildAt(1)).getText().toString());
                    model.setLzsj(((DrawTextView) linearLayout.getChildAt(2)).getText().toString());
                    model.setJzsx(((DrawEditText) linearLayout.getChildAt(3)).getText().toString());
                    model.setJzyxr(((DrawEditText) linearLayout.getChildAt(4)).getText().toString());
                    model.setCph(((DrawEditText) linearLayout.getChildAt(5)).getText().toString());
                    model.setWlryszdw(((DrawEditText) linearLayout.getChildAt(6)).getText().toString());
                    model.setWlryfzr(((DrawTextView) linearLayout.getChildAt(7)).getText().toString());
                    model.setRs(((DrawTextView) linearLayout.getChildAt(8)).getText().toString());
                    model.setGznr(((DrawEditText) linearLayout.getChildAt(9)).getText().toString());
                    model.setBz(((DrawEditText) linearLayout.getChildAt(10)).getText().toString());
                    waiLaiJiLuModelList.add(model);
                }
            }
        }
        DataSupport.saveAll(waiLaiJiLuModelList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
