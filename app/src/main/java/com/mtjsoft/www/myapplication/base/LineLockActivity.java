package com.mtjsoft.www.myapplication.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.utils.HHFilesUtils;
import com.mtjsoft.www.myapplication.view.ContentView;
import com.mtjsoft.www.myapplication.view.Drawl;

import java.util.HashMap;
import java.util.Map;

/**
 * @author mtj 手势选人
 *         isChoose 是否是手势选人
 *         chooseNames 已选择的人名
 */
public class LineLockActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout linearLayout;
    private FrameLayout body_layout;
    private ContentView content;
    private LinearLayout hsclLinearLayout;
    private Button delButton;
    private Map<String, String> userMap = new HashMap<>();
    private StringBuffer stringBuffer = new StringBuffer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 隐藏标题栏
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_line_lock);
        linearLayout = findViewById(R.id.ll_lock_layout);
        linearLayout.setOnClickListener(this);
        hsclLinearLayout = findViewById(R.id.ll_hscl);
        delButton = findViewById(R.id.btu_name_del_sure);
        delButton.setOnClickListener(this);
        body_layout = findViewById(R.id.body_layout);

        System.out.println("测试下码云");
        System.out.println("再次测试下码云的上传");
        //读取手势用户配置文件
        String userNameResult = HHFilesUtils.readAssetsTxt(getBaseContext(), "userTable");
        if (!TextUtils.isEmpty(userNameResult)) {
            String[] usersList = userNameResult.split("@");
            if (usersList != null && usersList.length > 0) {
                for (int i = 0; i < usersList.length; i++) {
                    String[] user = usersList[i].split(",");
                    if (user != null && user.length == 2) {
                        userMap.put(user[0], user[1]);
                    }
                }
            }
        }
        // 初始化一个显示各个点的viewGroup
        addLockContentView();
        //显示传过来的名字
        addChooseName();
    }

    private void addChooseName() {
        String name = getIntent().getStringExtra("chooseNames");
        if (!TextUtils.isEmpty(name)) {
            String[] names = name.split(",");
            for (int i = 0; i < names.length; i++) {
                View view = View.inflate(getBaseContext(), R.layout.include_choosename_bottom_textview, null);
                TextView tv = view.findViewById(R.id.tv_include);
                tv.setText(names[i]);
                view.setTag(names[i]);
                view.setOnClickListener(this);
                hsclLinearLayout.addView(view);
            }
            delButton.setVisibility(View.VISIBLE);
        } else {
            delButton.setVisibility(View.GONE);
        }
    }

    /**
     * 初始化一个显示各个点的viewGroup
     */
    private void addLockContentView() {
        content = new ContentView(this, "", 0, new Drawl.GestureCallBack() {
            @Override
            public void checkedSuccess(String psw) {
                String name = userMap.get(psw);
                if (TextUtils.isEmpty(name)) {
                    Toast.makeText(getBaseContext(), getString(R.string.user_not), Toast.LENGTH_SHORT)
                            .show();
                } else if (getIntent().getBooleanExtra("isChoose", false)) {
                    getHsclNames();
                    if (!TextUtils.isEmpty(stringBuffer.toString()) && stringBuffer.toString().contains(name)) {
                        Toast.makeText(getBaseContext(), getString(R.string.user_have), Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        Intent intent = new Intent();
                        intent.putExtra("chooseName", stringBuffer.toString() + name + ",");
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                }
            }

            @Override
            public void checkedFail() {
            }

            @Override
            public void minLenghtFail() {
            }
        });
        // 设置手势解锁显示到哪个布局里面
        content.setParentView(body_layout);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_lock_layout:
                finish();
                break;
            case R.id.btu_name_del_sure:
                getHsclNames();
                Intent intent = new Intent();
                intent.putExtra("chooseName", stringBuffer.toString());
                setResult(RESULT_OK, intent);
                finish();
                break;
            default:
                hsclLinearLayout.removeView(v);
                break;
        }
    }

    private void getHsclNames() {
        stringBuffer = new StringBuffer();
        if (hsclLinearLayout.getChildCount() > 0) {
            for (int i = 0; i < hsclLinearLayout.getChildCount(); i++) {
                stringBuffer.append(hsclLinearLayout.getChildAt(i).getTag());
                stringBuffer.append(",");
            }
        }
    }
}
