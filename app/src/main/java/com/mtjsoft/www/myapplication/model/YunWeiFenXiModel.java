package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 运维分析记录
 *         Created by Administrator on 2018/3/16.
 */

public class YunWeiFenXiModel extends DataSupport {
    private int id;//主键id
    private String ywbz;//运维班站
    private String bc;//班次
    private String jlrq;//记录日期
    private String fxlb;//分析类别
    private String fxtm;//分析题目
    private String tcr;//提出人
    private String cjr;//参加人
    private String qxry;//缺席人员
    private String qxyy;//缺席原因
    private String fxnr;//分析内容
    private String csjzxqk;//	措施及执行情况
    private String jlr;//记录人
    private String jcr;//检查人
    private String bz;//备注

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getYwbz() {
        return ywbz;
    }

    public void setYwbz(String ywbz) {
        this.ywbz = ywbz;
    }

    public String getBc() {
        return bc;
    }

    public void setBc(String bc) {
        this.bc = bc;
    }

    public String getJlrq() {
        return jlrq;
    }

    public void setJlrq(String jlrq) {
        this.jlrq = jlrq;
    }

    public String getFxlb() {
        return fxlb;
    }

    public void setFxlb(String fxlb) {
        this.fxlb = fxlb;
    }

    public String getFxtm() {
        return fxtm;
    }

    public void setFxtm(String fxtm) {
        this.fxtm = fxtm;
    }

    public String getTcr() {
        return tcr;
    }

    public void setTcr(String tcr) {
        this.tcr = tcr;
    }

    public String getCjr() {
        return cjr;
    }

    public void setCjr(String cjr) {
        this.cjr = cjr;
    }

    public String getQxry() {
        return qxry;
    }

    public void setQxry(String qxry) {
        this.qxry = qxry;
    }

    public String getQxyy() {
        return qxyy;
    }

    public void setQxyy(String qxyy) {
        this.qxyy = qxyy;
    }

    public String getFxnr() {
        return fxnr;
    }

    public void setFxnr(String fxnr) {
        this.fxnr = fxnr;
    }

    public String getCsjzxqk() {
        return csjzxqk;
    }

    public void setCsjzxqk(String csjzxqk) {
        this.csjzxqk = csjzxqk;
    }

    public String getJlr() {
        return jlr;
    }

    public void setJlr(String jlr) {
        this.jlr = jlr;
    }

    public String getJcr() {
        return jcr;
    }

    public void setJcr(String jcr) {
        this.jcr = jcr;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }
}
