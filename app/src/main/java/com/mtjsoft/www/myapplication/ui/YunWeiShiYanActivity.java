package com.mtjsoft.www.myapplication.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.YunWeiShiYanModel;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mtj 电气设备检修试验记录
 *         Created by Administrator on 2018/3/11.
 */

public class YunWeiShiYanActivity extends BaseActivity {

    private static final int CHOOSENAME = 1002;
    private static final int CHOOSEBIANDIAN = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private int col = 0;
    private int row = 20;
    private List<YunWeiShiYanModel> yunWeiShiYanModelList;
    //
    private DrawEditText dwEditText;
    private DrawTextView zhanTextView;

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setOnClickListener(this);
        setTopName("电气设备检修试验记录");
        addTableTitle();
        readData();
        addCenterView(row);
        setData();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        removeTopLayoutViewAll();
        String[] topsname = getResources().getStringArray(R.array.yunwei_shiyan_top);
        LinearLayout viewTop = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        for (int i = 0; i < 4; i++) {
            switch (i) {
                case 0:
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(topsname[0]);
                    drawTextView.setLayoutParams(layoutParams);
                    viewTop.addView(drawTextView);
                    break;
                case 1:
                    dwEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    dwEditText.setLayoutParams(layoutParams);
                    viewTop.addView(dwEditText);
                    break;
                case 2:
                    DrawTextView drawTextView2 = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView2.setText(topsname[1]);
                    drawTextView2.setLayoutParams(layoutParams);
                    viewTop.addView(drawTextView2);
                    break;
                case 3:
                    zhanTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    zhanTextView.setLayoutParams(layoutParams);
                    zhanTextView.setTag("chooseBDZ");
                    zhanTextView.setOnClickListener(this);
                    viewTop.addView(zhanTextView);
                    break;
                default:
                    break;
            }
        }
        addTopLayoutView(viewTop);
        //标题
        String[] titles = getResources().getStringArray(R.array.yunwei_shiyan);
        col = titles.length;
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < col; i++) {
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            if (i == 1 || i == 5 || i == 6) {
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
            } else {
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            }
            drawTextView.setLayoutParams(layoutParams);
            drawTextView.setText(titles[i]);
            view.addView(drawTextView);
        }
        addTopLayoutView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(int row) {
        for (int j = 0; j < row; j++) {
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            for (int i = 0; i < col; i++) {
                if (i == 0 || i == 7 || i == 8) {
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                    drawTextView.setTag(i);
                    drawTextView.setOnClickListener(this);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                } else if (i == 1 || i == 5 || i == 6) {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                    drawEditText.setLayoutParams(layoutParams);
                    view.addView(drawEditText);
                } else {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                    drawEditText.setLayoutParams(layoutParams);
                    view.addView(drawEditText);
                }
            }
            pLayout.addView(view);
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        if (v.getId() == R.id.iv_add_row) {
            addCenterView(5);
            return;
        }
        if ("chooseBDZ".equals(v.getTag())) {
            Intent intent = new Intent(this, ChooseActivity.class);
            intent.putExtra("type", 1);
            startActivityForResult(intent, CHOOSEBIANDIAN);
            return;
        }
        int tag = (int) v.getTag();
        switch (tag) {
            case 0:
                chooseTime();
                break;
            case 7:
            case 8:
                Intent intent = new Intent(this, LineLockActivity.class);
                intent.putExtra("isChoose", true);
                startActivityForResult(intent, CHOOSENAME);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name);
                        }
                    }
                    break;
                case CHOOSENAME:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",",""));
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 选择日期
     */
    private void chooseTime() {
        //时间选择器
        TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                clickText.setText(TurnsUtils.convertToString(date,"yyyy-MM-dd"));
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(new boolean[]{true, true, true, false, false, false})
                .build();
        pvTime.show();
    }

    /**
     * 读取数据
     */
    private void readData() {
        yunWeiShiYanModelList = DataSupport.findAll(YunWeiShiYanModel.class);
        if (yunWeiShiYanModelList != null && yunWeiShiYanModelList.size() > row) {
            row = yunWeiShiYanModelList.size();
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        if (yunWeiShiYanModelList != null && yunWeiShiYanModelList.size() > 0) {
            for (int i = 0, plCh = yunWeiShiYanModelList.size(); i < plCh; i++) {
                YunWeiShiYanModel model = yunWeiShiYanModelList.get(i);
                dwEditText.setText(model.getYc());
                zhanTextView.setText(model.getBdzmc());
                if (pLayout.getChildAt(i) instanceof LinearLayout) {
                    LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                    ((DrawTextView) linearLayout.getChildAt(0)).setText(model.getRq());
                    ((DrawEditText) linearLayout.getChildAt(1)).setText(model.getSbjgmc());
                    ((DrawEditText) linearLayout.getChildAt(2)).setText(model.getGzpbh());
                    ((DrawEditText) linearLayout.getChildAt(3)).setText(model.getGznr());
                    ((DrawEditText) linearLayout.getChildAt(4)).setText(model.getJxxz());
                    ((DrawEditText) linearLayout.getChildAt(5)).setText(model.getJxxmjczwt());
                    ((DrawEditText) linearLayout.getChildAt(6)).setText(model.getJl());
                    ((DrawTextView) linearLayout.getChildAt(7)).setText(model.getGzfzr());
                    ((DrawTextView) linearLayout.getChildAt(8)).setText(model.getYsryqz());
                }
            }
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        DataSupport.deleteAll(YunWeiShiYanModel.class);
        List<YunWeiShiYanModel> yunWeiShiYanModelList = new ArrayList<>();
        for (int i = 0, plCh = pLayout.getChildCount(); i < plCh; i++) {
            YunWeiShiYanModel model = new YunWeiShiYanModel();
            model.setBdzmc(zhanTextView.getText().toString());
            model.setYc(dwEditText.getText().toString().trim());
            if (pLayout.getChildAt(i) instanceof LinearLayout) {
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                DrawTextView drawTextView = (DrawTextView) linearLayout.getChildAt(0);
                if (!TextUtils.isEmpty(drawTextView.getText().toString())) {
                    model.setRq(drawTextView.getText().toString());
                    model.setSbjgmc(((DrawEditText) linearLayout.getChildAt(1)).getText().toString());
                    model.setGzpbh(((DrawEditText) linearLayout.getChildAt(2)).getText().toString());
                    model.setGznr(((DrawEditText) linearLayout.getChildAt(3)).getText().toString());
                    model.setJxxz(((DrawEditText) linearLayout.getChildAt(4)).getText().toString());
                    model.setJxxmjczwt(((DrawEditText) linearLayout.getChildAt(5)).getText().toString());
                    model.setJl(((DrawEditText) linearLayout.getChildAt(6)).getText().toString());
                    model.setGzfzr(((DrawTextView) linearLayout.getChildAt(7)).getText().toString());
                    model.setYsryqz(((DrawTextView) linearLayout.getChildAt(8)).getText().toString());
                    yunWeiShiYanModelList.add(model);
                }
            }
        }
        DataSupport.saveAll(yunWeiShiYanModelList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
