package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 防误装置检查记录
 *         Created by Administrator on 2018/3/10.
 */

public class FangWuJianChaModel extends DataSupport {
    private int id;//	主键id
    private String bdzmc;//	变电站名称
    private String mc;//	名称
    private String xh;//	型号
    private String sl;//	数量
    private String jcrq;//	检查日期
    private String jcxm;//	检查项目
    private String czwt;//	存在问题
    private String jcry;//	检查人员
    private String bz;//	备注

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBdzmc() {
        return bdzmc;
    }

    public void setBdzmc(String bdzmc) {
        this.bdzmc = bdzmc;
    }

    public String getMc() {
        return mc;
    }

    public void setMc(String mc) {
        this.mc = mc;
    }

    public String getXh() {
        return xh;
    }

    public void setXh(String xh) {
        this.xh = xh;
    }

    public String getSl() {
        return sl;
    }

    public void setSl(String sl) {
        this.sl = sl;
    }

    public String getJcrq() {
        return jcrq;
    }

    public void setJcrq(String jcrq) {
        this.jcrq = jcrq;
    }

    public String getJcxm() {
        return jcxm;
    }

    public void setJcxm(String jcxm) {
        this.jcxm = jcxm;
    }

    public String getCzwt() {
        return czwt;
    }

    public void setCzwt(String czwt) {
        this.czwt = czwt;
    }

    public String getJcry() {
        return jcry;
    }

    public void setJcry(String jcry) {
        this.jcry = jcry;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }
}
