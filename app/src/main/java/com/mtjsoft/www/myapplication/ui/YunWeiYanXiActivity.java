package com.mtjsoft.www.myapplication.ui;

import android.content.Intent;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.YunWeiYanXiModel;
import com.mtjsoft.www.myapplication.utils.HHDensityUtils;
import com.mtjsoft.www.myapplication.utils.HHInputUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mtj 反事故演习记录
 *         Created by Administrator on 2018/3/11.
 */

public class YunWeiYanXiActivity extends BaseActivity {

    private static final int CHOOSEBIANDIAN = 1001;
    private static final int CHOOSEGJ = 1002;
    private static final int CHOOSENAME = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private List<DrawTextView> drawTextViewList = new ArrayList<>();
    private List<DrawEditText> drawEditTextList = new ArrayList<>();

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setVisibility(View.GONE);
        setTopName("反事故演习记录");
        addCenterView("演习班组：", true, "演习变电站：", true);
        addCenterView("演习主持人：", true, "演习人员：", true);
        addCenterView("演习开始时间：", true,"演习结束时间：",true);
        addCenterView("天气：", false,"记录日期：",true);
        addBottomLayout("运行方式");
        addBottomLayout("演习题目");
        addBottomLayout("事故现象");
        addBottomLayout("处理经过");
        addBottomLayout("改进措施及评价");
        addBottomLayout("演习人员（签字）");
        addCenterView("主持人（签字）", true, "检查人（签字）", true);
        addBottomLayout("备注");
        setData();
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(String leftName, boolean leftIsText, String rightName, boolean rightIsText) {
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < 4; i++) {
            DrawTextView drawTextView = null;
            DrawEditText drawEditText = null;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            switch (i) {
                case 0:
                    drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(leftName);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                    break;
                case 1:
                    if (leftIsText) {
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setLayoutParams(layoutParams);
                        drawTextView.setOnClickListener(this);
                        drawTextView.setId(drawTextViewList.size());
                        drawTextViewList.add(drawTextView);
                        view.addView(drawTextView);
                    } else {
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        drawEditTextList.add(drawEditText);
                        view.addView(drawEditText);
                    }
                    break;
                case 2:
                    drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(rightName);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                    break;
                case 3:
                    if (rightIsText) {
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setLayoutParams(layoutParams);
                        drawTextView.setOnClickListener(this);
                        drawTextView.setId(drawTextViewList.size());
                        drawTextViewList.add(drawTextView);
                        view.addView(drawTextView);
                    } else {
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        drawEditTextList.add(drawEditText);
                        view.addView(drawEditText);
                    }
                    break;
                default:
                    break;
            }
        }
        pLayout.addView(view);
    }

    private void addBottomLayout(String bottomName) {
        DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText(bottomName);
        drawTextView.setPadding(16, 0, 0, 0);
        drawTextView.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
        pLayout.addView(drawTextView);
        //
        DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
        drawEditText.setMinHeight(HHDensityUtils.dip2px(getBaseContext(), 200));
        drawEditText.setLayoutParams(layoutParams);
        drawEditTextList.add(drawEditText);
        pLayout.addView(drawEditText);
    }


    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        Intent intent = null;
        switch (v.getId()) {
            case 0:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 5);
                startActivityForResult(intent, CHOOSEBIANDIAN);
                break;
            case 1:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 1);
                startActivityForResult(intent, CHOOSEBIANDIAN);
                break;
            case 4:
            case 5:
            case 6:
                HHInputUtils.chooseTime(true, clickText);
                break;
            case 2:
            case 3:
            case 7:
            case 8:
                intent = new Intent(this, LineLockActivity.class);
                intent.putExtra("isChoose", true);
                startActivityForResult(intent, CHOOSENAME);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSENAME:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        YunWeiYanXiModel model = DataSupport.findLast(YunWeiYanXiModel.class);
        if (model != null) {
            drawEditTextList.get(0).setText(model.getTq());
            drawEditTextList.get(1).setText(model.getYxfs());
            drawEditTextList.get(2).setText(model.getYxtm());
            drawEditTextList.get(3).setText(model.getSgxx());
            drawEditTextList.get(4).setText(model.getCljg());
            drawEditTextList.get(5).setText(model.getGjcsjpj());
            drawEditTextList.get(6).setText(model.getYxryqz());
            drawEditTextList.get(7).setText(model.getBz());
            //
            drawTextViewList.get(0).setText(model.getYxbz());
            drawTextViewList.get(1).setText(model.getYxbdz());
            drawTextViewList.get(2).setText(model.getYxzcr());
            drawTextViewList.get(3).setText(model.getYxry());
            drawTextViewList.get(4).setText(model.getYxkssj());
            drawTextViewList.get(5).setText(model.getYxjssj());
            drawTextViewList.get(6).setText(model.getJlrq());
            drawTextViewList.get(7).setText(model.getZcrqz());
            drawTextViewList.get(8).setText(model.getJcrqz());
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        YunWeiYanXiModel model = new YunWeiYanXiModel();
        model.setTq(drawEditTextList.get(0).getText().toString());
        model.setYxfs(drawEditTextList.get(1).getText().toString());
        model.setYxtm(drawEditTextList.get(2).getText().toString());
        model.setSgxx(drawEditTextList.get(3).getText().toString());
        model.setCljg(drawEditTextList.get(7).getText().toString());
        model.setGjcsjpj(drawEditTextList.get(5).getText().toString());
        model.setYxryqz(drawEditTextList.get(6).getText().toString());
        model.setBz(drawEditTextList.get(7).getText().toString());
        //
        model.setYxbz(drawTextViewList.get(0).getText().toString());
        model.setYxbdz(drawTextViewList.get(1).getText().toString());
        model.setYxzcr(drawTextViewList.get(2).getText().toString());
        model.setYxry(drawTextViewList.get(3).getText().toString());
        model.setYxkssj(drawTextViewList.get(4).getText().toString());
        model.setYxjssj(drawTextViewList.get(5).getText().toString());
        model.setJlrq(drawTextViewList.get(6).getText().toString());
        model.setZcrqz(drawTextViewList.get(7).getText().toString());
        model.setJcrqz(drawTextViewList.get(8).getText().toString());
        model.save();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
