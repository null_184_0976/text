package com.mtjsoft.www.myapplication.ui;

import android.content.Intent;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.BiaoZhunWeiHuModel;
import com.mtjsoft.www.myapplication.utils.HHDensityUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mtj 标准化作业卡
 *         Created by Administrator on 2018/3/11.
 */

public class BiaoZhunWeiHuActivity extends BaseActivity {

    private static final int CHOOSENAMES = 1001;
    private static final int CHOOSENAME = 1002;
    private static final int CHOOSEZHIXING = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private int row = 0;
    private List<BiaoZhunWeiHuModel> biaoZhunWeiHuModelList;
    private DrawEditText mcEditText;
    private DrawEditText timeEditText;
    private DrawEditText bhEditText;
    private DrawTextView gzryTextView;
    private DrawEditText pjEditText;
    private DrawTextView fzrTextView;

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setVisibility(View.GONE);
        setTopName("维护工作标准化作业卡");
        addTableTitle();
        readData();
        addCenterView();
        setData();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        removeTopLayoutViewAll();
        String[] titles = {"设备双重名称(1号主变压器)", "", "工作时间", "", "作业卡编号"};
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < 6; i++) {
            if ((i + 1) % 2 != 0) {
                DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                drawTextView.setLayoutParams(layoutParams);
                drawTextView.setText(titles[i]);
                view.addView(drawTextView);
            } else {
                DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                switch (i) {
                    case 1:
                        mcEditText = drawEditText;
                        break;
                    case 3:
                        timeEditText = drawEditText;
                        break;
                    case 5:
                        bhEditText = drawEditText;
                        break;
                    default:
                        break;
                }
                drawEditText.setLayoutParams(layoutParams);
                view.addView(drawEditText);
            }
        }
        addTopLayoutView(view);
        String[] tops = {"序号", "关键工序", "质量标准及要求", "风险辨识与预控措施", "执行情况"};
        LinearLayout viewTop = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < 5; i++) {
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 4);
            if (i == 0) {
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            } else if (i == 4) {
                layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
            }
            drawTextView.setLayoutParams(layoutParams);
            drawTextView.setText(tops[i]);
            viewTop.addView(drawTextView);
        }
        addTopLayoutView(viewTop);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView() {
        if (biaoZhunWeiHuModelList != null && biaoZhunWeiHuModelList.size() > 0) {
            for (int i = 0; i < row; i++) {
                LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
                for (int j = 0; j < 5; j++) {
                    //添加序号
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 4);
                    if (j == 0) {
                        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
                        drawTextView.setText((pLayout.getChildCount() + 1) + "");
                    } else if (j == 4) {
                        drawTextView.setOnClickListener(this);
                        drawTextView.setTag("chooseZhiXing");
                        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2);
                    }
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                }
                pLayout.addView(view);
            }
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            drawTextView.setText("工作人员签名");
            drawTextView.setLayoutParams(layoutParams);
            view.addView(drawTextView);
            drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 3);
            drawTextView.setLayoutParams(layoutParams);
            drawTextView.setOnClickListener(this);
            gzryTextView = drawTextView;
            drawTextView.setTag("CHOOSENAMEs");
            view.addView(drawTextView);
            pLayout.addView(view);
            addQianZiYiJian("执行评价：", "");
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        if ("chooseZhiXing".equals(v.getTag())) {
            Intent intent = new Intent(getBaseContext(), ChooseActivity.class);
            intent.putExtra("type", 2);
            startActivityForResult(intent, CHOOSEZHIXING);
            return;
        }
        if ("CHOOSENAMEs".equals(v.getTag())) {
            Intent intent = new Intent(getBaseContext(), LineLockActivity.class);
            intent.putExtra("isChoose", true);
            intent.putExtra("chooseNames", clickText.getText().toString());
            startActivityForResult(intent, CHOOSENAMES);
            return;
        }
        if ("CHOOSENAME".equals(v.getTag())) {
            Intent intent = new Intent(getBaseContext(), LineLockActivity.class);
            intent.putExtra("isChoose", true);
            startActivityForResult(intent, CHOOSENAME);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSEZHIXING:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name);
                        }
                    }
                    break;
                case CHOOSENAMES:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name);
                        }
                    }
                    break;
                case CHOOSENAME:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private void addQianZiYiJian(String title, String fuzeren) {
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setLineShow(true, true, false, false);
        drawTextView.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText(title);
        view.addView(drawTextView);
        DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
        drawEditText.setGravity(Gravity.TOP);
        drawEditText.setLineShow(false, true, true, false);
        drawEditText.setMinHeight(HHDensityUtils.dip2px(getBaseContext(), 100));
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 3);
        drawEditText.setLayoutParams(layoutParams);
        pjEditText = drawEditText;
        view.addView(drawEditText);
        pLayout.addView(view);
        //
        view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setLineShow(true, false, false, true);
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText(fuzeren);
        view.addView(drawTextView);
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setLineShow(false, false, false, true);
        drawTextView.setLayoutParams(layoutParams);
        view.addView(drawTextView);
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setLineShow(false, false, false, true);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText("工作负责人签名：");
        view.addView(drawTextView);
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setLineShow(false, false, true, true);
        drawTextView.setLayoutParams(layoutParams);
        view.addView(drawTextView);
        fzrTextView = drawTextView;
        drawTextView.setTag("CHOOSENAME");
        drawTextView.setOnClickListener(this);
        pLayout.addView(view);
    }

    /**
     * 读取数据
     */
    private void readData() {
        biaoZhunWeiHuModelList = DataSupport.findAll(BiaoZhunWeiHuModel.class);
        if (biaoZhunWeiHuModelList != null && biaoZhunWeiHuModelList.size() > row) {
            row = biaoZhunWeiHuModelList.size();
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        if (biaoZhunWeiHuModelList != null && biaoZhunWeiHuModelList.size() > 0) {
            for (int i = 0; i < biaoZhunWeiHuModelList.size(); i++) {
                BiaoZhunWeiHuModel model = biaoZhunWeiHuModelList.get(i);
                mcEditText.setText(model.getSbscmc());
                timeEditText.setText(model.getGzsj());
                bhEditText.setText(model.getZykbh());
                gzryTextView.setText(model.getGzryqm());
                pjEditText.setText(model.getZxpj());
                fzrTextView.setText(model.getGzfzrqm());
                LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                ((DrawTextView) linearLayout.getChildAt(1)).setText(model.getGjgx());
                ((DrawTextView) linearLayout.getChildAt(2)).setText(model.getZlbzjyq());
                ((DrawTextView) linearLayout.getChildAt(3)).setText(model.getFxbsykzcs());
                ((DrawTextView) linearLayout.getChildAt(4)).setText(model.getZxqk());
            }
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        DataSupport.deleteAll(BiaoZhunWeiHuModel.class);
        List<BiaoZhunWeiHuModel> biaoZhunWeiHuModelList = new ArrayList<>();
        for (int i = 0; i < row; i++) {
            LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
            BiaoZhunWeiHuModel model = new BiaoZhunWeiHuModel();
            model.setSbscmc(mcEditText.getText().toString());
            model.setGzsj(timeEditText.getText().toString());
            model.setZykbh(bhEditText.getText().toString());
            model.setGzryqm(gzryTextView.getText().toString());
            model.setZxpj(pjEditText.getText().toString());
            model.setGzfzrqm(fzrTextView.getText().toString());
            model.setGjgx(((DrawTextView) linearLayout.getChildAt(1)).getText().toString());
            model.setZlbzjyq(((DrawTextView) linearLayout.getChildAt(2)).getText().toString());
            model.setFxbsykzcs(((DrawTextView) linearLayout.getChildAt(3)).getText().toString());
            model.setZxqk(((DrawTextView) linearLayout.getChildAt(4)).getText().toString());
            biaoZhunWeiHuModelList.add(model);
        }
        DataSupport.saveAll(biaoZhunWeiHuModelList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
