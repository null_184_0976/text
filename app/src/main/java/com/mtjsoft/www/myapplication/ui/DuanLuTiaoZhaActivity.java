package com.mtjsoft.www.myapplication.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.DuanLuTiaoZhaModel;
import com.mtjsoft.www.myapplication.utils.TurnsUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mtj 断路器跳闸记录
 *         Created by Administrator on 2018/3/9.
 */

public class DuanLuTiaoZhaActivity extends BaseActivity {

    private static final int CHOOSEName = 1001;
    private static final int CHOOSEZM = 1002;
    private static final int CHOOSEBIANDIAN = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private int row = 20;
    private List<DuanLuTiaoZhaModel> duanLuTiaoZhaModelList;
    private List<List<DuanLuTiaoZhaModel>> listRowDatas = new ArrayList<>();
    private String zhan = "___";
    private DrawTextView zhanTextView;

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setOnClickListener(this);
        setTopName("断路器跳闸记录");
        addTableTitle();
        readData();
        addCenterView(row);
        setData();
    }

    /**
     * 添加表头
     */
    private void addTableTitle() {
        removeTopLayoutViewAll();
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText("变电站名称");
        view.addView(drawTextView);
        zhanTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 3);
        zhanTextView.setLayoutParams(layoutParams);
        zhanTextView.setOnClickListener(this);
        zhanTextView.setTag("chooseZhan");
        view.addView(zhanTextView);
        addTopLayoutView(view);
        //
        String[] titles = {"日期时间", "断路器编号", "保护动作情况", "断路器检查情况", "故障电流", "最后一次大修日期", "登记人"};
        view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0, psize = titles.length; i < psize; i++) {
            drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            drawTextView.setLayoutParams(layoutParams);
            drawTextView.setText(titles[i]);
            view.addView(drawTextView);
        }
        LinearLayout shuView = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_vertical_linearlayout_w_w, null);
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 6);
        shuView.setLayoutParams(layoutParams);
        //
        LinearLayout hengView = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 3);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText("本次跳闸次数");
        hengView.addView(drawTextView);
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 3);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText("跳闸累计次数");
        hengView.addView(drawTextView);
        shuView.addView(hengView);
        //
        hengView = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        String[] xiangS = {"A项", "B项", "C项", "A项", "B项", "C项"};
        for (int i = 0, size = xiangS.length; i < size; i++) {
            drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            drawTextView.setLayoutParams(layoutParams);
            drawTextView.setText(xiangS[i]);
            hengView.addView(drawTextView);
        }
        shuView.addView(hengView);
        view.addView(shuView, 5);
        addTopLayoutView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(int row) {
        for (int j = 0; j < row; j++) {
            LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            for (int i = 0; i < 13; i++) {
                if (i == 0 || i == 11 || i == 12) {
                    DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setTag(i + "");
                    drawTextView.setOnClickListener(this);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                } else {
                    DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                    drawEditText.setLayoutParams(layoutParams);
                    view.addView(drawEditText);
                }
            }
            pLayout.addView(view);
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        if (v.getId() == R.id.iv_add_row) {
            addCenterView(5);
            return;
        }
        Intent intent = null;
        String tag = (String) v.getTag();
        switch (tag) {
            case "0":
                chooseTime(true);
                break;
            case "11":
                chooseTime(false);
                break;
            case "12":
                intent = new Intent(this, LineLockActivity.class);
                intent.putExtra("isChoose", true);
                startActivityForResult(intent, CHOOSEName);
                break;
            case "chooseZhan":
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 1);
                startActivityForResult(intent, CHOOSEZM);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSEZM:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            zhan = name;
                            clickText.setText(zhan);
                        }
                    }
                    break;
                case CHOOSEName:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 选择日期
     */
    private void chooseTime(boolean isHHmm) {
        String format = "yyyy-MM-dd";
        boolean[] show = new boolean[]{true, true, true, false, false, false};
        if (isHHmm) {
            format = "yyyy-MM-dd HH:mm";
            show = new boolean[]{true, true, true, true, true, false};
        }
        //时间选择器
        final String finalFormat = format;
        TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                clickText.setText(TurnsUtils.convertToString(date,finalFormat));
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(show)
                .build();
        pvTime.show();
    }

    /**
     * 读取数据
     */
    private void readData() {
        String type = "=";
        duanLuTiaoZhaModelList = DataSupport.findAll(DuanLuTiaoZhaModel.class);
        if (duanLuTiaoZhaModelList != null && duanLuTiaoZhaModelList.size() > 0) {
            for (int i = 0; i < duanLuTiaoZhaModelList.size(); i++) {
                if (!type.equals(duanLuTiaoZhaModelList.get(i).getDlqbh())) {
                    type = duanLuTiaoZhaModelList.get(i).getDlqbh();
                    List<DuanLuTiaoZhaModel> list = DataSupport.where("dlqbh = ?", type).find(DuanLuTiaoZhaModel.class);
                    listRowDatas.add(list);
                }
            }
            row = listRowDatas.size() > row ? listRowDatas.size() : row;
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        if (listRowDatas != null && listRowDatas.size() > 0) {
            for (int i = 0, plCh = listRowDatas.size(); i < plCh; i++) {
                for (int j = 0; j < listRowDatas.get(i).size(); j++) {
                    DuanLuTiaoZhaModel model = listRowDatas.get(i).get(j);
                    LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
                    if (model != null) {
                        zhan = model.getBdzmc();
                        zhanTextView.setText(zhan);
                        ((DrawTextView) linearLayout.getChildAt(0)).setText(model.getRqsj());
                        ((DrawEditText) linearLayout.getChildAt(1)).setText(model.getDlqbh());
                        ((DrawEditText) linearLayout.getChildAt(2)).setText(model.getBhdzqk());
                        ((DrawEditText) linearLayout.getChildAt(3)).setText(model.getDlqjcqk());
                        ((DrawEditText) linearLayout.getChildAt(4)).setText(model.getGzdl());
                        ((DrawTextView) linearLayout.getChildAt(11)).setText(model.getZhycdxrq());
                        ((DrawTextView) linearLayout.getChildAt(12)).setText(model.getDjr());
                        switch (model.getXb()) {
                            case "A":
                                ((DrawEditText) linearLayout.getChildAt(5)).setText(model.getBctzcs());
                                ((DrawEditText) linearLayout.getChildAt(8)).setText(model.getLjtzcs());
                                break;
                            case "B":
                                ((DrawEditText) linearLayout.getChildAt(6)).setText(model.getBctzcs());
                                ((DrawEditText) linearLayout.getChildAt(9)).setText(model.getLjtzcs());
                                break;
                            case "C":
                                ((DrawEditText) linearLayout.getChildAt(7)).setText(model.getBctzcs());
                                ((DrawEditText) linearLayout.getChildAt(10)).setText(model.getLjtzcs());
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        DataSupport.deleteAll(DuanLuTiaoZhaModel.class);
        List<DuanLuTiaoZhaModel> duanLuTiaoZhaModelList = new ArrayList<>();
        for (int i = 0, plCh = pLayout.getChildCount(); i < plCh; i++) {
            LinearLayout linearLayout = (LinearLayout) pLayout.getChildAt(i);
            DrawEditText drawEditText = (DrawEditText) linearLayout.getChildAt(1);
            if (!TextUtils.isEmpty(drawEditText.getText().toString())) {
                for (int j = 0; j < 3; j++) {
                    DuanLuTiaoZhaModel model = new DuanLuTiaoZhaModel();
                    model.setBdzmc(zhan);
                    model.setDlqbh(drawEditText.getText().toString());
                    model.setRqsj(((DrawTextView) linearLayout.getChildAt(0)).getText().toString());
                    model.setBhdzqk(((DrawEditText) linearLayout.getChildAt(2)).getText().toString());
                    model.setDlqjcqk(((DrawEditText) linearLayout.getChildAt(3)).getText().toString());
                    model.setGzdl(((DrawEditText) linearLayout.getChildAt(4)).getText().toString());
                    model.setZhycdxrq(((DrawTextView) linearLayout.getChildAt(11)).getText().toString());
                    model.setDjr(((DrawTextView) linearLayout.getChildAt(12)).getText().toString());
                    switch (j) {
                        case 0:
                            model.setXb("A");
                            model.setBctzcs(((DrawEditText) linearLayout.getChildAt(5)).getText().toString());
                            model.setLjtzcs(((DrawEditText) linearLayout.getChildAt(8)).getText().toString());
                            break;
                        case 1:
                            model.setXb("B");
                            model.setBctzcs(((DrawEditText) linearLayout.getChildAt(6)).getText().toString());
                            model.setLjtzcs(((DrawEditText) linearLayout.getChildAt(9)).getText().toString());
                            break;
                        case 2:
                            model.setXb("C");
                            model.setBctzcs(((DrawEditText) linearLayout.getChildAt(7)).getText().toString());
                            model.setLjtzcs(((DrawEditText) linearLayout.getChildAt(10)).getText().toString());
                            break;
                        default:
                            break;
                    }
                    duanLuTiaoZhaModelList.add(model);
                }
            }
        }
        DataSupport.saveAll(duanLuTiaoZhaModelList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
