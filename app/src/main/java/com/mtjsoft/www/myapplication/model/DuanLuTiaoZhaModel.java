package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 断路器跳闸记录
 *         Created by Administrator on 2018/3/15.
 */

public class DuanLuTiaoZhaModel extends DataSupport {
    private int id;//主键id
    private String bdzmc;//	变电站名称
    private String rqsj;//日期时间
    private String dlqbh;//	断路器编号
    private String bhdzqk;//	保护动作情况
    private String dlqjcqk;//	断路器检查情况
    private String gzdl;//故障电流
    private String xb;//相别
    private String bctzcs;//	本次跳闸次数
    private String ljtzcs;//	累计跳闸次数
    private String zhycdxrq;//	最后一次大修日期
    private String djr;//登记人

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBdzmc() {
        return bdzmc;
    }

    public void setBdzmc(String bdzmc) {
        this.bdzmc = bdzmc;
    }

    public String getRqsj() {
        return rqsj;
    }

    public void setRqsj(String rqsj) {
        this.rqsj = rqsj;
    }

    public String getDlqbh() {
        return dlqbh;
    }

    public void setDlqbh(String dlqbh) {
        this.dlqbh = dlqbh;
    }

    public String getBhdzqk() {
        return bhdzqk;
    }

    public void setBhdzqk(String bhdzqk) {
        this.bhdzqk = bhdzqk;
    }

    public String getDlqjcqk() {
        return dlqjcqk;
    }

    public void setDlqjcqk(String dlqjcqk) {
        this.dlqjcqk = dlqjcqk;
    }

    public String getGzdl() {
        return gzdl;
    }

    public void setGzdl(String gzdl) {
        this.gzdl = gzdl;
    }

    public String getXb() {
        return xb;
    }

    public void setXb(String xb) {
        this.xb = xb;
    }

    public String getBctzcs() {
        return bctzcs;
    }

    public void setBctzcs(String bctzcs) {
        this.bctzcs = bctzcs;
    }

    public String getLjtzcs() {
        return ljtzcs;
    }

    public void setLjtzcs(String ljtzcs) {
        this.ljtzcs = ljtzcs;
    }

    public String getZhycdxrq() {
        return zhycdxrq;
    }

    public void setZhycdxrq(String zhycdxrq) {
        this.zhycdxrq = zhycdxrq;
    }

    public String getDjr() {
        return djr;
    }

    public void setDjr(String djr) {
        this.djr = djr;
    }
}
