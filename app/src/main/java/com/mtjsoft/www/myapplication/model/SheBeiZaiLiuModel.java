package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 变电站设备载流参数表
 *         Created by Administrator on 2018/3/13.
 */

public class SheBeiZaiLiuModel extends DataSupport {
    private int id;//主键id
    private String rq;//日期
    private String bdzmc;//	变电站名称
    private String sbmc;//设备名称
    private String kgbh;//开关编号
    private String dlq;//断路器（开关、刀闸额定电流）
    private String glkg;//隔离开关（开关、刀闸额定电流）
    private String ed;//额定（电流互感器变比）
    private String bhCT;//保护CT（电流互感器变比）
    private String jcCT;//计/测CT（电流互感器变比）
    private String zbq;//阻波器（额定电流(A)）
    private String ndyxggxh;//	（内导引线）规格型号（变电站内导引线载流25°C（A））
    private String ndyx;//（内导）引线（变电站内导引线载流25°C（A））
    private String sdxlzlggxh;//		（输电线路载流）规格型号（输电线路载流25°C（A））
    private String xl;//线路（输电线路载流25°C（A））
    private String bh;//保护（允许电流(A)）
    private String zxzlyj;//	最小载流元件
    private String zdyxdl;//	最大允许电流

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRq() {
        return rq;
    }

    public void setRq(String rq) {
        this.rq = rq;
    }

    public String getBdzmc() {
        return bdzmc;
    }

    public void setBdzmc(String bdzmc) {
        this.bdzmc = bdzmc;
    }

    public String getSbmc() {
        return sbmc;
    }

    public void setSbmc(String sbmc) {
        this.sbmc = sbmc;
    }

    public String getKgbh() {
        return kgbh;
    }

    public void setKgbh(String kgbh) {
        this.kgbh = kgbh;
    }

    public String getDlq() {
        return dlq;
    }

    public void setDlq(String dlq) {
        this.dlq = dlq;
    }

    public String getGlkg() {
        return glkg;
    }

    public void setGlkg(String glkg) {
        this.glkg = glkg;
    }

    public String getEd() {
        return ed;
    }

    public void setEd(String ed) {
        this.ed = ed;
    }

    public String getBhCT() {
        return bhCT;
    }

    public void setBhCT(String bhCT) {
        this.bhCT = bhCT;
    }

    public String getJcCT() {
        return jcCT;
    }

    public void setJcCT(String jcCT) {
        this.jcCT = jcCT;
    }

    public String getZbq() {
        return zbq;
    }

    public void setZbq(String zbq) {
        this.zbq = zbq;
    }

    public String getNdyxggxh() {
        return ndyxggxh;
    }

    public void setNdyxggxh(String ndyxggxh) {
        this.ndyxggxh = ndyxggxh;
    }

    public String getNdyx() {
        return ndyx;
    }

    public void setNdyx(String ndyx) {
        this.ndyx = ndyx;
    }

    public String getSdxlzlggxh() {
        return sdxlzlggxh;
    }

    public void setSdxlzlggxh(String sdxlzlggxh) {
        this.sdxlzlggxh = sdxlzlggxh;
    }

    public String getXl() {
        return xl;
    }

    public void setXl(String xl) {
        this.xl = xl;
    }

    public String getBh() {
        return bh;
    }

    public void setBh(String bh) {
        this.bh = bh;
    }

    public String getZxzlyj() {
        return zxzlyj;
    }

    public void setZxzlyj(String zxzlyj) {
        this.zxzlyj = zxzlyj;
    }

    public String getZdyxdl() {
        return zdyxdl;
    }

    public void setZdyxdl(String zdyxdl) {
        this.zdyxdl = zdyxdl;
    }
}
