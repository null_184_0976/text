package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * 继电保护及安全自动装置工作记录
 * Created by Administrator on 2018/3/12.
 */

public class YunWeiZiDongModel extends DataSupport {
    private int id;//主键id
    private String bdzmc;//	变电站名称
    private String yc;//页次
    private String rq;//日期
    private String zzmc;//装置名称
    private String gzpbh;//	工作票编号
    private String gznr;//工作内容
    private String jyjg;//检验结果(包括接线改变、定值变更原因及发现问题尚存在缺陷事项等)
    private String jl;//结论
    private String jyfzrqz;//	检验负责人签字
    private String ysryqz;//	验收人员签字

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBdzmc() {
        return bdzmc;
    }

    public void setBdzmc(String bdzmc) {
        this.bdzmc = bdzmc;
    }

    public String getYc() {
        return yc;
    }

    public void setYc(String yc) {
        this.yc = yc;
    }

    public String getRq() {
        return rq;
    }

    public void setRq(String rq) {
        this.rq = rq;
    }

    public String getZzmc() {
        return zzmc;
    }

    public void setZzmc(String zzmc) {
        this.zzmc = zzmc;
    }

    public String getGzpbh() {
        return gzpbh;
    }

    public void setGzpbh(String gzpbh) {
        this.gzpbh = gzpbh;
    }

    public String getGznr() {
        return gznr;
    }

    public void setGznr(String gznr) {
        this.gznr = gznr;
    }

    public String getJyjg() {
        return jyjg;
    }

    public void setJyjg(String jyjg) {
        this.jyjg = jyjg;
    }

    public String getJl() {
        return jl;
    }

    public void setJl(String jl) {
        this.jl = jl;
    }

    public String getJyfzrqz() {
        return jyfzrqz;
    }

    public void setJyfzrqz(String jyfzrqz) {
        this.jyfzrqz = jyfzrqz;
    }

    public String getYsryqz() {
        return ysryqz;
    }

    public void setYsryqz(String ysryqz) {
        this.ysryqz = ysryqz;
    }
}
