package com.mtjsoft.www.myapplication.utils;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;

import com.bigkoo.pickerview.TimePickerView;
import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import java.util.Date;

/**
 * @author mtj
 * Created by Administrator on 2018/3/15.
 */

public class HHInputUtils {

    /**
     * 选择日期
     */
    public static void chooseTime(boolean isHHmm, final DrawTextView clickText) {
        String format = "yyyy-MM-dd";
        boolean[] show = new boolean[]{true, true, true, false, false, false};
        if (isHHmm) {
            format = "yyyy-MM-dd HH:mm";
            show = new boolean[]{true, true, true, true, true, false};
        }
        //时间选择器
        final String finalFormat = format;
        TimePickerView pvTime = new TimePickerView.Builder(clickText.getContext(), new TimePickerView.OnTimeSelectListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                clickText.setText(TurnsUtils.convertToString(date,finalFormat));
            }
        }).isDialog(true)
                //标题文字颜色
                .setTitleColor(clickText.getContext().getResources().getColor(R.color.main_base_color))
                //确定按钮文字颜色
                .setSubmitColor(clickText.getContext().getResources().getColor(R.color.main_base_color))
                //取消按钮文字颜色
                .setCancelColor(clickText.getContext().getResources().getColor(R.color.main_base_color))
                .setOutSideCancelable(true)
                .setType(show)
                .build();
        pvTime.show();
    }
}
