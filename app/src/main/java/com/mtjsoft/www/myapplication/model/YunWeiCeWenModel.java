package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 设备测温记录
 *         Created by Administrator on 2018/3/20.
 */

public class YunWeiCeWenModel extends DataSupport {
    private int id;//主键id
    private String bdz;//变电站
    private String cwsj;//测温时间
    private String sfsqzcw;//	是否是全站（区域）测温
    private String cwlx;//测温类型
    private String hjwd;//环境温度（℃）
    private String sd;//湿度
    private String sbmc;//设备名称
    private String jgdy;//间隔单元
    private String fs;//风速m/s
    private String cwr;//测温人
    private String cwyq;//测温仪器（型号、编号）
    private String cwfw;//测温范围
    private String cwwz;//测温位置
    private String fxwt;//发现问题
    private String bz;//备注

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBdz() {
        return bdz;
    }

    public void setBdz(String bdz) {
        this.bdz = bdz;
    }

    public String getCwsj() {
        return cwsj;
    }

    public void setCwsj(String cwsj) {
        this.cwsj = cwsj;
    }

    public String getSfsqzcw() {
        return sfsqzcw;
    }

    public void setSfsqzcw(String sfsqzcw) {
        this.sfsqzcw = sfsqzcw;
    }

    public String getCwlx() {
        return cwlx;
    }

    public void setCwlx(String cwlx) {
        this.cwlx = cwlx;
    }

    public String getHjwd() {
        return hjwd;
    }

    public void setHjwd(String hjwd) {
        this.hjwd = hjwd;
    }

    public String getSd() {
        return sd;
    }

    public void setSd(String sd) {
        this.sd = sd;
    }

    public String getSbmc() {
        return sbmc;
    }

    public void setSbmc(String sbmc) {
        this.sbmc = sbmc;
    }

    public String getJgdy() {
        return jgdy;
    }

    public void setJgdy(String jgdy) {
        this.jgdy = jgdy;
    }

    public String getFs() {
        return fs;
    }

    public void setFs(String fs) {
        this.fs = fs;
    }

    public String getCwr() {
        return cwr;
    }

    public void setCwr(String cwr) {
        this.cwr = cwr;
    }

    public String getCwyq() {
        return cwyq;
    }

    public void setCwyq(String cwyq) {
        this.cwyq = cwyq;
    }

    public String getCwfw() {
        return cwfw;
    }

    public void setCwfw(String cwfw) {
        this.cwfw = cwfw;
    }

    public String getCwwz() {
        return cwwz;
    }

    public void setCwwz(String cwwz) {
        this.cwwz = cwwz;
    }

    public String getFxwt() {
        return fxwt;
    }

    public void setFxwt(String fxwt) {
        this.fxwt = fxwt;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }
}
