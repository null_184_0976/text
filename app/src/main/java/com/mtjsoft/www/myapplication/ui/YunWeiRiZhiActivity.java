package com.mtjsoft.www.myapplication.ui;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mtjsoft.www.myapplication.R;
import com.mtjsoft.www.myapplication.base.BaseActivity;
import com.mtjsoft.www.myapplication.base.ChooseActivity;
import com.mtjsoft.www.myapplication.base.LineLockActivity;
import com.mtjsoft.www.myapplication.model.YunWeiRiZhiModel;
import com.mtjsoft.www.myapplication.utils.HHDensityUtils;
import com.mtjsoft.www.myapplication.utils.HHInputUtils;
import com.mtjsoft.www.myapplication.viewself.DrawEditText;
import com.mtjsoft.www.myapplication.viewself.DrawTextView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mtj 变电运维工作日志
 *         Created by Administrator on 2018/3/11.
 */

public class YunWeiRiZhiActivity extends BaseActivity {

    private static final int CHOOSEBIANDIAN = 1001;
    private static final int CHOOSEGJ = 1002;
    private static final int CHOOSENAME = 1003;
    private DrawTextView clickText;
    private ImageView addImageView;
    private LinearLayout pLayout;
    private List<DrawTextView> drawTextViewList = new ArrayList<>();
    private List<DrawEditText> drawEditTextList = new ArrayList<>();

    @Override
    protected View initViewResId() {
        View view = View.inflate(getBaseContext(), R.layout.activity_yunwei, null);
        pLayout = view.findViewById(R.id.ll_add_row);
        addImageView = view.findViewById(R.id.iv_add_row);
        return view;
    }

    @Override
    protected void initView() {
        addImageView.setVisibility(View.GONE);
        setTopName("变电运维工作日志");
        addCenterView("运维班（站）：", true, "日期：", true);
        addCenterView("星期：", true, "天气：", false);
        addCenterView("当值接班终了时间：", true);
        addCenterView("当值交班终了时间：", true);
        addCenterView("交班人：", true);
        addCenterView("接班人：", true);
        addBottomLayout("运行方式");
        DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText("运行记事");
        pLayout.addView(drawTextView);
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < 3; i++) {
            drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
            switch (i) {
                case 0:
                    layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
                    drawTextView.setText("序号");
                    break;
                case 1:
                    layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 4);
                    drawTextView.setText("日期");
                    break;
                case 2:
                    layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 7);
                    drawTextView.setText("内容");
                    break;
                default:
                    break;
            }
            drawTextView.setLayoutParams(layoutParams);
            view.addView(drawTextView);
        }
        pLayout.addView(view);
        //
        addCenterView("一", "巡视工作");
        addCenterView("二", "调控指令");
        addCenterView("三", "设备缺陷情况");
        addCenterView("四", "工作票执行情况");
        addCenterView("五", "倒闸操作情况");
        addCenterView("六", "保护方式调整情况");
        addCenterView("七", "故障及异常情况");
        addCenterView("八", "接地线（接地刀闸）使用情况");
        addCenterView("九", "解锁钥匙使用情况");
        addCenterView("十", "下发文件、通知、要求、规定");
        addCenterView("十一", "设备维护情况");
        addCenterView("十二", "其他");
        setData();
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(String leftName, boolean leftIsText, String rightName, boolean rightIsText) {
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < 4; i++) {
            DrawTextView drawTextView = null;
            DrawEditText drawEditText = null;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            switch (i) {
                case 0:
                    drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(leftName);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                    break;
                case 1:
                    if (leftIsText) {
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setLayoutParams(layoutParams);
                        drawTextView.setOnClickListener(this);
                        drawTextView.setId(drawTextViewList.size());
                        drawTextViewList.add(drawTextView);
                        view.addView(drawTextView);
                    } else {
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        drawEditTextList.add(drawEditText);
                        view.addView(drawEditText);
                    }
                    break;
                case 2:
                    drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(rightName);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                    break;
                case 3:
                    if (rightIsText) {
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setLayoutParams(layoutParams);
                        drawTextView.setOnClickListener(this);
                        drawTextView.setId(drawTextViewList.size());
                        drawTextViewList.add(drawTextView);
                        view.addView(drawTextView);
                    } else {
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        drawEditTextList.add(drawEditText);
                        view.addView(drawEditText);
                    }
                    break;
                default:
                    break;
            }
        }
        pLayout.addView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(String leftName, boolean leftIsText) {
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        for (int i = 0; i < 2; i++) {
            DrawTextView drawTextView = null;
            DrawEditText drawEditText = null;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            switch (i) {
                case 0:
                    drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                    drawTextView.setText(leftName);
                    drawTextView.setLayoutParams(layoutParams);
                    view.addView(drawTextView);
                    break;
                case 1:
                    layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 3);
                    if (leftIsText) {
                        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
                        drawTextView.setLayoutParams(layoutParams);
                        drawTextView.setOnClickListener(this);
                        drawTextView.setId(drawTextViewList.size());
                        drawTextViewList.add(drawTextView);
                        view.addView(drawTextView);
                    } else {
                        drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
                        drawEditText.setLayoutParams(layoutParams);
                        drawEditTextList.add(drawEditText);
                        view.addView(drawEditText);
                    }
                    break;
                default:
                    break;
            }
        }
        pLayout.addView(view);
    }

    /**
     * 添加中间表格
     */
    private void addCenterView(String xuhao, String title) {
        LinearLayout view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setText(xuhao);
        drawTextView.setLayoutParams(layoutParams);
        view.addView(drawTextView);
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 11);
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setText(title);
        drawTextView.setLayoutParams(layoutParams);
        view.addView(drawTextView);
        pLayout.addView(view);
        view = (LinearLayout) View.inflate(getBaseContext(), R.layout.include_horizontal_linearlayout_f_w, null);
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setLayoutParams(layoutParams);
        view.addView(drawTextView);
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 4);
        drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setOnClickListener(this);
        drawTextView.setId(drawTextViewList.size());
        drawTextViewList.add(drawTextView);
        view.addView(drawTextView);
        DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
        layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 7);
        drawEditText.setLayoutParams(layoutParams);
        drawEditTextList.add(drawEditText);
        view.addView(drawEditText);
        pLayout.addView(view);
    }

    private void addBottomLayout(String bottomName) {
        DrawTextView drawTextView = (DrawTextView) View.inflate(getBaseContext(), R.layout.include_w_w_drawtext, null);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        drawTextView.setLayoutParams(layoutParams);
        drawTextView.setText(bottomName);
        pLayout.addView(drawTextView);
        //
        DrawEditText drawEditText = (DrawEditText) View.inflate(getBaseContext(), R.layout.include_w_w_drawedit, null);
        drawEditText.setMinHeight(HHDensityUtils.dip2px(getBaseContext(), 150));
        drawEditText.setLayoutParams(layoutParams);
        drawEditTextList.add(drawEditText);
        pLayout.addView(drawEditText);
    }


    @Override
    public void onClick(View v) {
        if (v instanceof DrawTextView) {
            clickText = (DrawTextView) v;
        }
        Intent intent = null;
        switch (v.getId()) {
            case 0:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 1);
                startActivityForResult(intent, CHOOSEBIANDIAN);
                break;
            case 2:
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra("type", 8);
                startActivityForResult(intent, CHOOSEBIANDIAN);
                break;
            case 3:
            case 4:
                HHInputUtils.chooseTime(true, clickText);
                break;
            case 5:
            case 6:
                intent = new Intent(this, LineLockActivity.class);
                intent.putExtra("isChoose", true);
                startActivityForResult(intent, CHOOSENAME);
                break;
            default:
                HHInputUtils.chooseTime(false, clickText);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSENAME:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name.replace(",", ""));
                        }
                    }
                    break;
                case CHOOSEBIANDIAN:
                    if (clickText != null) {
                        String name = data.getStringExtra("chooseName");
                        if (!TextUtils.isEmpty(name)) {
                            clickText.setText(name);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 设置数据
     */
    private void setData() {
        YunWeiRiZhiModel model = DataSupport.findLast(YunWeiRiZhiModel.class);
        if (model != null) {
            drawEditTextList.get(0).setText(model.getTq());
            drawEditTextList.get(1).setText(model.getYxfs());
            drawEditTextList.get(2).setText(model.getXsgz());
            drawEditTextList.get(3).setText(model.getTkzl());
            drawEditTextList.get(4).setText(model.getQxqk());
            drawEditTextList.get(5).setText(model.getGzpzx());
            drawEditTextList.get(6).setText(model.getDzcz());
            drawEditTextList.get(7).setText(model.getBhfs());
            drawEditTextList.get(8).setText(model.getGzqk());
            drawEditTextList.get(9).setText(model.getJdxsy());
            drawEditTextList.get(10).setText(model.getJsys());
            drawEditTextList.get(11).setText(model.getWjtz());
            drawEditTextList.get(12).setText(model.getSbwh());
            drawEditTextList.get(13).setText(model.getQt());
            //
            drawTextViewList.get(0).setText(model.getYwbz());
            drawTextViewList.get(1).setText(model.getTbrq());
            drawTextViewList.get(2).setText(model.getXq());
            drawTextViewList.get(3).setText(model.getJiebsj());
            drawTextViewList.get(4).setText(model.getJbsj());
            drawTextViewList.get(5).setText(model.getJbr());
            drawTextViewList.get(6).setText(model.getJiebr());
            drawTextViewList.get(7).setText(model.getXsrq());
            drawTextViewList.get(8).setText(model.getTkrq());
            drawTextViewList.get(9).setText(model.getQtrq());
            drawTextViewList.get(10).setText(model.getGzprq());
            drawTextViewList.get(11).setText(model.getDzrq());
            drawTextViewList.get(12).setText(model.getBhrq());
            drawTextViewList.get(13).setText(model.getGzrq());
            drawTextViewList.get(14).setText(model.getJdrq());
            drawTextViewList.get(15).setText(model.getJsrq());
            drawTextViewList.get(16).setText(model.getWjrq());
            drawTextViewList.get(17).setText(model.getSbrq());
            drawTextViewList.get(18).setText(model.getQtrq());
        }
    }

    /**
     * 保存数据
     */
    private void saveData() {
        YunWeiRiZhiModel model = new YunWeiRiZhiModel();
        model.setTq(drawEditTextList.get(0).getText().toString());
        model.setYxfs(drawEditTextList.get(1).getText().toString());
        model.setXsgz(drawEditTextList.get(2).getText().toString());
        model.setTkzl(drawEditTextList.get(3).getText().toString());
        model.setQxqk(drawEditTextList.get(4).getText().toString());
        model.setGzpzx(drawEditTextList.get(5).getText().toString());
        model.setDzcz(drawEditTextList.get(6).getText().toString());
        model.setBhfs(drawEditTextList.get(7).getText().toString());
        model.setGzqk(drawEditTextList.get(8).getText().toString());
        model.setJdxsy(drawEditTextList.get(9).getText().toString());
        model.setJsys(drawEditTextList.get(10).getText().toString());
        model.setWjtz(drawEditTextList.get(11).getText().toString());
        model.setSbwh(drawEditTextList.get(12).getText().toString());
        model.setQt(drawEditTextList.get(13).getText().toString());
        //
        model.setYwbz(drawTextViewList.get(0).getText().toString());
        model.setTbrq(drawTextViewList.get(1).getText().toString());
        model.setXq(drawTextViewList.get(2).getText().toString());
        model.setJiebsj(drawTextViewList.get(3).getText().toString());
        model.setJbsj(drawTextViewList.get(4).getText().toString());
        model.setJbr(drawTextViewList.get(5).getText().toString());
        model.setJiebr(drawTextViewList.get(6).getText().toString());
        model.setXsrq(drawTextViewList.get(7).getText().toString());
        model.setTkrq(drawTextViewList.get(8).getText().toString());
        model.setQtrq(drawTextViewList.get(9).getText().toString());
        model.setGzprq(drawTextViewList.get(10).getText().toString());
        model.setDzrq(drawTextViewList.get(11).getText().toString());
        model.setBhrq(drawTextViewList.get(12).getText().toString());
        model.setGzrq(drawTextViewList.get(13).getText().toString());
        model.setJdrq(drawTextViewList.get(14).getText().toString());
        model.setJsrq(drawTextViewList.get(15).getText().toString());
        model.setWjrq(drawTextViewList.get(16).getText().toString());
        model.setSbrq(drawTextViewList.get(17).getText().toString());
        model.setQtrq(drawTextViewList.get(18).getText().toString());
        model.save();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }
}
