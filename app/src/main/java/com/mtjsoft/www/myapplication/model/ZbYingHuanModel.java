package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 变电站周边隐患台账
 *         Created by Administrator on 2018/3/10.
 */

public class ZbYingHuanModel extends DataSupport {
    private int id;//	主键id
    private String zm;//	站名
    private String rq;//	日期
    private String bdzmc;//		变电站名称
    private String yhqk;//	隐患情况
    private String sbrq;//	上报日期
    private String tbr;//	填报人
    private String csqk;//	措施情况
    private String bz;//	备注

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getZm() {
        return zm;
    }

    public void setZm(String zm) {
        this.zm = zm;
    }

    public String getRq() {
        return rq;
    }

    public void setRq(String rq) {
        this.rq = rq;
    }

    public String getBdzmc() {
        return bdzmc;
    }

    public void setBdzmc(String bdzmc) {
        this.bdzmc = bdzmc;
    }

    public String getYhqk() {
        return yhqk;
    }

    public void setYhqk(String yhqk) {
        this.yhqk = yhqk;
    }

    public String getSbrq() {
        return sbrq;
    }

    public void setSbrq(String sbrq) {
        this.sbrq = sbrq;
    }

    public String getTbr() {
        return tbr;
    }

    public void setTbr(String tbr) {
        this.tbr = tbr;
    }

    public String getCsqk() {
        return csqk;
    }

    public void setCsqk(String csqk) {
        this.csqk = csqk;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }
}
