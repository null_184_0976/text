package com.mtjsoft.www.myapplication.model;

import org.litepal.crud.DataSupport;

/**
 * @author mtj 变电站外来人员登记记录
 *         Created by Administrator on 2018/3/11.
 */

public class WaiLaiJiLuModel extends DataSupport {
    private int id;//主键id
    private String bdzmc;//	变电站名称
    private String rq;//日期
    private String jzsj;//进站时间
    private String lzsj;//离站时间
    private String jzsx;//进站手续
    private String jzyxr;//	进站允许人
    private String cph;//车牌号
    private String wlryszdw;//	外来人员所在单位
    private String wlryfzr;//	外来人员负责人
    private String rs;//人数
    private String gznr;//工作内容
    private String bz;//备注

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBdzmc() {
        return bdzmc;
    }

    public void setBdzmc(String bdzmc) {
        this.bdzmc = bdzmc;
    }

    public String getRq() {
        return rq;
    }

    public void setRq(String rq) {
        this.rq = rq;
    }

    public String getJzsj() {
        return jzsj;
    }

    public void setJzsj(String jzsj) {
        this.jzsj = jzsj;
    }

    public String getLzsj() {
        return lzsj;
    }

    public void setLzsj(String lzsj) {
        this.lzsj = lzsj;
    }

    public String getJzsx() {
        return jzsx;
    }

    public void setJzsx(String jzsx) {
        this.jzsx = jzsx;
    }

    public String getJzyxr() {
        return jzyxr;
    }

    public void setJzyxr(String jzyxr) {
        this.jzyxr = jzyxr;
    }

    public String getCph() {
        return cph;
    }

    public void setCph(String cph) {
        this.cph = cph;
    }

    public String getWlryszdw() {
        return wlryszdw;
    }

    public void setWlryszdw(String wlryszdw) {
        this.wlryszdw = wlryszdw;
    }

    public String getWlryfzr() {
        return wlryfzr;
    }

    public void setWlryfzr(String wlryfzr) {
        this.wlryfzr = wlryfzr;
    }

    public String getRs() {
        return rs;
    }

    public void setRs(String rs) {
        this.rs = rs;
    }

    public String getGznr() {
        return gznr;
    }

    public void setGznr(String gznr) {
        this.gznr = gznr;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }
}
